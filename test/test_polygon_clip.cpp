#include <geomod/affine/vector.h>
#include <geomod/affine/point.h>
#include <geomod/affine/ray.h>
#include <geomod/affine/transform.h>
#include <geomod/planar/polygon.h>
#include <iostream>
#include <cassert>

using geomod::coord_type;
using geomod::affine::Point2;
using geomod::affine::Vector2;
using geomod::affine::Ray2;
using geomod::planar::Polygon;

void print_poly(const Polygon &poly){
	for(size_t i = 0; i < poly.size(); ++i){
		std::cout << "  " << poly[i] << std::endl;
	}
}

void test_triangles(){
	const Point2 vertices[3] = {
		Point2(0, 0),
		Point2(2, 0),
		Point2(0, 2),
	};
	for(unsigned perm = 0; perm < 3; ++perm){
		std::cout << "Case 1 Permutation " << perm << std::endl;
		Polygon::vertex_vector v;
		v.push_back(vertices[(0+perm)%3]);
		v.push_back(vertices[(1+perm)%3]);
		v.push_back(vertices[(2+perm)%3]);
		Polygon p(v);
		//std::cout << "Starting polygon:\n"; print_poly(p);
		Ray2 r(Point2(1, 0), Vector2(0, 1));
		Polygon q = p.Clip(r);
		//std::cout << "Clipped polygon:\n"; print_poly(q);
		assert(std::abs(q.Area() - 1.5) < 1e-10);
	}
	for(unsigned perm = 0; perm < 3; ++perm){
		std::cout << "Case 2 Permutation " << perm << std::endl;
		Polygon::vertex_vector v;
		v.push_back(vertices[(0+perm)%3]);
		v.push_back(vertices[(1+perm)%3]);
		v.push_back(vertices[(2+perm)%3]);
		Polygon p(v);
		//std::cout << "Starting polygon:\n"; print_poly(p);
		Ray2 r(Point2(1, 0), Vector2(-1, 1));
		Polygon q = p.Clip(r);
		//std::cout << "Clipped polygon:\n"; print_poly(q);
		assert(std::abs(q.Area() - 0.5) < 1e-10);
	}
}

void test_two(){
	const size_t n = 8;
	const Point2 vertices[8] = {
		Point2(0, 0),
		Point2(3, 0),
		Point2(3, 1),
		Point2(1, 1),
		Point2(1, 2),
		Point2(3, 2),
		Point2(3, 3),
		Point2(0, 3),
	};
	for(unsigned perm = 0; perm < n; ++perm){
		std::cout << "Case two Permutation " << perm << std::endl;
		Polygon::vertex_vector v;
		for(size_t i = 0; i < n; ++i){
			v.push_back(vertices[(i+perm)%n]);
		}
		Polygon p(v);
		//std::cout << "Starting polygon:\n"; print_poly(p);
		Ray2 r(Point2(2, 0), Vector2(0, 1));
		Polygon q = p.Clip(r);
		//std::cout << "Clipped polygon:\n"; print_poly(q);
		assert(std::abs(q.Area() - 5) < 1e-10);
	}
}


void test_multiple(){
	const size_t n = 12;
	const Point2 vertices[12] = {
		Point2(0, 0),
		Point2(3, 0),
		Point2(3, 1),
		Point2(1, 1),
		Point2(1, 2),
		Point2(3, 2),
		Point2(3, 3),
		Point2(1, 3),
		Point2(1, 4),
		Point2(3, 4),
		Point2(3, 5),
		Point2(0, 5),
	};
	for(unsigned perm = 0; perm < n; ++perm){
		std::cout << "Case multiple Permutation " << perm << std::endl;
		Polygon::vertex_vector v;
		for(size_t i = 0; i < n; ++i){
			v.push_back(vertices[(i+perm)%n]);
		}
		Polygon p(v);
		//std::cout << "Starting polygon:\n"; print_poly(p);
		Ray2 r(Point2(2, 0), Vector2(0, 1));
		Polygon q = p.Clip(r);
		//std::cout << "Clipped polygon:\n"; print_poly(q);
		assert(std::abs(q.Area() - 8) < 1e-10);
	}
}

int main(int argc, char *argv[]){
	test_triangles();
	test_two();
	test_multiple();
	return 0;
}
