#include <geomod/affine/vector.h>
#include <geomod/affine/point.h>
#include <geomod/affine/aabb.h>
#include <geomod/affine/ray.h>
#include <geomod/util/quaternion.h>
#include <geomod/affine/transform.h>
#include <iostream>
#include <cassert>

using geomod::coord_type;
using geomod::affine::Point2;
using geomod::affine::Vector2;
using geomod::affine::Point3;
using geomod::affine::Vector3;
using geomod::util::Quaternion;

int main(int argc, char *argv[]){
	Point2 a(2,4);
	Point2 b(10,12);
	std::cout << Point2::Interpolate(a, b, 0.5) << std::endl;
	
	{
		geomod::util::Quaternion q(std::sqrt(0.5) * Vector3::Z(), std::sqrt(0.5));
		coord_type m[9];
		q.AsMatrix(m);
		std::cout
		<< m[0] << ", " << m[3] << ", " << m[6] << "\n"
		<< m[1] << ", " << m[4] << ", " << m[7] << "\n"
		<< m[2] << ", " << m[5] << ", " << m[8] << std::endl;
		// Ensure positive orientation
		assert(m[1] > 0);
		assert(m[3] < 0);
	}
	
	// Test transformation
	{
		coord_type m[12];
		auto R = geomod::affine::Transform3::Rotate(Vector3(1,0,0), 90);
		auto T = geomod::affine::Transform3::Translate(Vector3(0,1,0));
		auto X = T*R;
		
		Quaternion Rqr, Rqd, Tqr, Tqd, Xqr, Xqd;
		R.AsDualQuaternion(Rqr, Rqd);
		T.AsDualQuaternion(Tqr, Tqd);
		X.AsDualQuaternion(Xqr, Xqd);
		std::cout << "R = " << Rqr << " + " << Rqd << "e" << std::endl;
		std::cout << "T = " << Tqr << " + " << Tqd << "e" << std::endl;
		std::cout << "X = " << Xqr << " + " << Xqd << "e" << std::endl;
		
		Quaternion d1 = Rqd * Tqr + Rqr * Tqd;
		Quaternion d2 = Tqd * Rqr + Tqr * Rqd;
		std::cout << "q1 = " << d1 << std::endl;
		std::cout << "q2 = " << d2 << std::endl;
		
		X.AsMatrix(m);
		std::cout
		<< m[0] << ", " << m[3] << ", " << m[6] << ", " << m[ 9] << "\n"
		<< m[1] << ", " << m[4] << ", " << m[7] << ", " << m[10] << "\n"
		<< m[2] << ", " << m[5] << ", " << m[8] << ", " << m[11] << std::endl;
		
		Point3 x(1,0,0);
		Point3 y(0,1,0);
		Point3 z(0,0,1);
		Point3 xp = X(x);
		Point3 yp = X(y);
		Point3 zp = X(z);
		std::cout << "X" << x << " -> " << xp << std::endl;
		std::cout << "X" << y << " -> " << yp << std::endl;
		std::cout << "X" << z << " -> " << zp << std::endl;
		
		
		Vector3 vx(1,0,0);
		Vector3 vy(0,1,0);
		Vector3 vz(0,0,1);
		Vector3 vxp = X(vx);
		Vector3 vyp = X(vy);
		Vector3 vzp = X(vz);
		std::cout << "X" << vx << " -> " << vxp << std::endl;
		std::cout << "X" << vy << " -> " << vyp << std::endl;
		std::cout << "X" << vz << " -> " << vzp << std::endl;
	}
	return 0;
}
