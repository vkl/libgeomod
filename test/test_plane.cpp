#include <geomod/affine/vector.h>
#include <geomod/affine/point.h>
#include <geomod/affine/ray.h>
#include <geomod/affine/transform.h>
#include <geomod/surface/plane.h>
#include <geomod/planar/polygon.h>
#include <iostream>

using geomod::coord_type;
using geomod::affine::Point3;
using geomod::affine::Vector3;
using geomod::affine::Point2;
using geomod::affine::Vector2;
using geomod::affine::Ray2;
using geomod::affine::Transform3;
using geomod::surface::Plane;

int main(int argc, char *argv[]){
	Transform3 p0 = Plane::ToTransform(Point3(0, 0, 0), Vector3(1, 0, 0));
	Transform3 p1 = Plane::ToTransform(Point3(0, 0, 0), Vector3(0, 1, 0));
	Transform3 p2 = Plane::ToTransform(Point3(0, 0, 0), Vector3(0, 0, 1));
	Transform3 p3 = Plane::ToTransform(Point3(0, 0, 1), Vector3(1, 1, 1));
	std::cout << "x = 0 plane transform: " << p0 << std::endl;
	std::cout << "y = 0 plane transform: " << p1 << std::endl;
	std::cout << "z = 0 plane transform: " << p2 << std::endl;
	std::cout << "<111> plane transform: " << p3 << std::endl;

	// p3 is hereby designated the <111> plane, that passes through (1, 0, 0), (0, 1, 0), and (0, 0, 1).
	// We compute the intersection of the cartesian planes with the <111> plane,
	// returning the intersection lines as rays in the <111> plane's coordinate frame.
	Plane p;
	Ray2 r(Point2::Origin(), Vector2::X());
	p.PlaneIntersection(Transform3::Inverse(p3) * p0, r);
	std::cout << "r(x=0, <111>) = " << r.origin() << ", " << r. dir() << std::endl;
	p.PlaneIntersection(Transform3::Inverse(p3) * p1, r);
	std::cout << "r(y=0, <111>) = " << r.origin() << ", " << r. dir() << std::endl;
	p.PlaneIntersection(Transform3::Inverse(p3) * p2, r);
	std::cout << "r(z=0, <111>) = " << r.origin() << ", " << r. dir() << std::endl;

	return 0;
}
