#include <geomod/affine/vector.h>
#include <geomod/util/quaternion.h>
#include <iostream>

using geomod::coord_type;
using geomod::affine::Vector3;
using geomod::util::Quaternion;

int main(int argc, char *argv[]){
	Quaternion a(Vector3(1,2,3), 4);
	Quaternion b(Vector3(5,6,7), 8);
	Quaternion c = a*b;
	std::cout << a << " * " << b << " = " << c << std::endl;

	return 0;
}
