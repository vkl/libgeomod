#include <geomod/planar/rectangle.h>
#include <geomod/planar/polygon.h>
#include <geomod/planar/circle.h>
#include <geomod/volume/extrusion.h>
#include <geomod/surface/mesh.h>
#include <iostream>

using namespace geomod;
using namespace geomod::affine;

void test_box(){
	geomod::planar::Rectangle r(1, 2);
	geomod::volume::Extrusion e(&r, Vector3(0.1, 0.2, 1));
	geomod::surface::Mesh *m = e.AsMesh();
	
	geomod::surface::Mesh::vertex_vector vertices;
	geomod::surface::Mesh::index_vector triangle_indices;
	m->GetElements(vertices, triangle_indices);
	const size_t nv = vertices.size();
	const size_t nt = triangle_indices.size() / 3;
	for(size_t i = 0; i < nv; ++i){
		std::cout << "v[" << i << "] = " << vertices[i] << std::endl;
	}
	for(size_t i = 0; i < nt; ++i){
		std::cout << "t[" << i << "] = ("
			<< triangle_indices[3*i+0] << ", "
			<< triangle_indices[3*i+1] << ", "
			<< triangle_indices[3*i+2] << ")\n";
	}
}

class Parabolic : public geomod::volume::Extrusion::SurfaceProfile{
	coord_type c;
public:
	Parabolic(const coord_type &c):c(c){}
	coord_type Evaluate(const affine::Point2 &r, affine::Vector2 *gradient = NULL, coord_type *II = NULL) const{
		coord_type rr = r[0]*r[0] + r[1]*r[1];
		if(NULL != gradient){
			*gradient = Vector2(2*c*r[0], 2*c*r[1]);
		}
		return c*rr;
	}
	onedim::Interval Bounds(const affine::AABB2 &bounds) const{
		return onedim::Interval(0,0);
	}
	Parabolic *Clone() const{
		return new Parabolic(c);
	}
};
void test_surface(){
	geomod::planar::Circle c(1);
	Parabolic *surf0 = new Parabolic(0.2);
	Parabolic *surf1 = new Parabolic(-0.1);
	geomod::volume::Extrusion e(&c, Vector3(0,0,1), surf0, surf1);
	geomod::surface::Mesh *m = e.AsMesh();
	m->Serialize(geomod::surface::Mesh::MESH_FORMAT_OFF, "test.off");
	AABB3 bnd = e.Bounds();
	std::cout << "Bounds: " << bnd.Min() << ", " << bnd.Max() << std::endl;
}

int main(int argc, char *argv[]){
	test_box();
	test_surface();
	return 0;
}
