#include <complex.h>
#include <geomod/affine/vector.h>
#include <geomod/util/tensor.h>
#include <iostream>

using geomod::coord_type;
using geomod::affine::Vector3;
using geomod::util::Quaternion;
using Tensor = geomod::util::TTensor3<std::complex<float> >;

int main(int argc, char *argv[]){
	Tensor t(1);
	std::cout << t << std::endl;

	Vector3 v(3, 4, 5);
	std::cout << t.Contraction(v, v) << std::endl;
	return 0;
}
