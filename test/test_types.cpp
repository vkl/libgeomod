#include <geomod/types.h>

using namespace geomod;

int main() {
	using C1 = upgrade_if_complex<float, float>::type;
	static_assert(std::is_same<C1, float>::value, "Error: C1 should be float");

	// Case 2: A is double, B is std::complex<float>
	using C2 = upgrade_if_complex<double, std::complex<float>>::type;
	static_assert(std::is_same<C2, std::complex<double>>::value, "Error: C2 should be std::complex<double>");

	// Case 3: A is std::complex<long double>, B is long
	using C3 = upgrade_if_complex<std::complex<long double>, double>::type;
	static_assert(std::is_same<C3, std::complex<long double>>::value, "Error: C3 should be std::complex<long double>");

	// Case 4: A and B are std::complex with different base types
	using C4 = upgrade_if_complex<std::complex<float>, std::complex<double>>::type;
	static_assert(std::is_same<C4, std::complex<double>>::value, "Error: C4 should be std::complex<double>");

	// Case 5: A is float, B is double
	using C5 = upgrade_if_complex<float, double>::type;
	static_assert(std::is_same<C5, double>::value, "Error: C5 should be double");

	std::complex<double> zd(1, 2);
	std::complex<float> zf;
	complex_cast(zd, zf);

	return 0;
}
