#include <geomod/planar/grid.h>
#include <iostream>

using geomod::index_type;
using geomod::coord_type;
using geomod::affine::Point2;
using geomod::affine::Vector2;

int main(int argc, char *argv[]){
	/*
	geomod::planar::RegularGrid grid(
		geomod::planar::Lattice(Vector2(1, 0), Vector2(0, 1)),
		Point2::Origin(),
		4, 5
	);
	*/
	geomod::planar::HexapolarGrid grid(1, 4);
	for(index_type i = 0; i < grid.NumPoints(); ++i){
		//std::cout << i << "\t" << grid.GetPoint(i) << std::endl;
		std::cout << "{" << grid.GetPoint(i)[0] << "," << grid.GetPoint(i)[1] << "}," << std::endl;
	}



	return 0;
}
