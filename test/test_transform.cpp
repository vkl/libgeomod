#include <geomod/affine/vector.h>
#include <geomod/affine/point.h>
#include <geomod/affine/aabb.h>
#include <geomod/affine/ray.h>
#include <geomod/affine/transform.h>
#include <geomod/util/matrix.h>
#include <iostream>

using geomod::coord_type;
using geomod::affine::Point3;
using geomod::affine::Vector3;
using geomod::affine::Transform3;
typedef geomod::util::TMatrix4<coord_type> Matrix4;

int main(int argc, char *argv[]){
	Transform3 x1(Vector3(0, 0, -3));
	Transform3 x2(Vector3::X(), 27);
	Transform3 x3(Vector3::Y(), 36);

	std::cout << x1 << std::endl;
	std::cout << x2 << std::endl;
	std::cout << x3 << std::endl;

	Transform3 modelview(
		x1 * x2 * x3
	);
	std::cout << modelview << std::endl;
	Matrix4 mv(modelview);
	std::cout << mv << std::endl;

	Matrix4 p(Matrix4::Perspective(45, (coord_type)1024 / 768, 0.1, 100));
	std::cout << p << std::endl;
	
	Matrix4 mvp(Matrix4::Multiply(p, mv));
	std::cout << mvp << std::endl;

	Point3 pa(10, 20, 30);
	std::cout << modelview(pa) << std::endl;
	std::cout << Transform3::Inverse(modelview)(modelview(pa)) << std::endl;

	return 0;
}
