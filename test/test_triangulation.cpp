#include <geomod/planar/mesh.h>
#include <geomod/planar/circle.h>
#include <geomod/planar/rectangle.h>
#include <cstdio>

using geomod::index_type;
using geomod::coord_type;
using geomod::affine::Point2;
using geomod::affine::Vector2;
using geomod::planar::Shape;
using geomod::planar::Circle;
using geomod::planar::Rectangle;
using geomod::planar::Polygon;
using geomod::planar::Mesh;

int main(int argc, char *argv[]){
	Shape* shape = new Circle(1.0);
	std::vector<Point2> samples;
	shape->GetSamples(100, Shape::SAMPLED_APPROX, samples);

	Polygon* boundary = shape->AsPolygon();
	std::vector<Point2> vertices(boundary->GetVertices());
	vertices.insert(vertices.end(), samples.begin(), samples.end());
	Mesh *mesh = Mesh::Triangulate(vertices);
	
	printf("%%PS\n");
	printf("306 306 translate\n");
	printf("72 3 mul dup scale\n");
	printf("0.001 setlinewidth\n");

	for(int i = 0; i < mesh->NumVertices(); ++i){
		printf("%f %f %f 0 360 arc fill\n",
			mesh->GetVertex(i)[0],
			mesh->GetVertex(i)[1],
			0.01
		);
	}
	for(int i = 0; i < mesh->NumFaces(); ++i){
		Mesh::vertex_index vi[3];
		mesh->GetFace(i, vi[0], vi[1], vi[2]);
		const Point2 &v0 = mesh->GetVertex(vi[0]);
		const Point2 &v1 = mesh->GetVertex(vi[1]);
		const Point2 &v2 = mesh->GetVertex(vi[2]);
		const Point2 a(Point2::Interpolate(v0, Point2::Interpolate(v1, v2, 0.5), 0.05));
		const Point2 b(Point2::Interpolate(v1, Point2::Interpolate(v2, v0, 0.5), 0.05));
		const Point2 c(Point2::Interpolate(v2, Point2::Interpolate(v0, v1, 0.5), 0.05));
		printf("%f %f moveto %f %f lineto %f %f lineto closepath gsave 0.5 setgray fill grestore stroke\n",
			a[0], a[1],
			b[0], b[1],
			c[0], c[1]
		);
	}

	printf("showpage\n");

	delete mesh;
	delete boundary;
	delete shape;

	return 0;
}
