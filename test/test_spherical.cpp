#include <geomod/surface/spherical.h>
#include <cstdio>

using namespace geomod;
using namespace geomod::affine;

int main(int argc, char *argv[]){
	geomod::surface::Spherical c(0);
	std::vector<Point3> points;
	c.GetSamples(100, points);
	printf("OFF\n%d 0 0\n", (int)points.size());
	for(size_t i = 0; i < points.size(); ++i){
		printf("%g %g %g\n", points[i][0], points[i][1], points[i][2]);
	}
	return 0;
}
