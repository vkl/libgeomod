#include <geomod/affine/vector.h>
#include <geomod/affine/point.h>
#include <geomod/affine/transform.h>
#include <geomod/planar/region.h>
#include <geomod/planar/rectangle.h>
#include <geomod/planar/polygon.h>
#include <iostream>

using geomod::coord_type;
using geomod::complex_type;
using geomod::affine::Point2;
using geomod::affine::Vector2;
using geomod::affine::Transform2;
using geomod::planar::Shape;
using geomod::planar::Region;
using geomod::planar::Polygon;
using geomod::planar::Rectangle;

int main(int argc, char *argv[]){
	Rectangle rect(3.0, 2.0);
	Transform2 xform = Transform2::Translate(Vector2(5, 6)) * Transform2::Rotate(30);
	
	Polygon *poly0 = rect.AsPolygon();
	Polygon *poly1 = poly0->Clone(); poly1->Transform(xform);
	Region *reg1 = rect.Transformed(xform);
	Region *reg2 = poly1->Transformed(Transform2::Translate(Vector2::Zero()));
	
	Vector2 f(-2.3, 3.2);
	const Shape::FourierTransformable *ftp = poly1->GetFourierTransformable();
	const Shape::FourierTransformable *ftr = reg1->GetFourierTransformable();
	const Shape::FourierTransformable *ft2 = reg2->GetFourierTransformable();

	complex_type zp = ftp->FourierTransform(f);
	complex_type zr = ftr->FourierTransform(f);
	complex_type z2 = ft2->FourierTransform(f);

	std::cout << zp << std::endl;
	std::cout << zr << std::endl;
	std::cout << z2 << std::endl;

	delete reg2;
	delete reg1;
	delete poly1;
	delete poly0;
	return 0;
}
