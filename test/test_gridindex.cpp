#include <geomod/scene/gridindex.h>

#include <cmath>
#include <cstdlib>
#include <iostream>
#include <vector>

using namespace geomod;
using namespace geomod::affine;

double frand(){
	return (double)rand() / (double)RAND_MAX;
}
Point3 randpt(){
	return Point3(10*frand(), 10*frand(), 10*frand());
}

int main(int argc, char *argv[]){
	int seed = 0;
	if(argc > 1){
		seed = atoi(argv[1]);
	}
	srand(seed);

	AABB3 domain(Point3(0, 0, 0), Point3(10, 10, 10));
	geomod::scene::GridIndex3<uint32_t, int> index(domain, 8, 8, 8);

	const int n = 10000;
	std::vector<Point3> p;
	for(int i = 0; i < n; ++i){
		for(int j = 0; j < rand()%4; ++j){
			p.push_back(randpt());
			index.insert(p.back(), p.size()-1);
		}
	}

	const double dist = 2;
	std::vector<int> result;
	Point3 q(randpt());
	index.query(q, dist, result);

	for(int i = 0; i < n; ++i){
		double d = (q-p[i]).NormInf();
		char ind_expected = (d < dist ? 'X' : ' ');
		char ind = ' ';
		for(int j = 0; j < result.size(); ++j){
			if(result[j] == i){ ind = 'X'; break; }
		}
		if(ind != ind_expected){
			std::cout << i << "\t" << p[i] << "\t" << d << "\t" << ind << "\t" << ind_expected << std::endl;
		}
	}

	return 0;
}
