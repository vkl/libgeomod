#include <geomod/affine/vector.h>
#include <geomod/affine/point.h>
#include <geomod/planar/polygon.h>
#include <iostream>
#include <cassert>

using geomod::coord_type;
using geomod::affine::Point2;
using geomod::affine::Vector2;
using geomod::planar::Polygon;

void print_poly(const Polygon &poly){
	for(size_t i = 0; i < poly.size(); ++i){
		std::cout << "  " << poly[i] << std::endl;
	}
}

void check_result(
	const coord_type &dist, const coord_type &expected_dist,
	const Vector2 &grad, const Vector2 &expected_grad
){
	assert(std::abs(dist - expected_dist) < 1e-10);
	assert(std::abs(grad[0] - expected_grad[0]) < 1e-10);
	assert(std::abs(grad[1] - expected_grad[1]) < 1e-10);
}

void test_square(){
	Polygon::vertex_vector v;
	v.emplace_back(0, 0);
	v.emplace_back(1, 0);
	v.emplace_back(1, 1);
	v.emplace_back(0, 1);
	Polygon poly(v);

	coord_type dist;
	Vector2 grad;

	coord_type expected_dist;
	Vector2 expected_grad;

	dist = poly.SignedDistance(Point2(2, 2), &grad);
	check_result(dist, M_SQRT2, grad, Vector2(M_SQRT1_2, M_SQRT1_2));

	dist = poly.SignedDistance(Point2(0.5, 0.1), &grad);
	check_result(dist, -0.1, grad, Vector2(0, -1));

	dist = poly.SignedDistance(Point2(0.5, -0.1), &grad);
	check_result(dist, 0.1, grad, Vector2(0, -1));

	dist = poly.SignedDistance(Point2(0.5, 1.1), &grad);
	check_result(dist, 0.1, grad, Vector2(0, 1));

	dist = poly.SignedDistance(Point2(0.5, 0.9), &grad);
	check_result(dist, -0.1, grad, Vector2(0, 1));
}

void test_reflex(){
	Polygon::vertex_vector v;
	v.emplace_back(0, 0);
	v.emplace_back(2, 0);
	v.emplace_back(2, 1);
	v.emplace_back(1, 1);
	v.emplace_back(1, 2);
	v.emplace_back(0, 2);
	Polygon poly(v);

	coord_type dist;
	Vector2 grad;

	coord_type expected_dist;
	Vector2 expected_grad;

	dist = poly.SignedDistance(Point2(1.2, 1.1), &grad);
	check_result(dist, 0.1, grad, Vector2(0, 1));

	dist = poly.SignedDistance(Point2(0.9, 0.9), &grad);
	check_result(dist, -0.1*M_SQRT2, grad, Vector2(M_SQRT1_2, M_SQRT1_2));
}

int main(int argc, char *argv[]){
	test_square();
	test_reflex();
	return 0;
}
