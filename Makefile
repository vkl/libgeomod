CXX = c++
CXXFLAGS = -std=c++17 -fPIC
CXXINC = -I./include -I../CDT/CDT/include

BUILD_DIR = build_output

SRCS = $(shell find src -name '*.cpp')
DIRS = $(shell find src -type d | sed 's/src/./g' ) 
OBJS = $(patsubst src/%.cpp, $(BUILD_DIR)/%.o, $(SRCS))

all: outputdirs lib tests

lib: $(OBJS)
	$(AR) -crs $(BUILD_DIR)/libgeomod.a $^

$(BUILD_DIR)/%.o: src/%.cpp
	$(CXX) -c $(CXXFLAGS) $(CXXINC) $< -o $@

tests: \
	$(BUILD_DIR)/test_types \
	$(BUILD_DIR)/test_tensor \
	$(BUILD_DIR)/test_quaternion \
	$(BUILD_DIR)/test_affine \
	$(BUILD_DIR)/test_transform \
	$(BUILD_DIR)/test_plane \
	$(BUILD_DIR)/test_region \
	$(BUILD_DIR)/test_spherical \
	$(BUILD_DIR)/test_polygon_signed_distance \
	$(BUILD_DIR)/test_polygon_clip \
	$(BUILD_DIR)/test_triangulation \
	$(BUILD_DIR)/test_extrusion \
	$(BUILD_DIR)/test_gridindex \
	$(BUILD_DIR)/test_grid

$(BUILD_DIR)/test_types: test/test_types.cpp
	$(CXX) $(CXXFLAGS) $(CXXINC) $(TESTLIBS) $< -o $@
$(BUILD_DIR)/test_tensor: test/test_tensor.cpp
	$(CXX) $(CXXFLAGS) $(CXXINC) $(TESTLIBS) $< -o $@
$(BUILD_DIR)/test_quaternion: test/test_quaternion.cpp
	$(CXX) $(CXXFLAGS) $(CXXINC) $(TESTLIBS) $< -o $@
$(BUILD_DIR)/test_affine: test/test_affine.cpp
	$(CXX) $(CXXFLAGS) $(CXXINC) $(TESTLIBS) $< -o $@
$(BUILD_DIR)/test_transform: test/test_transform.cpp
	$(CXX) $(CXXFLAGS) $(CXXINC) $(TESTLIBS) $< -o $@
$(BUILD_DIR)/test_plane: test/test_plane.cpp
	$(CXX) $(CXXFLAGS) $(CXXINC) $(TESTLIBS) $< -o $@ -L$(BUILD_DIR) -lgeomod
$(BUILD_DIR)/test_region: test/test_region.cpp
	$(CXX) $(CXXFLAGS) $(CXXINC) $(TESTLIBS) $< -o $@ -L$(BUILD_DIR) -lgeomod
$(BUILD_DIR)/test_spherical: test/test_spherical.cpp
	$(CXX) $(CXXFLAGS) $(CXXINC) $(TESTLIBS) $< -o $@ -L$(BUILD_DIR) -lgeomod
$(BUILD_DIR)/test_polygon_signed_distance: test/test_polygon_signed_distance.cpp
	$(CXX) $(CXXFLAGS) $(CXXINC) $(TESTLIBS) $< -o $@ -L$(BUILD_DIR) -lgeomod
$(BUILD_DIR)/test_polygon_clip: test/test_polygon_clip.cpp
	$(CXX) $(CXXFLAGS) $(CXXINC) $(TESTLIBS) $< -o $@ -L$(BUILD_DIR) -lgeomod
$(BUILD_DIR)/test_triangulation: test/test_triangulation.cpp
	$(CXX) $(CXXFLAGS) $(CXXINC) $(TESTLIBS) $< -o $@ -L$(BUILD_DIR) -lgeomod
$(BUILD_DIR)/test_extrusion: test/test_extrusion.cpp
	$(CXX) $(CXXFLAGS) $(CXXINC) $(TESTLIBS) $< -o $@ -L$(BUILD_DIR) -lgeomod
$(BUILD_DIR)/test_gridindex: test/test_gridindex.cpp
	$(CXX) $(CXXFLAGS) $(CXXINC) $(TESTLIBS) $< -o $@
$(BUILD_DIR)/test_grid: test/test_grid.cpp
	$(CXX) $(CXXFLAGS) $(CXXINC) $(TESTLIBS) $< -o $@ -L$(BUILD_DIR) -lgeomod

usage: \
	$(BUILD_DIR)/raytrace

$(BUILD_DIR)/raytrace: usage/raytrace.cpp
	$(CXX) $(CXXFLAGS) $(CXXINC) $(TESTLIBS) $< -o $@ -L$(BUILD_DIR) -lgeomod

outputdirs:
	mkdir -p $(BUILD_DIR)
	for dir in $(DIRS); do mkdir -p $(BUILD_DIR)/$$dir; done
