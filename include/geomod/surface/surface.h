#ifndef GEOMOD_SURFACE_SURFACE_H_
#define GEOMOD_SURFACE_SURFACE_H_

#include <geomod/types.h>
#include <geomod/affine/vector.h>
#include <geomod/affine/point.h>
#include <geomod/affine/ray.h>
#include <geomod/surface/mesh.h>
#include <geomod/planar/region.h>
#include <geomod/surface/parameterization.h>

namespace geomod{
namespace surface{

class Surface:
	public Parameterizable,
	public Parameterization
{
public:
	virtual ~Surface(){}

	// Returns only the first intersection for positive ray parameter.
	// x may be modified even if false is returned.
	virtual bool RayIntersection(const affine::Ray3 &r, affine::Ray3::Intersection &x) const = 0;

	virtual Mesh* AsMesh(const planar::Region *reg, const coord_type &tol) const = 0;

	virtual coord_type Orient(const affine::Point3 &q) const = 0;

	size_t NumCharts() const{ return 1; }
	const Parameterization* GetChartParameterization(index_type ichart) const{ return this; }

	virtual affine::Point3 operator()(const affine::Point2 &uv) const = 0;
	virtual affine::Point2 operator()(const affine::Point3 &r) const = 0;
	virtual bool RayIntersection(const affine::Ray3 &r, std::vector<affine::Ray3::Intersection> &x) const = 0;
	virtual void Neighborhood(const affine::Point2 &uv, DifferentialGeometry &dg) const = 0;
	affine::AABB2 Bounds() const{ return affine::AABB2(); }

	virtual Surface* Clone() const = 0;
};

} // namespace surface
} // namespace geomod

#endif // GEOMOD_SURFACE_SURFACE_H_
