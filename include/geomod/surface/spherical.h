#ifndef GEOMOD_SURFACE_SPHERICAL_H_
#define GEOMOD_SURFACE_SPHERICAL_H_

#include <geomod/types.h>
#include <geomod/affine/point.h>
#include <geomod/affine/aabb.h>
#include <vector>

namespace geomod{
namespace surface{

class Spherical{
	coord_type maxangle; // degrees
public:
	Spherical(const coord_type &polar_angle_limit_degrees):maxangle(polar_angle_limit_degrees){}
	void GetSamples(size_t n, std::vector<affine::Point3> &samples) const;
};

} // namespace surface
} // namespace geomod

#endif // GEOMOD_SURFACE_SPHERICAL_H_
