#ifndef GEOMOD_SURFACE_PLANE_H_
#define GEOMOD_SURFACE_PLANE_H_

#include <geomod/types.h>
#include <geomod/affine/vector.h>
#include <geomod/affine/point.h>
#include <geomod/affine/ray.h>
#include <geomod/affine/transform.h>
#include <geomod/surface/surface.h>

namespace geomod{
namespace surface{

class Plane : public Surface{
public:
	Plane(){}

	bool RayIntersection(const affine::Ray3 &r, affine::Ray3::Intersection &x) const;

	Mesh* AsMesh(const planar::Region *reg, const coord_type &tol = 1e-3) const;

	coord_type Orient(const affine::Point3 &q) const{
		return q[2];
	}

	affine::Point3 operator()(const affine::Point2 &uv) const{ return affine::Point3(uv[0], uv[1], 0); }
	affine::Point2 operator()(const affine::Point3 &r) const{ return affine::Point2(r[0], r[1]); };
	bool RayIntersection(const affine::Ray3 &r, std::vector<affine::Ray3::Intersection> &x) const{
		affine::Ray3::Intersection i;
		if(RayIntersection(r, i)){
			x.push_back(i);
			return true;
		}
		return false;
	}
	void Neighborhood(const affine::Point2 &uv, DifferentialGeometry &dg) const{
		dg.dpdu = affine::Vector3::X();
		dg.dpdv = affine::Vector3::Y();
		dg.II[0] = 1;
		dg.II[1] = 0;
		dg.II[2] = 1;
		dg.orientation = 1;
	}

	Plane* Clone() const{ return new Plane(); }

	// Given a plane in point normal form (i.e. n.(r-p) = 0), return
	// the transform taking the z=0 plane to the given plane. The
	// rotation in the transform is the minimal rotation.
	static affine::Transform3 ToTransform(const affine::Point3 &p, const affine::Vector3 &n);

	// Given another plane posed by `t` (the z=0 plane is placed
	// using the `t` transform), return the intersection with
	// this plane as a ray in this plane's coordinate system (in the z=0 plane).
	bool PlaneIntersection(const affine::Transform3 &t, affine::Ray2 &x) const;
};

} // namespace surface
} // namespace geomod

#endif // GEOMOD_SURFACE_PLANE_H_
