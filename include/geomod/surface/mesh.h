#ifndef GEOMOD_SURFACE_MESH_H_
#define GEOMOD_SURFACE_MESH_H_

#include <geomod/types.h>
#include <geomod/affine/point.h>
#include <geomod/affine/aabb.h>
#include <vector>

namespace geomod{
namespace surface{

// This class represents a surface mesh in 3D.

class Mesh{
public:
	typedef affine::Point3 vertex_type;
	typedef std::vector<vertex_type> vertex_vector;
	typedef std::vector<index_type> index_vector;
private:
	vertex_vector vert;
	index_vector tri; // assume triangular elements, sets of 3 vertex indices for each triangle
	index_vector neigh; // sets of 3 triangle indices for each triangle
public:
	Mesh(const vertex_vector &vertices, const index_vector &triangles):
		vert(vertices),
		tri(triangles)
	{}
	~Mesh(){}

	size_t NumVertices() const{ return vert.size(); }
	size_t NumEdges() const;
	size_t NumFaces() const{ return tri.size()/3; }
	size_t NumConnectedComponents() const;

	const vertex_type& GetVertex(index_type vi) const;
	void GetFace(index_type f, index_type &v0, index_type &v1, index_type &v2) const;

	bool Closed() const;
	bool Convex() const;

	affine::AABB3 Bounds() const;

	void GetElements(vertex_vector &vertices, index_vector &triangle_indices) const{
		vertices = vert;
		triangle_indices = tri;
	}

	static Mesh* ConvexHull(const vertex_vector &points);

	enum Format{
		MESH_FORMAT_OFF, // currently the only supported format
		MESH_FORMAT_OBJ,
		MESH_FORMAT_STL,
		MESH_FORMAT_PLY
	};
	int Serialize(Format format, const char *filename) const;
};

} // namespace surface
} // namespace geomod

#endif // GEOMOD_SURFACE_MESH_H_
