#ifndef GEOMOD_SURFACE_PARAMETERIZATION_H_
#define GEOMOD_SURFACE_PARAMETERIZATION_H_

#include <geomod/types.h>
#include <geomod/affine/vector.h>
#include <geomod/affine/point.h>
#include <geomod/affine/aabb.h>
#include <geomod/affine/ray.h>
#include <vector>

namespace geomod{
namespace surface{

// The Parameterization class represents a surface parameterization.
// It is assumed that the parameterization is (a) invertible, and (b)
// rectangular in parameter domain.

class Parameterization{
public:
	virtual ~Parameterization(){}

	// Evaluate the parameterization; going from 2D to 3D space
	virtual affine::Point3 operator()(const affine::Point2 &uv) const = 0;

	// Evaluate the inverse parameterization
	virtual affine::Point2 operator()(const affine::Point3 &r) const = 0;

	// Compute intersection of the parameterization with the ray.
	// Returns only positive ray parameter intersections, sorted by increasing ray parameter
	virtual bool RayIntersection(const affine::Ray3 &r, std::vector<affine::Ray3::Intersection> &x) const = 0;
	// Returns only the first intersection; overload this for efficiency.
	// x may be modified even if false is returned.
	virtual bool RayIntersection(const affine::Ray3 &r, affine::Ray3::Intersection &x) const{
		std::vector<affine::Ray3::Intersection> xv;
		bool ret = this->RayIntersection(r, xv);
		if(ret){
			x = xv.front();
		}
		return ret;
	}
	// Returns number of positive ray parameter intersections; overload this for efficiency.
	virtual unsigned RayIntersections(const affine::Ray3 &r) const{
		std::vector<affine::Ray3::Intersection> xv;
		this->RayIntersection(r, xv);
		return xv.size();
	}

	struct DifferentialGeometry{
		affine::Vector3 dpdu, dpdv;
		coord_type II[3]; // lower triangle of second fundamental form, (L, M, N)
		signed orientation; // 1 for positive, -1 for negative
	};
	virtual void Neighborhood(const affine::Point2 &uv, DifferentialGeometry &dg) const = 0;

	virtual affine::AABB2 Bounds() const = 0;

	typedef std::vector<affine::Point2> uv_boundary;
	virtual uv_boundary GetBoundary(const coord_type &tol = 1e-3) const{
		return uv_boundary();
	}
};

// This is the interface class for simply-connected regions on 3D
// manifolds that support parameterization. It is assumed that the
// boundary of the region is piecewise smooth, such that each
// piece corresponds to a parameter chart.

class Parameterizable{
public:
	virtual ~Parameterizable(){}

	typedef std::vector<affine::Point3> chart_outline;

	virtual size_t NumCharts() const = 0;

	// For purposes of rendering
	virtual chart_outline GetChartOutline(index_type ichart, const coord_type &tol = 1e-3) const{
		return chart_outline();
	}

	virtual const Parameterization* GetChartParameterization(index_type ichart) const = 0;

	// Returns only the first intersection; overload this for efficiency.
	// x may be modified even if -1 is returned.
	virtual index_type FirstRayIntersection(const affine::Ray3 &r, affine::Ray3::Intersection &x) const{
		index_type ichart_ret = -1;
		for(size_t ichart = 0; ichart < NumCharts(); ++ichart){
			const Parameterization *param = this->GetChartParameterization(ichart);
			affine::Ray3::Intersection xc;
			if(!param->RayIntersection(r, xc) || xc.t <= 0){ continue; }
			if(ichart_ret < 0 || xc < x){
				x = xc;
				ichart_ret = ichart;
			}
		}
		return ichart_ret;
	}
};

} // namespace surface
} // namespace geomod

#endif // GEOMOD_SURFACE_PARAMETERIZATION_H_
