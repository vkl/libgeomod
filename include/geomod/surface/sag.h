#ifndef GEOMOD_SURFACE_SAG_H_
#define GEOMOD_SURFACE_SAG_H_

#include <geomod/types.h>
#include <geomod/affine/ray.h>
#include <geomod/planar/region.h>
#include <geomod/surface/surface.h>

namespace geomod{
namespace surface{

class Sag : public Surface{
public:
	class Function{
	public:
		virtual ~Function(){}
		virtual coord_type Evaluate(const affine::Point2 &r, affine::Vector2 *gradient = NULL, coord_type *II = NULL) const = 0;
		virtual onedim::Interval Bounds(const affine::AABB2 &bounds) const = 0;

		virtual Function* Clone() const = 0;

		virtual coord_type BaseCurvature() const{ return 0; }
	};
	const Function *func;
	const planar::Region *reg;
public:
	Sag(const Function *func, const planar::Region *region);
	~Sag();
	
	bool RayIntersection(const affine::Ray3 &r, affine::Ray3::Intersection &x) const;

	Mesh* AsMesh(const planar::Region *reg, const coord_type &tol) const;

	coord_type Orient(const affine::Point3 &q) const;

	affine::Point3 operator()(const affine::Point2 &uv) const;
	affine::Point2 operator()(const affine::Point3 &r) const;
	bool RayIntersection(const affine::Ray3 &r, std::vector<affine::Ray3::Intersection> &x) const;
	void Neighborhood(const affine::Point2 &uv, DifferentialGeometry &dg) const;

	Sag* Clone() const;

	const Function *GetFunction() const{ return func; }
	const planar::Region *GetRegion() const{ return reg; }
};

} // namespace surface
} // namespace geomod

#endif // GEOMOD_SURFACE_SAG_H_
