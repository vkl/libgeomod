#ifndef GEOMOD_VOLUME_SOLID_H_
#define GEOMOD_VOLUME_SOLID_H_

#include <vector>
#include <complex>
#include <geomod/types.h>
#include <geomod/affine/vector.h>
#include <geomod/affine/point.h>
#include <geomod/affine/aabb.h>
#include <geomod/surface/parameterization.h>

namespace geomod{

namespace surface{
	class Mesh;
}

namespace volume{

// This represents a volume of space that has a single connected component.
// It is assumed to have piecewise smooth surface.

class Solid:
	public surface::Parameterizable
{
public:
	typedef affine::Point3  point_type;
	typedef affine::Vector3 vector_type;
	typedef affine::AABB3   bound_type;

	virtual coord_type SurfaceArea() const = 0;
	virtual coord_type Volume() const = 0;
	virtual point_type Centroid() const = 0;
	
	virtual point_type Extremum(const vector_type &dir) const = 0;
	virtual point_type Nearest(const point_type &p) const = 0;
	virtual bound_type Bounds() const = 0;
	
	virtual ~Solid(){}

	virtual Solid* Clone() const = 0;

	virtual bool Contains(const point_type &p) const = 0;
	virtual bool Convex() const{ return false; }

	virtual int EulerCharacteristic() const{ return 2; }
	
	virtual surface::Mesh* AsMesh(const coord_type &tol = 1e-3) const = 0;
	virtual Solid* Offset(const coord_type &dist, const coord_type &tol = 0) const{ return NULL; }
};

} // namespace volume
} // namespace geomod

#endif // GEOMOD_VOLUME_SOLID_H_
