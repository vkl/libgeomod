#ifndef GEOMOD_VOLUME_MESH_H_
#define GEOMOD_VOLUME_MESH_H_

#include <geomod/types.h>
#include <geomod/affine/point.h>
#include <geomod/affine/aabb.h>
#include <vector>

namespace geomod{
namespace volume{

// This class represents a volume mesh in 3D (e.g. tetrahedral mesh).

class Mesh{
public:
	typedef std::vector<affine::Point3> vertex_vector;
private:
	vertex_vector v;
public:
	virtual ~Mesh(){}

	size_t NumVertices() const;
	size_t NumEdges() const;
	size_t NumFaces() const;
	size_t NumConnectedComponents() const;

	bool Closed() const;
	bool Convex() const;

	affine::AABB3 Bounds() const;
};

} // namespace volume
} // namespace geomod

#endif // GEOMOD_VOLUME_MESH_H_
