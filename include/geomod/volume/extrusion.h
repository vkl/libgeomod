#ifndef GEOMOD_VOLUME_EXTRUSION_H_
#define GEOMOD_VOLUME_EXTRUSION_H_

#include <vector>
#include <complex>
#include <geomod/volume/solid.h>
#include <geomod/planar/shape.h>

namespace geomod{
namespace volume{

class Extrusion : public Solid{
public:
	typedef affine::Point3  point_type;
	typedef affine::Vector3 vector_type;
	typedef affine::AABB3   bound_type;
	typedef planar::Shape   shape_type;

	class SurfaceProfile{
	public:
		virtual ~SurfaceProfile(){}
		virtual coord_type Evaluate(const affine::Point2 &r, affine::Vector2 *gradient = NULL, coord_type *II = NULL) const = 0;
		virtual onedim::Interval Bounds(const affine::AABB2 &bounds) const = 0;

		virtual SurfaceProfile* Clone() const = 0;
	};

private:
	class BaseParameterization : public surface::Parameterization{
		const Extrusion *e;
		unsigned isurf;
	public:
		BaseParameterization(const Extrusion *parent, unsigned isurf): e(parent), isurf(isurf) {}
		affine::Point3 operator()(const affine::Point2 &uv) const;
		affine::Point2 operator()(const affine::Point3 &r) const;
		bool RayIntersection(const affine::Ray3 &r, std::vector<affine::Ray3::Intersection> &x) const;
		void Neighborhood(const affine::Point2 &uv, DifferentialGeometry &dg) const;
		affine::AABB2 Bounds() const;
		uv_boundary GetBoundary(const coord_type &tol = 1e-3) const;
	};
	class EdgeParameterization : public surface::Parameterization{
		const Extrusion *e;
		index_type ichart;
	public:
		EdgeParameterization(const Extrusion *parent, index_type ichart): e(parent), ichart(ichart) {}
		affine::Point3 operator()(const affine::Point2 &uv) const;
		affine::Point2 operator()(const affine::Point3 &r) const;
		bool RayIntersection(const affine::Ray3 &r, std::vector<affine::Ray3::Intersection> &x) const;
		void Neighborhood(const affine::Point2 &uv, DifferentialGeometry &dg) const;
		affine::AABB2 Bounds() const;
		uv_boundary GetBoundary(const coord_type &tol = 1e-3) const;
	};
	shape_type *shape;
	vector_type h;
	const SurfaceProfile *surface[2];
	BaseParameterization paramb[2];
	std::vector<EdgeParameterization> parame;
public:
	Extrusion(
		const shape_type *shape, const vector_type &v,
		const SurfaceProfile *surface0 = NULL, // owned pointer
		const SurfaceProfile *surface1 = NULL  // owned pointer
		// The surface0 and surface1 arguments, if provided, are stored
		// directly in the `surface` member and are deleted in the destructor.
	);
	~Extrusion();

	const shape_type* base() const{ return shape; }
	const vector_type& offset() const{ return h; }
	const SurfaceProfile* profile(unsigned i) const{ return surface[i]; }

	// These following three functions are only correct if surface[] is NULL
	coord_type SurfaceArea() const;
	coord_type Volume() const;
	point_type Centroid() const;
	
	// These following two functions are only correct if surface[] is NULL
	point_type Extremum(const vector_type &dir) const;
	point_type Nearest(const point_type &p) const;
	bound_type Bounds() const;

	size_t NumCharts() const;
	const surface::Parameterization* GetChartParameterization(index_type ichart) const;
	surface::Parameterizable::chart_outline GetChartOutline(index_type ichart, const coord_type &tol = 1e-3) const;

	Extrusion* Clone() const;

	// The following function is only strictly correct if surface[] is NULL
	bool Contains(const point_type &p) const;
	bool Convex() const;

	int EulerCharacteristic() const{ return 2; }

	surface::Mesh* AsMesh(const coord_type &tol = 1e-3) const;
	Solid* Offset(const coord_type &dist, const coord_type &tol = 0) const;
};

} // namespace volume
} // namespace geomod

#endif // GEOMOD_VOLUME_EXTRUSION_H_
