#ifndef GEOMOD_VOLUME_SPHERE_H_
#define GEOMOD_VOLUME_SPHERE_H_

#include <vector>
#include <complex>
#include <geomod/volume/solid.h>

namespace geomod{
namespace volume{

class Sphere : public Solid{
public:
	typedef Solid::point_type  point_type;
	typedef Solid::vector_type vector_type;
	typedef Solid::bound_type  bound_type;
private:
	class Parameterization : public surface::Parameterization{
		const Sphere *s;
	public:
		Parameterization(const Sphere *parent):s(parent){}
		affine::Point3 operator()(const affine::Point2 &uv) const;
		affine::Point2 operator()(const affine::Point3 &r) const;
		bool RayIntersection(const affine::Ray3 &r, std::vector<affine::Ray3::Intersection> &x) const;
		void Neighborhood(const affine::Point2 &uv, DifferentialGeometry &dg) const;
		affine::AABB2 Bounds() const;
		uv_boundary GetBoundary(const coord_type &tol = 1e-3) const;
	};

	coord_type r;
	Parameterization param;
public:
	Sphere(const coord_type &radius): r(radius), param(this){}

	const coord_type& radius() const{ return r; }

	coord_type SurfaceArea() const;
	coord_type Volume() const;
	point_type Centroid() const;
	
	point_type Extremum(const vector_type &dir) const;
	point_type Nearest(const point_type &p) const;
	bound_type Bounds() const;

	size_t NumCharts() const;
	const surface::Parameterization* GetChartParameterization(index_type ichart) const;
	index_type RayIntersection(const affine::Ray3 &r, affine::Ray3::Intersection &x) const;

	Sphere* Clone() const;

	bool Contains(const point_type &p) const;
	bool Convex() const{ return true; }

	int EulerCharacteristic() const{ return 2; }
	
	surface::Mesh* AsMesh(const coord_type &tol = 1e-3) const;
	Sphere* Offset(const coord_type &dist, const coord_type &tol = 0) const;
};

} // namespace volume
} // namespace geomod

#endif // GEOMOD_VOLUME_SPHERE_H_
