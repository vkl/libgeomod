#ifndef GEOMOD_UTIL_QUATERNION_H_
#define GEOMOD_UTIL_QUATERNION_H_

#include <geomod/affine/vector.h>

namespace geomod {
namespace util {

// This class represents a quaternion with real part w, and vector
// part v.

template <typename T>
class TQuaternion{
public:
	typedef T value_type;
	typedef affine::TVector3<value_type> vector_type;

	vector_type v;
	value_type w;
public:
	TQuaternion() : v(0, 0, 0), w(0){}
	TQuaternion(const vector_type &v, const value_type &w = 0) : v(v), w(w){}

	const value_type  Scalar() const{ return w; }
	const vector_type Vector() const{ return v; }

	bool Equals(const TQuaternion &q) const{
		return v == q.v && w == q.w;
	}
	TQuaternion& Normalize(){
		coord_type ia = coord_type(1) / std::sqrt(v.NormSq() + w*w);
		v *= ia; w *= ia;
		return *this;
	}
	TQuaternion &operator+=(const TQuaternion &q){ v += q.v; w += q.w; return *this; }
	friend TQuaternion operator+(const TQuaternion &q1, const TQuaternion &q2){
		TQuaternion<value_type> ret(q1);
		return ret += q2;
	}
	TQuaternion &operator-=(const TQuaternion &q){ v -= q.v; w -= q.w; return *this; }
	friend TQuaternion operator-(const TQuaternion &q1, const TQuaternion &q2){
		TQuaternion ret(q1);
		return ret -= q2;
	}
	TQuaternion operator-() const {
		return TQuaternion(-v, -w);
	}
	TQuaternion &operator*=(const value_type &s){ v *= s; w *= s; return *this; }
	TQuaternion operator*(const value_type &s) const {
		TQuaternion<value_type> ret(*this);
		ret.v *= s;
		ret.w *= s;
		return ret;
	}

	TQuaternion &operator/=(const value_type &d){ v /= d; w /= d; return *this; }
	TQuaternion operator/(const value_type &d) const {
		TQuaternion<value_type> ret(*this);
		ret.v /= d;
		ret.w /= d;
		return ret;
	}
	TQuaternion Conjugate() const{ return TQuaternion<value_type>(-v, w); }

	static value_type Dot(const TQuaternion &q1, const TQuaternion &q2){
		return vector_type::Dot(q1.v, q2.v) + q1.w*q2.w;
	}

	// Conversion from an orthonormal frame to a quaternion, and vice versa
	// Assumes x, y, and z are normalized.
	static TQuaternion FromOrthonormalFrame(const vector_type &x, const vector_type &y, const vector_type &z){
		const value_type tr(x[0] + y[1] + z[2]);
		if(tr >= 0){
			const value_type r = std::sqrt(1+tr);
			const value_type s = value_type(1) / (2*r);
			return TQuaternion(vector_type(
				(y[2] - z[1])*s,
				(z[0] - x[2])*s,
				(x[1] - y[0])*s),
				0.5*r);
		}else if(x[0] > y[1] && x[0] > z[2]){
			const value_type r = std::sqrt(1 + x[0] - y[1] - z[2]);
			const value_type s = value_type(1) / (2*r);
			return TQuaternion(vector_type(
				0.5*r,
				(y[0] + x[1])*s,
				(x[2] + z[0])*s),
				(y[2] - z[1])*s);
		}else if(y[1] > z[2]){
			const value_type r = std::sqrt(1 - x[0] + y[1] - z[2]);
			const value_type s = value_type(1) / (2*r);
			return TQuaternion(vector_type(
				(y[0] + x[1])*s,
				0.5*r,
				(z[1] + y[2])*s),
				(z[0] - x[2])*s);
		}else{
			const value_type r = std::sqrt(1 - x[0] - y[1] + z[2]);
			const value_type s = value_type(1) / (2*r);
			return TQuaternion(vector_type(
				(x[2] + z[0])*s,
				(z[1] + y[2])*s,
				0.5*r),
				(x[1] - y[0])*s);
		}
	}
	TQuaternion(const vector_type &x, const vector_type &y){
		vector_type xn(x); xn.Normalize();
		vector_type yn(y); yn.Normalize();
		vector_type zn(vector_type::Cross(xn, yn));
		FromOrthonormalFrame(xn, yn, zn);
	}
	TQuaternion(const vector_type &xn, const vector_type &yn, const vector_type &zn){
		FromOrthonormalFrame(xn, yn, zn);
	}
	void AsMatrix(value_type *m) const {
		const value_type ww = w*w;
		const value_type xx = v[0]*v[0];
		const value_type yy = v[1]*v[1];
		const value_type zz = v[2]*v[2];
		const value_type xy = v[0]*v[1];
		const value_type xz = v[0]*v[2];
		const value_type yz = v[1]*v[2];
		const value_type wx = w*v[0];
		const value_type wy = w*v[1];
		const value_type wz = w*v[2];

		m[0] = ww + xx - yy - zz;
		m[1] = 2 * (xy + wz);
		m[2] = 2 * (xz - wy);
		
		m[3] = 2 * (xy - wz);
		m[4] = ww - xx + yy - zz;
		m[5] = 2 * (yz + wx);
		
		m[6] = 2 * (xz + wy);
		m[7] = 2 * (yz - wx);
		m[8] = ww - xx - yy + zz;
	}
};

template <typename T>
inline bool operator==(const TQuaternion<T> &a, const TQuaternion<T> &b){
	return a.Equals(b);
}

template <typename T>
inline TQuaternion<T> operator*(const TQuaternion<T> &q1, const TQuaternion<T> &q2){
	return TQuaternion<T>(
		q1.w * q2.v + q2.w * q1.v + affine::TVector3<T>::Cross(q1.v, q2.v),
		q1.w * q2.w - affine::TVector3<T>::Dot(q1.v, q2.v)
	);
}

template <typename T>
inline TQuaternion<T> operator*(const T &s, const TQuaternion<T> &q){
	return q * s;
}

template <typename T>
inline TQuaternion<T> Slerp(const T &t, const TQuaternion<T> &q1, const TQuaternion<T> &q2){
	T cs = TQuaternion<T>::Dot(q1, q2);
	if(cs > 0.9995){
		TQuaternion<T> ret((T(1) - t) * q1 + t * q2);
		ret.Normalize();
		return ret;
	}else{
		if(cs < -1){ cs = -1; }
		else if (cs > 1){ cs = 1; }
		T theta = std::acos(cs);
		T thetap = theta * t;
		TQuaternion<T> qperp(q2 - q1 * cs); qperp.Normalize();
		return q1 * std::cos(thetap) + qperp * std::sin(thetap);
	}
}

template <typename T>
inline std::ostream& operator<<(std::ostream &s, const TQuaternion<T> &q) {
	const typename TQuaternion<T>::vector_type &v = q.Vector();
	const typename TQuaternion<T>::value_type  &w = q.Scalar();
	s << '(' << w << '+' << v[0] << "i+" << v[1] << "j+" << v[2] << "k)";
	return s;
}

typedef TQuaternion<coord_type> Quaternion;


} // namespace util
} // namespace geomod

#endif  // GEOMOD_UTIL_QUATERNION_H_
