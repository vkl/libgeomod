#ifndef GEOMOD_UTIL_POLYNOMIAL_H_
#define GEOMOD_UTIL_POLYNOMIAL_H_

#include <vector>
#include <cmath>

namespace geomod{
namespace polynomial{

// Solves a*x^2 + 2*b*x + c == 0 for x. Note the 2 on the linear term.
// Returns the number of real roots.
template <typename T>
unsigned quadratic_equation(const T &a, const T &b, const T &c, T root[2]){
	if(NULL == root){ return -4; }
	if(0 == a){
		if(0 == b){
			// impossible to solve
			return 0;
		}else{
			root[0] = T(-0.5)*c/b;
		}
	}
	T disc(b*b - a*c);
	if(0 > disc){
		return 0;
	}else if(0 == disc){
		root[0] = -b/a;
		return 1;
	}else{
		T z(-b - std::copysign(std::sqrt(disc), b));
		root[0] = z/a;
		root[1] = c/z;
		return 2;
	}
}

// Solves c[i]*x^i for all real roots r.
template <typename T>
int monomial_roots_real(const std::vector<T> &c, std::vector<T> &r){
	return 0; // TODO
}

// Solves c[i]*B^n_i(x) for all real roots r, where B^n_i are
// the Bernstein polynomials of order n == c.size()-1
template <typename T>
int bernstein_roots_real(const std::vector<T> &c, std::vector<T> &r){
	return 0; // TODO
}

} // namespace polynomial
} // namespace geomod

#endif // GEOMOD_UTIL_POLYNOMIAL_H_
