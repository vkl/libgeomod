#ifndef GEOMOD_UTIL_ARCBALL_H_
#define GEOMOD_UTIL_ARCBALL_H_

#include <geomod/affine/vector.h>
#include <geomod/affine/point.h>
#include <geomod/affine/transform.h>
#include <geomod/util/quaternion.h>

namespace geomod {
namespace util {

class Arcball{
public:
	struct View{
		affine::Point3  cam_target; // target point, current view. May be mid-drag.
		affine::Vector3 cam_offset; // vector from target point to camera location.
		Quaternion q; // quaternion rotation for current view.
	};
protected:
	View current, predrag;

	// Mouse parameters, coordinates in range [0, 1] for full window extents
	affine::Point2 cursor, cursor_predrag;

	// Used for tweening animations
	affine::Point3 cam_target_prev;   // pre-animation camera target
	affine::Point3 cam_target_target; // post-animation camera target
	int anim_num_timesteps;
	int anim_timesteps_remaining;
	
	bool rotating, panning;

	affine::Vector3 MapPoint(const affine::Point2 &p);
public:
	Arcball();
	~Arcball();

	void GetView(View &view) const;
	void LoadView(const View &view);

	void UpdateCursor(const affine::Point2 &p); // Call this anytime mouse moves.

	bool Rotating() const;
	void RotationBegin();
	void RotationEnd();

	bool Panning() const;
	void PanBegin();
	void PanEnd();

	void Zoom(const coord_type &offset);
	
	affine::Transform3 ViewTransform() const; // The modelview matrix
	affine::Transform3 ViewRotation() const; // The rotation part of the modelview matrix
	coord_type CameraDistance() const; // return distance from camera to target point
	
	void AnimateToTarget(const affine::Point3 &target, int nsteps = 20);
	void IncrementAnimationTimestep();
};

} // namespace util
} // namespace geomod

#endif  // GEOMOD_UTIL_ARCBALL_H_
