#ifndef GEOMOD_UTIL_TENSOR_H_
#define GEOMOD_UTIL_TENSOR_H_

#include <geomod/affine/vector.h>
#include <geomod/affine/point.h>
#include <geomod/affine/ray.h>
#include <geomod/affine/transform.h>
#include <geomod/util/quaternion.h>
#include <geomod/util/eigensolver.h>
#include <type_traits>
#include <iostream>
#include <complex>

// A tensor in physics is considerably different than a simple 3x3 matrix.
// Of central importance are the properties of the tensor, and their
// preservation under transformations.

namespace geomod{
namespace util{

template <typename T>
class TTensor3{
public:
	typedef unsigned char flags_type;
	enum Property{
		REAL        = 0x01,
		HERMITIAN   = 0x02,
		SYMMETRIC   = 0x04,
		Z_UNCOUPLED = 0x08,
		DIAGONAL    = 0x10,
		SCALAR      = 0x20,
		POSDEF      = 0x80
	};
private:
	T m[9];
	flags_type flags;
public:
	void UpdateFlags(){
		const T& m00 = m[0];
		const T& m10 = m[1];
		const T& m20 = m[2];
		const T& m01 = m[3];
		const T& m11 = m[4];
		const T& m21 = m[5];
		const T& m02 = m[6];
		const T& m12 = m[7];
		const T& m22 = m[8];

		flags = 0;
		if(T(0) == m20 && T(0) == m21 && T(0) == m02 && T(0) == m12){
			flags |= Z_UNCOUPLED;
			if(T(0) == m10 && T(0) == m01){
				flags |= DIAGONAL;
				if(m00 == m11 && m11 == m22){
					flags |= SCALAR;
				}
			}
		}
		if(m01 == m10 && m02 == m20 && m12 == m21){
			flags |= SYMMETRIC;
		}
		if(
			m01 == std::conj(m10) && m02 == std::conj(m20) && m12 == std::conj(m21) &&
			T(0) == std::imag(m00) && T(0) == std::imag(m11) && T(0) == std::imag(m22)
		){
			flags |= HERMITIAN;
			// Check for positive definiteness
			if(flags & DIAGONAL){
				if(std::real(m00) > 0 && std::real(m11) > 0 && std::real(m22) > 0){
					flags |= POSDEF;
				}
			}else if(flags & Z_UNCOUPLED){
				if(
					std::real(m22) > 0 &&
					std::real(m00)+std::real(m11) > 0 &&
					std::real(m00)*std::real(m11) > std::norm(m10)
				){
					flags |= POSDEF;
				}
			}else{
				if(
					(std::real(m00)+std::real(m11)+std::real(m22)) > 0 && // trace > 0
					(std::real(m00)*std::real(m11)*std::real(m22)
						- std::real(m00)*std::norm(m21)
						- std::real(m11)*std::norm(m20)
						- std::real(m22)*std::norm(m10)
						+ 2*std::real(m10*m21*std::conj(m20))) > 0 && // det > 0
					((std::real(m00)*std::real(m11)*std::real(m22)
						- std::real(m00)*std::norm(m21)
						- std::real(m11)*std::norm(m20)
						- std::real(m22)*std::norm(m10)
						+ 2*std::real(m10*m21*std::conj(m20))) + (std::real(m00)+std::real(m11)+std::real(m22)) * (
							std::real(m00)*std::real(m11) - std::norm(m10) +
							std::real(m11)*std::real(m22) - std::norm(m21) +
							std::real(m22)*std::real(m00) - std::norm(m02)
						)
					) > 0 // det + trace(adj)*trace > 0
				){
					flags |= POSDEF;
				}
			}
		}
		if(
			0 == std::imag(m00) && 0 == std::imag(m01) && 0 == std::imag(m02) &&
			0 == std::imag(m10) && 0 == std::imag(m11) && 0 == std::imag(m12) &&
			0 == std::imag(m20) && 0 == std::imag(m21) && 0 == std::imag(m22)
		){
			flags |= REAL;
		}
	}
public:
	// Default constructor
	TTensor3(const T &diag = T(1)){
		m[0] = diag; m[1] = T(0); m[2] = T(0);
		m[3] = T(0); m[4] = diag; m[5] = T(0);
		m[6] = T(0); m[7] = T(0); m[8] = diag;
		flags = SCALAR | DIAGONAL | Z_UNCOUPLED | SYMMETRIC;
		if(0 == std::imag(diag)){
			flags |= (REAL | HERMITIAN);
			if(std::real(diag) > 0){
				flags |= POSDEF;
			}
		}
	}
	TTensor3(
		const T &m00, const T &m01, const T &m02,
		const T &m10, const T &m11, const T &m12,
		const T &m20, const T &m21, const T &m22
	){
		m[0] = m00; m[1] = m10; m[2] = m20;
		m[3] = m01; m[4] = m11; m[5] = m21;
		m[6] = m02; m[7] = m12; m[8] = m22;
		UpdateFlags();
	}
	TTensor3(
		const T &x, const T &y, const T &z
	){
		m[0] = x;    m[1] = T(0); m[2] = T(0);
		m[3] = T(0); m[4] = y;    m[5] = T(0);
		m[6] = T(0); m[7] = T(0); m[8] = z;
		flags = DIAGONAL | Z_UNCOUPLED | SYMMETRIC;
		if(x == y && y == z){ flags |= SCALAR; }
		if(0 == std::imag(x) && 0 == std::imag(y) && 0 == std::imag(z)){
			flags |= (REAL | HERMITIAN);
			if(std::real(x) > 0 && std::real(y) > 0 && std::real(z) > 0){
				flags |= POSDEF;
			}
		}
	}

	const T &operator()(unsigned r, unsigned c) const{ return m[r+c*3]; }
	T &operator()(unsigned r, unsigned c){ return m[r+c*3]; flags = 0; }

	bool IsScalar() const{ return SCALAR & flags; }
	bool IsReal() const{ return REAL & flags; }
	bool IsPositiveDefinite() const{ return POSDEF & flags; }
	T AsScalar() const{ return m[0]; }
	flags_type GetFlags() const{ return flags; }

	void Add(const T &coeff, const TTensor3<T> &b){
		for(unsigned i = 0; i < 9; ++i){
			if(T(0) != b.m[i]){
				m[i] += coeff*b.m[i];
			}
		}
		if(!((POSDEF & b.flags & flags) && (0 == std::imag(coeff) && std::real(coeff) >= 0))){
			flags &= ~POSDEF;
		}
		flags &= b.flags;
	}

	template <typename Real>
	void Rotate(const util::TQuaternion<Real> &q){
		if(IsScalar()){ return; }
		Real Q[9];
		q.AsMatrix(Q);
		T a[9];
		// a = m*Q'
		for(unsigned j = 0; j < 3; ++j){
			for(unsigned i = 0; i < 3; ++i){
				a[i+j*3] = 0;
				for(unsigned k = 0; k < 3; ++k){
					a[i+j*3] += T(Q[j+k*3]) * m[i+k*3];
				}
			}
		}
		// m = Q*a = Q*m*Q'
		for(unsigned j = 0; j < 3; ++j){
			for(unsigned i = 0; i < 3; ++i){
				m[i+j*3] = 0;
				for(unsigned k = 0; k < 3; ++k){
					m[i+j*3] += T(Q[i+k*3]) * a[k+j*3];
				}
			}
		}
		if(flags & SYMMETRIC){
			m[1] = T(0.5)*(m[1] + m[3]); m[3] = m[1];
			m[2] = T(0.5)*(m[2] + m[6]); m[6] = m[2];
			m[5] = T(0.5)*(m[5] + m[7]); m[7] = m[5];
		}else if(flags & HERMITIAN){
			m[1] = T(0.5)*(m[1] + std::conj(m[3])); m[3] = std::conj(m[1]);
			m[2] = T(0.5)*(m[2] + std::conj(m[6])); m[6] = std::conj(m[2]);
			m[5] = T(0.5)*(m[5] + std::conj(m[7])); m[7] = std::conj(m[5]);
		}
		UpdateFlags();
	}
	
	// If the tensor is M, then this computes row^T * M * M * col
	template <typename RowScalar, typename ColScalar>
	T Contraction(const affine::TVector3<RowScalar> &row, const affine::TVector3<ColScalar> &col) const{
		using RowType = typename upgrade_if_complex<T, RowScalar>::type;
		using ColType = typename upgrade_if_complex<T, ColScalar>::type;

		const ColType au(ColType(m[0])*col[0] + ColType(m[3])*col[1] + ColType(m[6])*col[2]);
		const ColType bu(ColType(m[1])*col[0] + ColType(m[4])*col[1] + ColType(m[7])*col[2]);
		const ColType cu(ColType(m[2])*col[0] + ColType(m[5])*col[1] + ColType(m[8])*col[2]);
		const RowType av(RowType(m[0])*row[0] + RowType(m[1])*row[1] + RowType(m[2])*row[2]);
		const RowType bv(RowType(m[3])*row[0] + RowType(m[4])*row[1] + RowType(m[5])*row[2]);
		const RowType cv(RowType(m[6])*row[0] + RowType(m[7])*row[1] + RowType(m[8])*row[2]);
		T ret;
		complex_cast(au*av + bu*bv + cu*cv, ret);
		return ret;
	}
	
	void Square(){ // In-place computation
		if(flags & DIAGONAL){
			m[0] *= m[0];
			m[4] *= m[4];
			m[8] *= m[8];
		}else if(flags & Z_UNCOUPLED){
			const T a[4] = { m[0], m[1], m[3], m[4] };
			for(unsigned j = 0; j < 2; ++j){
				for(unsigned i = 0; i < 2; ++i){
					T sum(0.);
					for(unsigned k = 0; k < 2; ++k){
						sum += a[i+k*2] * a[k+j*2];
					}
					m[i+j*3] = sum;
				}
			}
			m[8] *= m[8];
		}else{
			T a[9];
			for(unsigned i = 0; i < 9; ++i){ a[i] = m[i]; }
			for(unsigned j = 0; j < 3; ++j){
				for(unsigned i = 0; i < 3; ++i){
					T sum(0.);
					for(unsigned k = 0; k < 3; ++k){
						sum += a[i+k*3] * a[k+j*3];
					}
					m[i+j*3] = sum;
				}
			}
		}
		if(flags & HERMITIAN){
			flags |= POSDEF;
		}
	}
	
	void Sqrt(){ // In-place computation
		//UpdateFlags();
		// We really should replace the eigensolves with dedicated matrix iterations.
		if(flags & DIAGONAL){
			m[0] = std::sqrt(m[0]);
			if(flags & SCALAR){
				m[4] = m[0];
				m[8] = m[0];
			}else{
				m[4] = std::sqrt(m[4]);
				m[8] = std::sqrt(m[8]);
			}
		}else if(flags & Z_UNCOUPLED){
			if(flags & HERMITIAN){
				T a[4] = { m[0], m[1], m[3], m[4] };
				typename T::value_type d[2], work[4];
				T u[4];
				util::eigensolver::Hermitian(2, a, 2, d, u, 2, work);
				const T dc[2] = {
					std::sqrt(T(d[0])),
					std::sqrt(T(d[1]))
				};
				for(unsigned j = 0; j < 2; ++j){
					for(unsigned i = 0; i < 2; ++i){
						T sum(0.);
						for(unsigned k = 0; k < 2; ++k){
							sum += u[i+k*2] * dc[k] * std::conj(u[j+k*2]);
						}
						m[i+j*3] = sum;
					}
				}
			}else{
				T a[4] = { m[0], m[1], m[3], m[4] };
				T d[2], work[4], u[4];
				util::eigensolver::NonSymmetric(2, a, 2, d, u, 2, work);
				d[0] = std::sqrt(d[0]);
				d[1] = std::sqrt(d[1]);
				for(unsigned j = 0; j < 2; ++j){
					for(unsigned i = 0; i < 2; ++i){
						T sum(0.);
						for(unsigned k = 0; k < 2; ++k){
							sum += u[i+k*2] * d[k] * std::conj(u[j+k*2]);
						}
						m[i+j*3] = sum;
					}
				}
			}
			m[8] = std::sqrt(m[8]);
		}else{ // Here we assume that T is a std::complex<> type
			if(flags & HERMITIAN){
				T a[9];
				for(unsigned i = 0; i < 9; ++i){ a[i] = m[i]; }
				typename T::value_type d[3], work[6];
				T u[9];
				util::eigensolver::Hermitian(3, a, 3, d, u, 3, work);
				const T dc[3] = {
					std::sqrt(T(d[0])),
					std::sqrt(T(d[1])),
					std::sqrt(T(d[2]))
				};
				for(unsigned j = 0; j < 3; ++j){
					for(unsigned i = 0; i < 3; ++i){
						T sum(0.);
						for(unsigned k = 0; k < 3; ++k){
							sum += u[i+k*3] * dc[k] * std::conj(u[j+k*3]);
						}
						m[i+j*3] = sum;
					}
				}
			}else{
				T a[9];
				for(unsigned i = 0; i < 9; ++i){ a[i] = m[i]; }
				T d[3], work[6], u[9];
				util::eigensolver::NonSymmetric(3, a, 3, d, u, 3, work);
				d[0] = std::sqrt(d[0]);
				d[1] = std::sqrt(d[1]);
				d[2] = std::sqrt(d[2]);
				for(unsigned j = 0; j < 3; ++j){
					for(unsigned i = 0; i < 3; ++i){
						T sum(0.);
						for(unsigned k = 0; k < 3; ++k){
							sum += u[i+k*3] * d[k] * std::conj(u[j+k*3]);
						}
						m[i+j*3] = sum;
					}
				}
			}
		}
	}
};

template <typename T>
inline std::ostream& operator<<(std::ostream &s, const TTensor3<T> &m){
	s << '{'
		<< '{' << m(0,0) << ',' << m(0,1) << ',' << m(0,2) << "},"
		<< '{' << m(1,0) << ',' << m(1,1) << ',' << m(1,2) << "},"
		<< '{' << m(2,0) << ',' << m(2,1) << ',' << m(2,2) << "}}";
	return s;
}

} // namespace util
} // namespace geomod

#endif // GEOMOD_UTIL_TENSOR_H_
