#ifndef GEOMOD_UTIL_MATRIX_H_
#define GEOMOD_UTIL_MATRIX_H_

#include <geomod/affine/vector.h>
#include <geomod/affine/point.h>
#include <geomod/affine/ray.h>
#include <geomod/affine/transform.h>
#include <geomod/util/quaternion.h>

#include <iostream>

namespace geomod{
namespace util{

template <typename T>
class TMatrix4{
	T m[16];
public:
	// Default constructor
	TMatrix4(){}
	TMatrix4(const T &diag){
		m[ 0] = diag; m[ 1] = T(0); m[ 2] = T(0); m[ 3] = T(0);
		m[ 4] = T(0); m[ 5] = diag; m[ 6] = T(0); m[ 7] = T(0);
		m[ 8] = T(0); m[ 9] = T(0); m[10] = diag; m[11] = T(0);
		m[12] = T(0); m[13] = T(0); m[14] = T(0); m[15] = T(1);
	}
	TMatrix4(
		const T &m00, const T &m01, const T &m02, const T &m03,
		const T &m10, const T &m11, const T &m12, const T &m13,
		const T &m20, const T &m21, const T &m22, const T &m23,
		const T &m30, const T &m31, const T &m32, const T &m33
	){
		m[ 0] = m00; m[ 1] = m10; m[ 2] = m20; m[ 3] = m30;
		m[ 4] = m01; m[ 5] = m11; m[ 6] = m21; m[ 7] = m31;
		m[ 8] = m02; m[ 9] = m12; m[10] = m22; m[11] = m32;
		m[12] = m03; m[13] = m13; m[14] = m23; m[15] = m33;
	}
	TMatrix4(const affine::Transform3 &x){
		coord_type xm[12];
		x.AsMatrix(xm);
		m[0] = T(xm[0]);
		m[1] = T(xm[1]);
		m[2] = T(xm[2]);
		m[3] = T(0);

		m[4] = T(xm[3]);
		m[5] = T(xm[4]);
		m[6] = T(xm[5]);
		m[7] = T(0);

		m[ 8] = T(xm[6]);
		m[ 9] = T(xm[7]);
		m[10] = T(xm[8]);
		m[11] = T(0);

		m[12] = T(xm[ 9]);
		m[13] = T(xm[10]);
		m[14] = T(xm[11]);
		m[15] = T(1);

	}

	const T &operator()(unsigned r, unsigned c) const{ return m[r+c*4]; }
	T &operator()(unsigned r, unsigned c){ return m[r+c*4]; }
	
	Quaternion GetRotation() const{
		return Quaternion(
			affine::Vector3(m[0], m[1], m[2]),
			affine::Vector3(m[4], m[5], m[6]),
			affine::Vector3(m[8], m[9], m[10])
		);
	}
	
	static TMatrix4 Multiply(const TMatrix4 &A, const TMatrix4 &B){
		TMatrix4 M;
		for(unsigned j = 0; j < 4; ++j){
			for(unsigned i = 0; i < 4; ++i){
				M(i,j) = T(0);
				for(unsigned k = 0; k < 4; ++k){
					M(i,j) += A(i,k) * B(k,j);
				}
			}
		}
		return M;
	}
	static TMatrix4 Transpose(const TMatrix4 &A){
		return Matrix4(
			A.m[ 0], A.m[ 1], A.m[ 2], A.m[ 3],
			A.m[ 4], A.m[ 5], A.m[ 6], A.m[ 7],
			A.m[ 8], A.m[ 9], A.m[10], A.m[11],
			A.m[12], A.m[13], A.m[14], A.m[15]
		);
	}
	static TMatrix4 Inverse(const TMatrix4 &M){
		TMatrix4 minv(M);
		int indxc[4], indxr[4];
		int ipiv[4] = { 0, 0, 0, 0 };
		for (int i = 0; i < 4; i++) {
			int irow = -1, icol = -1;
			T big(0);
			// Choose pivot
			for(int j = 0; j < 4; j++){
				if(ipiv[j] != 1){
					for(int k = 0; k < 4; k++){
						if(ipiv[k] == 0){
							if(std::abs(minv(j,k)) >= big){
								big = T(std::abs(minv(j,k)));
								irow = j;
								icol = k;
							}
						}//else if(ipiv[k] > 1){
						//	Error("Singular matrix in MatrixInvert");
						//}
					}
				}
			}
			++ipiv[icol];

			// Swap rows _irow_ and _icol_ for pivot
			if(irow != icol){
				for(int k = 0; k < 4; ++k){
					std::swap(minv(irow,k), minv(icol,k));
				}
			}
			indxr[i] = irow;
			indxc[i] = icol;
			//if(minv[icol][icol] == 0.){
			//	Error("Singular matrix in MatrixInvert");
			//}

			// Set $m[icol][icol]$ to one by scaling row _icol_ appropriately
			T pivinv = T(1) / minv(icol,icol);
			minv(icol,icol) = T(1);
			for(int j = 0; j < 4; j++){ minv(icol,j) *= pivinv; }

			// Subtract this row from others to zero out their columns
			for(int j = 0; j < 4; j++){
				if(j != icol){
					T save = minv(j,icol);
					minv(j,icol) = 0;
					for(int k = 0; k < 4; k++){
						minv(j,k) -= minv(icol,k)*save;
					}
				}
			}
		}
		// Swap columns to reflect permutation
		for(int j = 3; j >= 0; j--){
			if(indxr[j] != indxc[j]){
				for(int k = 0; k < 4; k++){
					std::swap(minv(k,indxr[j]), minv(k,indxc[j]));
				}
			}
		}
		return minv;
	}

	static TMatrix4 Perspective(
		const coord_type &fovy, const coord_type &aspect,
		const coord_type &znear, const coord_type &zfar
	){
		coord_type ymax, xmax;
    	ymax = znear * tan(fovy * M_PI / 360);
    	xmax = ymax * aspect;
    	return TMatrix4::Frustum(-xmax, xmax, -ymax, ymax, znear, zfar);
	}
	static TMatrix4 Frustum(
		const coord_type &x0, const coord_type &x1,
		const coord_type &y0, const coord_type &y1,
		const coord_type &z0, const coord_type &z1
	){
    	const coord_type z02 = 2*z0;
    	const coord_type dx = x1-x0;
    	const coord_type dy = y1-y0;
    	const coord_type dz = z1-z0;
    	return TMatrix4(
    		z02/dx, 0, (x0+x1)/dx, 0,
    		0, z02/dy, (y0+y1)/dy, 0,
    		0,      0,-(z0+z1)/dz, -z02*z1/dz,
    		0, 0, -1, 0
    	);
	}
};

template <typename T>
inline std::ostream& operator<<(std::ostream &s, const TMatrix4<T> &m){
	s << '{'
		<< '{' << m(0,0) << ',' << m(0,1) << ',' << m(0,2) << ',' << m(0,3) << "},"
		<< '{' << m(1,0) << ',' << m(1,1) << ',' << m(1,2) << ',' << m(1,3) << "},"
		<< '{' << m(2,0) << ',' << m(2,1) << ',' << m(2,2) << ',' << m(2,3) << "},"
		<< '{' << m(3,0) << ',' << m(3,1) << ',' << m(3,2) << ',' << m(3,3) << "}}";
	return s;
}

template <typename T>
class Matrix{
	T *a;
	size_t m, n;
public:
	class View{
		T *a;
		size_t lda, m, n;
		friend class Matrix;
		View(size_t m, size_t n, T *a, size_t lda):
			a(a), lda(lda), m(m), n(n)
		{}
	public:
		const T &operator()(unsigned r, unsigned c) const{ return a[r+c*lda]; }
		T &operator()(unsigned r, unsigned c){ return a[r+c*lda]; }

		View GetSubmatrix(size_t i, size_t j, size_t nrows, size_t ncols) const{
			return View(nrows, ncols, &a[i+j*lda], lda);
		}
	};

	// Default constructor
	Matrix(size_t nrows, size_t ncols, const T &diag = T(0)):m(nrows), n(ncols){
		a = (T*)malloc(sizeof(T) * nrows * ncols);
		memset(a, 0, sizeof(T) * nrows * ncols);
		if(T(0) != diag){
			for(size_t j = 0; j < ncols; ++j){
				if(j < nrows){
					a[j+j*m] = diag;
				}else{ break; }
			}
		}
	}
	~Matrix(){
		free(a);
	}

	const T &operator()(unsigned r, unsigned c) const{ return a[r+c*m]; }
	T &operator()(unsigned r, unsigned c){ return a[r+c*m]; }

	View GetSubmatrix(size_t i, size_t j, size_t nrows, size_t ncols) const{
		return View(nrows, ncols, &a[i+j*m], m);
	}
};

} // namespace util
} // namespace geomod

#endif // GEOMOD_UTIL_MATRIX_H_
