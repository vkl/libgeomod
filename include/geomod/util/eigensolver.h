#ifndef GEOMOD_UTIL_EIGENSOLVER_H_
#define GEOMOD_UTIL_EIGENSOLVER_H_

#include <complex>
#include <limits>
#include <cstring>

// This file contains routines for solving eigenvalue problems
// using Jacobi iterations (as opposed to the standard QR algorithm).
// These routines do not scale well with matrix size, so they should
// only be used for small matrices.

// This code is adapted from code on a website originally found through
// code.google.com circa 2014, but that is no longer online.

namespace geomod{
namespace util{
namespace eigensolver{

template <typename Real, typename Index>
int Symmetric(
	Index n,
	Real *a, Index lda,
	Real *d,
	Real *u, Index ldu,
	Real *work = NULL
){
	const Index n2 = n*n;
	const Real red = Real(0.04)/(n2*n2);
	const Index max_sweeps = 50;
	const Real eps = std::numeric_limits<Real>::epsilon();
	const Real sym_eps = Real(2)*eps*eps;
	
	Real *ev = work;
	if(NULL == work){ ev = new Real[2*n]; }
	if(NULL == ev){ return -7; }
	
	if(1 == n){ d[0] = a[0]; u[0] = Real(1); }
	for(Index i = 0; i < n; ++i){
		d[i] = a[i+i*lda];
		ev[2*i+0] = 0;
		ev[2*i+1] = d[i];
	}
	for(Index j = 0; j < n; ++j){ // Set U to identity
		memset(&u[j*ldu], 0, sizeof(Real) * n);
		u[j+j*lda] = Real(1);
	}
	for(Index sweep = 0; sweep < max_sweeps; ++sweep){
		Real thresh(0);
		for(Index j = 1; j < n; ++j){
			for(Index i = 0; i < j; ++i){
				thresh += a[i+j*lda]*a[i+j*lda];
			}
		}
		if(!(thresh > sym_eps)){ return 0; }
		
		thresh = (sweep < 3) ? thresh*red : 0;
		for(Index j = 1; j < n; ++j){
			for(Index i = 0; i < j; ++i){
				const Real Aij(a[i+j*lda]);
				const Real off = Aij*Aij;
				if(sweep > 3 && off < sym_eps*(ev[2*i+1]*ev[2*i+1] + ev[2*j+1]*ev[2*j+1])){
					a[i+j*lda] = Real(0);
				}else if(off > thresh){
					Real delta, s, invc;
					
					Real t = Real(0.5)*(ev[2*i+1] - ev[2*j+1]);
					t = Real(1) / (t + std::copysign(std::sqrt(t*t + off), t));
					delta = off*t;
					ev[2*i+1] = d[i] + (ev[2*i+0] += delta);
					ev[2*j+1] = d[j] + (ev[2*j+0] -= delta);
					invc = std::sqrt(delta*t + Real(1));
					s = t/invc;
					t = delta/(invc + Real(1));
					
					for(Index k = 0; k < i; ++k){
						const Real x(a[k+i*lda]);
						const Real y(a[k+j*lda]);
						a[k+i*lda] = x + s*(Aij*y - t*x);
						a[k+j*lda] = y - s*(Aij*x + t*y);
					}
					for(Index k = i+1; k < j; ++k){
						const Real x(a[i+k*lda]);
						const Real y(a[k+j*lda]);
						a[i+k*lda] = x + s*(Aij*y - t*x);
						a[k+j*lda] = y - s*(Aij*x + t*y);
					}
					for(Index k = j+1; k < n; ++k){
						const Real x(a[i+k*lda]);
						const Real y(a[j+k*lda]);
						a[i+k*lda] = x + s*(Aij*y - t*x);
						a[j+k*lda] = y - s*(Aij*x + t*y);
					}
					a[i+j*lda] = Real(0);
					for(Index k = 0; k < n; ++k){
						const Real x(u[k+i*ldu]);
						const Real y(u[k+j*ldu]);
						u[k+i*ldu] = x + s*(Aij*y - t*x);
						u[k+j*ldu] = y - s*(Aij*x + t*y);
					}
				}
			}
			for(Index i = 0; i < n; ++i){
				ev[2*i+0] = 0;
				d[i] = ev[2*i+1];
			}
		}
	}
	
	if(NULL == work){ delete [] ev; }
	return 1;
}

// The matrix A is given in rectangular form, but only the upper triangle is
// referenced. Only the real parts of the diagonal are referenced; any non-
// zero imaginary parts of the diagonal are ignored and assumed to be zero.
// The strictly upper triangular part is overwritten upon exit.
// The eigenvectors in u are stored column-wise.
template <typename Real, typename Index>
int Hermitian(
	Index n,
	std::complex<Real> *a, Index lda,
	Real *d,
	std::complex<Real> *u, Index ldu,
	Real *work = NULL
){
	typedef std::complex<Real> Complex;
	const Index n2 = n*n;
	const Real red = Real(0.04)/(n2*n2);
	const Index max_sweeps = 50;
	const Real eps = std::numeric_limits<Real>::epsilon();
	const Real sym_eps = Real(2)*eps*eps;
	
	Real *ev = work;
	if(NULL == work){ ev = new Real[2*n]; }
	if(NULL == ev){ return -7; }
	
	if(1 == n){ d[0] = a[0].real(); u[0] = Real(1); }
	for(Index i = 0; i < n; ++i){
		d[i] = a[i+i*lda].real();
		ev[2*i+0] = 0;
		ev[2*i+1] = d[i];
	}
	for(Index j = 0; j < n; ++j){ // Set U to identity
		for(Index k = 0; k < n; ++k){
			u[j*ldu + k] = Complex(0, 0);
		}
		u[j+j*lda] = Real(1);
	}
	for(Index sweep = 0; sweep < max_sweeps; ++sweep){
		Real thresh(0);
		for(Index j = 1; j < n; ++j){
			for(Index i = 0; i < j; ++i){
				thresh += std::norm(a[i+j*lda]);
			}
		}
		if(!(thresh > sym_eps)){ return 0; }
		
		thresh = (sweep < 3) ? thresh*red : 0;
		for(Index j = 1; j < n; ++j){
			for(Index i = 0; i < j; ++i){
				const Complex Aij(a[i+j*lda]);
				const Real off = std::norm(Aij);
				if(sweep > 3 && off < sym_eps*(std::norm(ev[2*i+1]) + std::norm(ev[2*j+1]))){
					a[i+j*lda] = Real(0);
				}else if(off > thresh){
					Real delta, s, invc;
					
					Real t = Real(0.5)*(ev[2*i+1] - ev[2*j+1]);
					t = Real(1) / (t + std::copysign(std::sqrt(t*t + off), t));
					delta = off*t;
					ev[2*i+1] = d[i] + (ev[2*i+0] += delta);
					ev[2*j+1] = d[j] + (ev[2*j+0] -= delta);
					invc = std::sqrt(delta*t + Real(1));
					s = t/invc;
					t = delta/(invc + Real(1));
					
					for(Index k = 0; k < i; ++k){
						const Complex x(a[k+i*lda]);
						const Complex y(a[k+j*lda]);
						a[k+i*lda] = x + s*(std::conj(Aij)*y - t*x);
						a[k+j*lda] = y - s*(          Aij *x + t*y);
					}
					for(Index k = i+1; k < j; ++k){
						const Complex x(a[i+k*lda]);
						const Complex y(a[k+j*lda]);
						a[i+k*lda] = x + s*(Aij*std::conj(y) - t*x);
						a[k+j*lda] = y - s*(Aij*std::conj(x) + t*y);
					}
					for(Index k = j+1; k < n; ++k){
						const Complex x(a[i+k*lda]);
						const Complex y(a[j+k*lda]);
						a[i+k*lda] = x + s*(          Aij *y - t*x);
						a[j+k*lda] = y - s*(std::conj(Aij)*x + t*y);
					}
					a[i+j*lda] = Real(0);
					for(Index k = 0; k < n; ++k){
						const Complex x(u[k+i*ldu]);
						const Complex y(u[k+j*ldu]);
						u[k+i*ldu] = x + s*(std::conj(Aij)*y - t*x);
						u[k+j*ldu] = y - s*(          Aij *x + t*y);
					}
				}
			}
			for(Index i = 0; i < n; ++i){
				ev[2*i+0] = 0;
				d[i] = ev[2*i+1];
			}
		}
	}
	
	if(NULL == work){ delete [] ev; }
	return 1;
}

template <typename Real, typename Index>
int Symmetric(
	Index n,
	std::complex<Real> *a, Index lda,
	std::complex<Real> *d,
	std::complex<Real> *u, Index ldu,
	std::complex<Real> *work = NULL
){
	typedef std::complex<Real> Complex;
	const Index n2 = n*n;
	const Real red = Real(0.04)/(n2*n2);
	const Index max_sweeps = 50;
	const Real eps = std::numeric_limits<Real>::epsilon();
	const Real sym_eps = Real(2)*eps*eps;
	
	Real *ev = work;
	if(NULL == work){ ev = new Real[2*n]; }
	if(NULL == ev){ return -7; }
	
	if(1 == n){ d[0] = a[0]; u[0] = Real(1); }
	for(Index i = 0; i < n; ++i){
		d[i] = a[i+i*lda];
		ev[2*i+0] = 0;
		ev[2*i+1] = d[i];
	}
	for(Index j = 0; j < n; ++j){ // Set U to identity
		memset(&u[j*ldu], 0, sizeof(Complex) * n);
		u[j+j*lda] = Real(1);
	}
	for(Index sweep = 0; sweep < max_sweeps; ++sweep){
		Real thresh(0);
		for(Index j = 1; j < n; ++j){
			for(Index i = 0; i < j; ++i){
				thresh += std::norm(a[i+j*lda]);
			}
		}
		if(!(thresh > sym_eps)){ return 0; }
		
		thresh = (sweep < 3) ? thresh*red : 0;
		for(Index j = 1; j < n; ++j){
			for(Index i = 0; i < j; ++i){
				const Complex delta(a[i+j*lda]);
				const Real off = std::norm(delta);
				const Real sqi = std::norm(ev[2*i+1]);
				const Real sqj = std::norm(ev[2*j+1]);
				if(sweep > 3 && off < sym_eps*(sqi + sqj)){
					a[i+j*lda] = Real(0);
				}else if(off > thresh){
					Complex t, s;
					{
						Complex x = Real(0.5)*(ev[2*i+1] - ev[2*j+1]);
						Complex y = std::sqrt(x*x + delta*delta);
						if(std::norm(t = x-y) < std::norm(s = x+y)){ t = s; }
					}
					t = delta/t;
					delta *= t;
					ev[2*i+1] = d[i] + (ev[2*i+0] += delta);
					ev[2*j+1] = d[j] + (ev[2*j+0] -= delta);
					Complex invc = std::sqrt(t*t + Real(1));
					s = t/invc;
					t /= (invc + Real(1));
					
					for(Index k = 0; k < i; ++k){
						const Complex x(a[k+i*lda]);
						const Complex y(a[k+j*lda]);
						a[k+i*lda] = x + s*(y - t*x);
						a[k+j*lda] = y - s*(x + t*y);
					}
					for(Index k = i+1; k < j; ++k){
						const Complex x(a[i+k*lda]);
						const Complex y(a[k+j*lda]);
						a[i+k*lda] = x + s*(y - t*x);
						a[k+j*lda] = y - s*(x + t*y);
					}
					for(Index k = j+1; k < n; ++k){
						const Complex x(a[i+k*lda]);
						const Complex y(a[j+k*lda]);
						a[i+k*lda] = x + s*(y - t*x);
						a[j+k*lda] = y - s*(x + t*y);
					}
					a[i+j*lda] = Real(0);
					for(Index k = 0; k < n; ++k){
						const Complex x(u[k+i*ldu]);
						const Complex y(u[k+j*ldu]);
						u[k+i*ldu] = x + s*(y - t*x);
						u[k+j*ldu] = y - s*(x + t*y);
					}
				}
			}
			for(Index i = 0; i < n; ++i){
				ev[2*i+0] = 0;
				d[i] = ev[2*i+1];
			}
		}
	}
	
	if(NULL == work){ delete [] ev; }
	return 1;
}


template <typename Real, typename Index>
int NonSymmetric(
	Index n,
	std::complex<Real> *a, Index lda,
	std::complex<Real> *d,
	std::complex<Real> *u, Index ldu,
	std::complex<Real> *work = NULL
){
	typedef std::complex<Real> Complex;
	const Index n2 = n*n;
	const Real red = Real(0.01)/(n2*n2);
	const Index max_sweeps = 50;
	const Real eps = std::numeric_limits<Real>::epsilon();
	const Real nonsym_eps = eps*eps;
	
	Complex *ev = work;
	if(NULL == work){ ev = new Complex[2*n]; }
	if(NULL == ev){ return -7; }
	
	if(1 == n){ d[0] = a[0]; u[0] = Real(1); }
	for(Index i = 0; i < n; ++i){
		d[i] = a[i+i*lda];
		ev[2*i+0] = 0;
		ev[2*i+1] = d[i];
	}
	for(Index j = 0; j < n; ++j){ // Set U to identity
		for(Index k = 0; k < n; ++k){
			u[j*ldu + k] = Complex(0, 0);
		}
		u[j+j*lda] = Real(1);
	}
	for(Index sweep = 0; sweep < max_sweeps; ++sweep){
		Real thresh(0);
		for(Index j = 1; j < n; ++j){
			for(Index i = 0; i < j; ++i){
				thresh += std::norm(a[i+j*lda]) + std::norm(a[j+i*lda]);
			}
		}
		if(!(thresh > nonsym_eps)){ return 0; }
		
		thresh = (sweep < 3) ? thresh*red : 0;
		for(Index j = 1; j < n; ++j){
			for(Index i = 0; i < j; ++i){
				const Complex Aij(a[i+j*lda]);
				const Complex Aji(a[j+i*lda]);
				const Real off = std::norm(Aij) + std::norm(Aji);
				if(sweep > 3 && off < nonsym_eps*(std::norm(ev[2*i+1]) + std::norm(ev[2*j+1]))){
					a[i+j*lda] = Real(0);
					a[j+i*lda] = Real(0);
				}else if(off > thresh){
					Complex s, t;
					Complex delta = Aij*Aji;
					{
						Complex x = Real(0.5)*(ev[2*i+1] - ev[2*j+1]);
						Complex y = std::sqrt(x*x + delta);
						if(std::norm(t = x-y) < std::norm(s = x+y)){ t = s; }
					}
					t = Real(1)/t;
					delta *= t;
					ev[2*i+1] = d[i] + (ev[2*i+0] += delta);
					ev[2*j+1] = d[j] + (ev[2*j+0] -= delta);

					const Complex invc = std::sqrt(delta*t + Real(1));
					s = t/invc;
					t /= invc + Real(1);
					const Complex sx = s*Aij;
					const Complex ty = t*Aij;
					const Complex sy = s*Aji;
					const Complex tx = t*Aji;

					for(Index k = 0; k < n; ++k){
						Complex x = a[k+i*lda];
						Complex y = a[k+j*lda];
						a[k+i*lda] = x + sy*(y - ty*x);
						a[k+j*lda] = y - sx*(x + tx*y);
						x = a[i+k*lda];
						y = a[j+k*lda];
						a[i+k*lda] = x + sx*(y - tx*x);
						a[j+k*lda] = y - sy*(x + ty*y);
					}

					a[i+j*lda] = Real(0);
					a[j+i*lda] = Real(0);

					for(Index k = 0; k < n; ++k){
						const Complex x = u[k+i*ldu];
						const Complex y = u[k+j*ldu];
						u[k+i*ldu] = x + sy*(y - ty*x);
						u[k+j*ldu] = y - sx*(x + tx*y);
					}
				}
			}

			for(Index i = 0; i < n; ++i){
				ev[2*i+0] = Real(0);
				d[i] = ev[2*i+1];
			}
		}
	}
	
	if(NULL == work){ delete [] ev; }
	return 1;
}

template <typename Real, typename Index>
int Takagi(
	Index n,
	std::complex<Real> *a, Index lda,
	Real *d,
	std::complex<Real> *u, Index ldu,
	std::complex<Real> *work = NULL
){
	typedef std::complex<Real> Complex;
	const Index n2 = n*n;
	const Real red = Real(0.04)/(n2*n2);
	const Index max_sweeps = 50;
	const Real eps = std::numeric_limits<Real>::epsilon();
	const Real sym_eps = Real(2)*eps*eps;
	
	Complex *ev = work;
	if(NULL == work){ ev = new Complex[2*n]; }
	if(NULL == ev){ return -7; }
	
	if(1 == n){ d[0] = a[0]; u[0] = Real(1); }
	for(Index i = 0; i < n; ++i){
		d[i] = a[i+i*lda];
		ev[2*i+0] = 0;
		ev[2*i+1] = d[i];
	}
	for(Index j = 0; j < n; ++j){ // Set U to identity
		memset(&u[j*ldu], 0, sizeof(Complex) * n);
		u[j+j*lda] = Real(1);
	}
	for(Index sweep = 0; sweep < max_sweeps; ++sweep){
		Real thresh(0);
		for(Index j = 1; j < n; ++j){
			for(Index i = 0; i < j; ++i){
				thresh += std::norm(a[i+j*lda]);
			}
		}
		if(!(thresh > sym_eps)){ return 0; }
		
		thresh = (sweep < 3) ? thresh*red : 0;
		for(Index j = 1; j < n; ++j){
			for(Index i = 0; i < j; ++i){
				const Complex Aij(a[i+j*lda]);
				const Real off = std::norm(Aij);
				const Real sqi = std::norm(ev[2*i+1]);
				const Real sqj = std::norm(ev[2*j+1]);
				if(sweep > 3 && off < sym_eps*(sqi + sqj)){
					a[i+j*lda] = Real(0);
				}else if(off > thresh){
					Complex t, s, f;

					t = Real(0.5)*std::abs(sqi - sqj);
					if(t > eps){
						f = std::copysign(Real(1), sqi - sqj) * (ev[2*j+1]*std::conj(Aij) + std::conj(ev[2*i+1])*Aij);
					}else{
						f = (0 == sqi) ? Real(1) : std::sqrt(ev[2*j+1]/ev[2*i+1]);
					}
					t += std::sqrt(t*t + std::norm(f));
					f /= t;

					ev[2*i+1] = a[i+i*lda] + (ev[2*i+0] += Aij*std::conj(f));
					ev[2*j+1] = a[j+j*lda] + (ev[2*j+0] -= Aij*          f );

					t = std::norm(f);
					Complex invc = std::sqrt(t + Real(1));
					f /= invc;
					t /= invc*(invc + Real(1));
					
					for(Index k = 0; k < i; ++k){
						const Complex x(a[k+i*lda]);
						const Complex y(a[k+j*lda]);
						a[k+i*lda] = x + (std::conj(f)*y - t*x);
						a[k+j*lda] = y - (          f *x + t*y);
					}
					for(Index k = i+1; k < j; ++k){
						const Complex x(a[i+k*lda]);
						const Complex y(a[k+j*lda]);
						a[i+k*lda] = x + (std::conj(f)*y - t*x);
						a[k+j*lda] = y - (          f *x + t*y);
					}
					for(Index k = j+1; k < n; ++k){
						const Complex x(a[i+k*lda]);
						const Complex y(a[j+k*lda]);
						a[i+k*lda] = x + (std::conj(f)*y - t*x);
						a[j+k*lda] = y - (          f *x + t*y);
					}
					a[i+j*lda] = Real(0);
					for(Index k = 0; k < n; ++k){
						const Complex x(u[k+i*ldu]);
						const Complex y(u[k+j*ldu]);
						u[k+i*ldu] = x + (          f *y - t*x);
						u[k+j*ldu] = y - (std::conj(f)*x + t*y);
					}
				}
			}
			for(Index i = 0; i < n; ++i){
				ev[2*i+0] = 0;
				d[i] = ev[2*i+1];
			}
		}
	}
	
	if(NULL == work){ delete [] ev; }
	return 1;
}

} // namespace eigensolver
} // namespace util
} // namespace geomod

#endif // GEOMOD_UTIL_EIGENSOLVER_H_
