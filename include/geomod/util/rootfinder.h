#ifndef GEOMOD_UTIL_ROOTFINDER_H_
#define GEOMOD_UTIL_ROOTFINDER_H_

#include <limits>
#include <cmath>

namespace geomod{
namespace rootfinder{

// Func should be a function object with signature
//   T Func(const T &x)
template <typename T, typename Func>
T Bisection(
	Func func, T a, T b,
	const T &tol = std::numeric_limits<T>::epsilon(),
	size_t max_evals = 0
){
	static const T half(T(1)/T(2));

	T fa = func(a);
	T fb = func(b);
	if((fa > 0 && fb > 0) || (fa < 0 && fb < 0)){
		return std::numeric_limits<T>::quiet_NaN();
	}

	size_t nevals = 2;
	size_t nevals_max = (0 == max_evals ? size_t(-1) : max_evals);
	T m;
	do{
		m = half*a + half*b;
		T fm = func(m);
		if(fa < 0 && fb >= 0){
			if(fm >= 0){ b = m; fb = fm; }
			else       { a = m; fa = fm; }
		}else{
			if(fm >= 0){ a = m; fa = fm; }
			else       { b = m; fb = fm; }
		}
		nevals++;
	}while(std::abs(b-a) > m*tol && nevals < nevals_max);
	return m;
}

// Func should be a function object with signature
//   T Func(const T &x, T *dx)
template <typename T, typename Func>
T Newton(
	Func func, const T &x_guess,
	const T &tol = std::numeric_limits<T>::epsilon(),
	size_t max_evals = 0
){
	T x(x_guess);
	size_t nevals = 0;
	size_t nevals_max = (0 == max_evals ? size_t(-1) : max_evals);
	T step;
	do{
		T df;
		T f = func(x, &df);
		step = f/df;
		x -= step;
		nevals++;
	}while(std::abs(step) > tol && nevals < nevals_max);
	return x;
}

// Func should be a function object with signature
//   T Func(const T &x, T *dx, T *dx2)
template <typename T, typename Func>
T Halley(
	Func func, const T &x_guess,
	const T &tol = std::numeric_limits<T>::epsilon(),
	size_t max_evals = 0
){
	static const T two(2);
	T x(x_guess);
	size_t nevals = 0;
	size_t nevals_max = (0 == max_evals ? size_t(-1) : max_evals);
	T step;
	do{
		T df, df2;
		T f = func(x, &df, &df2);
		step = two*f*df/(two*df*df - f*df2);
		x -= step;
		nevals++;
	}while(std::abs(step) > tol && nevals < nevals_max);
	return x;
}

} // namespace rootfinder
} // namespace geomod

#endif // GEOMOD_UTIL_ROOTFINDER_H_
