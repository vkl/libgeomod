#ifndef GEOMOD_ONEDIM_PARAMETERIZATION_H_
#define GEOMOD_ONEDIM_PARAMETERIZATION_H_

#include <geomod/types.h>
#include <geomod/affine/point.h>
#include <geomod/affine/ray.h>
#include <geomod/onedim/interval.h>

namespace geomod{
namespace onedim{

// The Parameterization class represents a curve parameterization.
// It is assumed that the parameterization is invertible.

class Parameterization{
public:
	virtual ~Parameterization(){}

	// Evaluate the parameterization; going from 2D to 3D space
	virtual affine::Point2 operator()(const coord_type &u) const = 0;

	// Evaluate the inverse parameterization
	virtual coord_type operator()(const affine::Point2 &p)  const = 0;

	// Compute intersection of the parameterization with the ray.
	// Returns only positive ray parameter intersections, sorted by increasing ray parameter
	virtual bool RayIntersection(const affine::Ray2 &r, std::vector<affine::Ray2::Intersection> &x) const = 0;
	// Returns only the first intersection; overload this for efficiency.
	// x may be modified even if false is returned.
	virtual bool RayIntersection(const affine::Ray2 &r, affine::Ray2::Intersection &x) const{
		std::vector<affine::Ray2::Intersection> xv;
		bool ret = this->RayIntersection(r, xv);
		if(ret){
			x = xv.front();
		}
		return ret;
	}
	// Returns number of positive ray parameter intersections; overload this for efficiency.
	virtual unsigned RayIntersections(const affine::Ray2 &r) const{
		std::vector<affine::Ray2::Intersection> xv;
		this->RayIntersection(r, xv);
		return xv.size();
	}

	struct DifferentialGeometry{
		affine::Vector2 dp; // tangent vector
		affine::Vector2 ddp; // 2nd derivative
	};
	virtual void Neighborhood(const coord_type &u, DifferentialGeometry &dg) const = 0;

	virtual Interval Bounds() const = 0;
};

// This is the interface class for simply-connected regions on 2D
// manifolds that support parameterization. It is assumed that the
// boundary of the region is piecewise smooth, such that each
// piece corresponds to a parameter chart.

class Parameterizable{
public:
	virtual ~Parameterizable(){}


	typedef std::vector<affine::Point2> chart_polyline;
	// For purposes of rendering
	virtual chart_polyline GetChartPolyline(index_type ichart, const coord_type &tol = 1e-3) const{
		return chart_polyline();
	}

	virtual size_t NumCharts() const = 0;
	virtual const Parameterization* GetChartParameterization(index_type ichart) const = 0;

	// Returns only the first intersection; overload this for efficiency.
	virtual index_type RayIntersection(const affine::Ray2 &r, affine::Ray2::Intersection &x) const{
		index_type ichart_ret = -1;
		for(size_t ichart = 0; ichart < NumCharts(); ++ichart){
			const Parameterization *param = this->GetChartParameterization(ichart);
			affine::Ray2::Intersection xc;
			if(!param->RayIntersection(r, xc)){ continue; }
			if(ichart_ret < 0 || xc < x){
				x = xc;
				ichart_ret = ichart;
			}
		}
		return ichart_ret;
	}
};

} // namespace onedim
} // namespace geomod

#endif // GEOMOD_ONEDIM_PARAMETERIZATION_H_
