#ifndef GEOMOD_ONEDIM_INTERVAL_H_
#define GEOMOD_ONEDIM_INTERVAL_H_

#include <geomod/types.h>
#include <vector>

namespace geomod{
namespace onedim{

// This class represents a simply-connected region in 1D; an interval.

class Interval{
	coord_type ab[2];
public:
	Interval(const coord_type &x0, const coord_type &x1):ab{x0, x1}{}

	const coord_type& beginning() const{ return ab[0]; }
	const coord_type& end()   const{ return ab[1]; }
	const coord_type& operator[](unsigned i) const{ return ab[i]; }
	
	bool Contains(const coord_type &x) const{ return ab[0] <= x && x < ab[1]; }

	coord_type Center() const{
		return 0.5*(ab[0]+ab[1]);
	}
	coord_type Size() const{
		return ab[1]-ab[0];
	}

	bool Empty() const{
		return (ab[1] <= ab[0]);
	}

	static void AsBreakpoints(
		const std::vector<Interval> &intervals,
		const Interval &domain,
		std::vector< std::pair<coord_type, size_t> > &x
	);
};

} // namespace onedim
} // namespace geomod

#endif // GEOMOD_ONEDIM_INTERVAL_H_

