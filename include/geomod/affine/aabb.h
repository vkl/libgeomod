#ifndef GEOMOD_AFFINE_TAABB_H_
#define GEOMOD_AFFINE_TAABB_H_

#include <geomod/types.h>
#include <geomod/affine/vector.h>
#include <geomod/affine/point.h>

namespace geomod{
namespace affine{

// These classes represent axis-aligned bounding boxes (AABBs) in two
// and three dimensions. These are used for computing point and object
// bounds.

template <typename T>
class TAABB2{
public:
	typedef T value_type;
	typedef TPoint2<T> point_type;
private:
	point_type mn, mx;
	void Reset(){
		for(unsigned i = 0; i < 2; ++i){
			mn[i] =  std::numeric_limits<coord_type>::max();
			mx[i] = -std::numeric_limits<coord_type>::max();
		}
	}
public:
	TAABB2(){
		Reset();
	}
	TAABB2(const point_type &a, const point_type &b): mn(a), mx(b) {}
	TAABB2(const TAABB2 &b): mn(b.mn), mx(b.mx) {}
	TAABB2 &operator+=(const TAABB2 &b){
		for(unsigned i = 0; i < 2; ++i){
			mn[i] = std::min(mn[i], b.mn[i]);
			mx[i] = std::max(mx[i], b.mx[i]);
		}
		return *this;
	}
	TAABB2 &operator+=(const point_type &p){
		for(unsigned i = 0; i < 2; ++i){
			mn[i] = std::min(mn[i], p[i]);
			mx[i] = std::max(mx[i], p[i]);
		}
		return *this;
	}

	TAABB2 &operator+=(const coord_type &expand){
		for(unsigned i = 0; i < 2; ++i){
			mn[i] -= expand;
			mx[i] += expand;
		}
		return *this;
	}
	TAABB2 operator+(const TAABB2 &b) const{
		TAABB2 ret(*this);
		ret += b;
		return ret;
	}
	TAABB2 operator+(const coord_type &expand) const{
		TAABB2 ret(*this);
		ret += expand;
		return ret;
	}
	point_type Center() const{
		return point_type::Interpolate(mn, mx, 0.5);
	}
	coord_type Size(unsigned dim) const{
		return mx[dim] - mn[dim];
	}
	const point_type& Min() const{ return mn; }
	const point_type& Max() const{ return mx; }

	bool Empty() const{
		return (mx[0] < mn[0]) || (mx[1] < mn[1]);
	}

	unsigned MaxDim() const{
		return (mx - mn).MaxDim();
	}

	bool Contains(const point_type &p) const{
		return (
			mn[0] < p[0] && p[0] < mx[0] &&
			mn[1] < p[1] && p[1] < mx[1]
		);
	}
	bool Intersects(const point_type &p, const TVector2<value_type> &v_inv, value_type *tnear, value_type *tfar) const{
		// Branch free method here: http://tavianator.com/fast-branchless-raybounding-box-intersections/
		coord_type tx1 = (mn[0] - p[0])*v_inv[0];
		coord_type tx2 = (mx[0] - p[0])*v_inv[0];

		coord_type tmin = std::min(tx1, tx2);
		coord_type tmax = std::max(tx1, tx2);
		for(unsigned i = 1; i < 2; ++i){
			coord_type ty1 = (mn[i] - p[i])*v_inv[i];
			coord_type ty2 = (mx[i] - p[i])*v_inv[i];

			tmin = std::max(tmin, std::min(ty1, ty2));
			tmax = std::min(tmax, std::max(ty1, ty2));
		}
		if(tmax > tmin){
			*tnear = tmin;
			*tfar  = tmax;
			return true;
		}else{
			return false;
		}
	}
	bool Intersects(const point_type &p, const TVector2<value_type> &v_inv) const{
		value_type tnear = 0, tfar = 0;
		return Intersects(p, v_inv, &tnear, &tfar);
	}
	bool Intersects(const TAABB2 &b) const{
		for(unsigned i = 0; i < 2; ++i){
			if((mx[i] < b.mn[i]) || (mn[i] > b.mn[i])){ return false; }
		}
		return true;
	}

	point_type Vertex(unsigned i) const{
		i &= 0x3;
		switch(i){
		case 0:
			return mn;
		case 1:
			return Point2(mx[0], mn[1]);
		case 2:
			return Point2(mn[0], mx[1]);
		default:
			return mx;
		}
	}
};

template <typename T>
class TAABB3{
public:
	typedef T value_type;
	typedef TPoint3<T> point_type;
private:
	point_type mn, mx;
	void Reset(){
		for(unsigned i = 0; i < 3; ++i){
			mn[i] =  std::numeric_limits<coord_type>::max();
			mx[i] = -std::numeric_limits<coord_type>::max();
		}
	}
public:
	TAABB3(){
		Reset();
	}
	TAABB3(const point_type &a, const point_type &b): mn(a), mx(b) {}
	TAABB3(const TAABB3 &b): mn(b.mn), mx(b.mx) {}
	TAABB3 &operator+=(const TAABB3 &b){
		for(unsigned i = 0; i < 3; ++i){
			mn[i] = std::min(mn[i], b.mn[i]);
			mx[i] = std::max(mx[i], b.mx[i]);
		}
		return *this;
	}
	TAABB3 &operator+=(const point_type &p){
		for(unsigned i = 0; i < 3; ++i){
			mn[i] = std::min(mn[i], p[i]);
			mx[i] = std::max(mx[i], p[i]);
		}
		return *this;
	}

	TAABB3 &operator+=(const coord_type &expand){
		for(unsigned i = 0; i < 3; ++i){
			mn[i] -= expand;
			mx[i] += expand;
		}
		return *this;
	}
	TAABB3 operator+(const TAABB3 &b) const{
		TAABB3 ret(*this);
		ret += b;
		return ret;
	}
	TAABB3 operator+(const coord_type &expand) const{
		TAABB3 ret(*this);
		ret += expand;
		return ret;
	}
	point_type Center() const{
		return point_type::Interpolate(mn, mx, 0.5);
	}
	coord_type Size(unsigned dim) const{
		return mx[dim] - mn[dim];
	}
	const point_type& Min() const{ return mn; }
	const point_type& Max() const{ return mx; }

	bool Empty() const{
		return (mx[0] < mn[0]) || (mx[1] < mn[1]) || (mx[2] < mn[2]);
	}

	unsigned MaxDim() const{
		return (mx - mn).MaxDim();
	}

	bool Contains(const point_type &p) const{
		return (
			mn[0] <= p[0] && p[0] < mx[0] &&
			mn[1] <= p[1] && p[1] < mx[1] &&
			mn[2] <= p[2] && p[2] < mx[2]
		);
	}
	bool Intersects(const point_type &p, const TVector3<value_type> &v_inv, value_type *tnear, value_type *tfar) const{
		// Branch free method here: http://tavianator.com/fast-branchless-raybounding-box-intersections/
		coord_type tx1 = (mn[0] - p[0])*v_inv[0];
		coord_type tx2 = (mx[0] - p[0])*v_inv[0];

		coord_type tmin = std::min(tx1, tx2);
		coord_type tmax = std::max(tx1, tx2);
		for(unsigned i = 1; i < 3; ++i){
			coord_type ty1 = (mn[i] - p[i])*v_inv[i];
			coord_type ty2 = (mx[i] - p[i])*v_inv[i];

			tmin = std::max(tmin, std::min(ty1, ty2));
			tmax = std::min(tmax, std::max(ty1, ty2));
		}
		if(tmax > tmin){
			*tnear = tmin;
			*tfar  = tmax;
			return true;
		}else{
			return false;
		}
	}
	bool Intersects(const point_type &p, const TVector3<value_type> &v_inv) const{
		value_type tnear = 0, tfar = 0;
		return Intersects(p, v_inv, &tnear, &tfar);
	}
	bool Intersects(const TAABB3 &b) const{
		for(unsigned i = 0; i < 3; ++i){
			if((mx[i] < b.mn[i]) || (mn[i] > b.mn[i])){ return false; }
		}
		return true;
	}
	point_type Vertex(unsigned i) const{
		i &= 0x7;
		point_type ret(mn);
		for(unsigned idim = 0; idim < 3; ++idim){
			if(i & (0x1 << idim)){
				ret[idim] = mx[idim];
			}
		}
		return ret;
	}
};

typedef TAABB2<coord_type> AABB2;
typedef TAABB3<coord_type> AABB3;

} // namespace affine
} // namespace geomod

#endif  // GEOMOD_AFFINE_TAABB_H_
