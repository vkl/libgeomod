#ifndef GEOMOD_AFFINE_VECTOR_H_
#define GEOMOD_AFFINE_VECTOR_H_

#include <geomod/types.h>

namespace geomod{
namespace affine{

// A vector defines a displacement between two points.
// In homogeneous coordinates, a 2D vector has the form (x, y, 0).
// That is, translating a vector leaves it unchanged; it is still the
// same relative displacement.

template <typename T>
class TVector2{
public:
	typedef T value_type;
private:
	value_type v[2];
public:
	// Default constructor
	TVector2(){
		v[0] = 0; v[1] = 0;
	}
	TVector2(const value_type &x, const value_type &y){
		v[0] = x;
		v[1] = y;
	}

	// Direct component access
	value_type x() const{ return v[0]; }
	value_type y() const{ return v[1]; }
	const value_type &operator[](unsigned i) const{ return v[i]; }
	value_type &operator[](unsigned i){ return v[i]; }

	// Various norms
	value_type NormSq() const{ return v[0] * v[0] + v[1] * v[1]; }
	value_type Norm() const{ return std::hypot(v[0], v[1]); }
	value_type Norm1() const{
		return std::abs(v[0]) + std::abs(v[1]);
	}

	// Normalization to a given length
	value_type Normalize(const value_type &newlen = 1){
		value_type n = Norm();
		value_type in = newlen / n;
		v[0] *= in; v[1] *= in;
		return n;
	}
	value_type Angle() const{ // returns degrees
		return (std::atan2(v[1], v[0]) / M_PI) * value_type(180);
	}

	unsigned MinDim() const{
		unsigned i = 0;
		if(v[1] < v[i]){ i = 1; }
		return i;
	}
	unsigned MaxDim() const{
		unsigned i = 0;
		if(v[1] > v[i]){ i = 1; }
		return i;
	}

	TVector2 operator+(const TVector2 &rhs) const{
		return TVector2(v[0] + rhs[0], v[1] + rhs[1]);
	}
	TVector2 operator-(const TVector2 &rhs) const{
		return TVector2(v[0] - rhs[0], v[1] - rhs[1]);
	}

	TVector2& operator+=(const TVector2 &rhs){
		v[0] += rhs[0];
		v[1] += rhs[1];
		return *this;
	}
	TVector2& operator-=(const TVector2 &rhs){
		v[0] -= rhs[0];
		v[1] -= rhs[1];
		return *this;
	}

	TVector2& operator*=(const value_type &s){
		v[0] *= s;
		v[1] *= s;
		return *this;
	}
	TVector2 operator*(const value_type &s) const{
		return TVector2(v[0] * s, v[1] * s);
	}

	TVector2& operator/=(const value_type &s){
		v[0] /= s;
		v[1] /= s;
		return *this;
	}
	TVector2 operator/(const value_type &s) const{
		return TVector2(v[0] / s, v[1] / s);
	}

	TVector2 operator-() const{
		return TVector2(-v[0], -v[1]);
	}

	static TVector2 X(){
		static const TVector2 vx(value_type(1), value_type(0));
		return vx;
	}
	static TVector2 Y(){
		static const TVector2 vy(value_type(0), value_type(1));
		return vy;
	}
	static TVector2 Zero(){
		static const TVector2 zero(value_type(0), value_type(0));
		return zero;
	}

	// Scalar cross product
	static value_type Cross(const TVector2 &a, const TVector2 &b){
		return (a.v[0] * b.v[1] - a.v[1] * b.v[0]);
	}
	static value_type Dot(const TVector2 &a, const TVector2 &b){
		return value_type(a.v[0] * b.v[0] + a.v[1] * b.v[1]);
	}
	// Rotation by 90 deg
	static TVector2 Rot(const TVector2 &u){
		return TVector2(-u.v[1], u.v[0]);
	}
	
	static TVector2 Normalize(const TVector2 &u){
		TVector2 v(u); v.Normalize();
		return v;
	}
};

template <typename T>
inline bool operator==(const TVector2<T> &a, const TVector2<T> &b){
	return a[0] == b[0] && a[1] == b[1];
}

template <typename S, typename T>
inline TVector2<T> operator*(const S &s, const TVector2<T> &a){
	return TVector2<T>(s * a[0], s * a[1]);
}

template <typename T>
inline std::ostream& operator<<(std::ostream &s, const TVector2<T> &v){
	s << '[' << v[0] << ',' << v[1] << ']';
	return s;
}

template <typename T>
class TVector3{
public:
	typedef T value_type;
private:
	value_type v[3];
public:
	// Default constructor
	TVector3(){
		v[0] = 0; v[1] = 0; v[2] = 0;
	}
	TVector3(const value_type &x, const value_type &y, const value_type &z){
		v[0] = x;
		v[1] = y;
		v[2] = z;
	}
	TVector3(const TVector2<value_type> &v2, const value_type &z){ v[0] = v2[0]; v[1] = v2[1]; v[2] = z; }

	value_type x() const{ return v[0]; }
	value_type y() const{ return v[1]; }
	value_type z() const{ return v[2]; }
	const value_type &operator[](unsigned i) const{ return v[i]; }
	value_type &operator[](unsigned i){ return v[i]; }

	value_type NormSq() const{ return v[0] * v[0] + v[1] * v[1] + v[2] * v[2]; }
	value_type Norm1() const{
		return std::abs(v[0]) + std::abs(v[1]) + std::abs(v[2]);
	}
	value_type NormInf() const{
		value_type a = std::abs(v[0]);
		value_type b = std::abs(v[1]);
		if(b > a){ a = b; }
		b = std::abs(v[2]);
		if(b > a){ a = b; }
		return a;
	}
	value_type Norm() const{
		return sqrt(NormSq());
		/*
		value_type a[3] = { std::abs(v[0]), std::abs(v[1]), std::abs(v[2]) };
		value_type w = a[0];
		if(a[1] > w){ w = a[1]; }
		if(a[2] > w){ w = a[2]; }
		if(0 == w){
			return a[0] + a[1] + a[2];
		}else{
			a[0] /= w; a[1] /= w; a[2] /= w;
			w *= sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2]);
			return w;
		}
		*/
	}
	value_type Normalize(const value_type &newlen = 1){
		const value_type n = Norm();
		value_type in = newlen / n;
		v[0] *= in; v[1] *= in; v[2] *= in;
		return n;
	}
	unsigned MinDim() const{
		unsigned i = 0;
		if(v[1] < v[i]){ i = 1; }
		if(v[2] < v[i]){ i = 2; }
		return i;
	}
	unsigned MaxDim() const{
		unsigned i = 0;
		if(v[1] > v[i]){ i = 1; }
		if(v[2] > v[i]){ i = 2; }
		return i;
	}

	TVector3 operator+(const TVector3 &rhs) const{
		return TVector3(v[0] + rhs[0], v[1] + rhs[1], v[2] + rhs[2]);
	}
	TVector3 operator-(const TVector3 &rhs) const{
		return TVector3(v[0] - rhs[0], v[1] - rhs[1], v[2] - rhs[2]);
	}

	TVector3& operator+=(const TVector3 &rhs){
		v[0] += rhs[0];
		v[1] += rhs[1];
		v[2] += rhs[2];
		return *this;
	}
	TVector3& operator-=(const TVector3 &rhs){
		v[0] -= rhs[0];
		v[1] -= rhs[1];
		v[2] -= rhs[2];
		return *this;
	}
	TVector3& operator*=(const value_type &s){
		v[0] *= s;
		v[1] *= s;
		v[2] *= s;
		return *this;
	}
	TVector3 operator*(const value_type &s) const{
		return TVector3(v[0] * s, v[1] * s, v[2] * s);
	}
	TVector3& operator/=(const value_type &s){
		v[0] /= s;
		v[1] /= s;
		v[2] /= s;
		return *this;
	}
	TVector3 operator/(const value_type &s) const{
		return TVector3(v[0] / s, v[1] / s, v[2] / s);
	}
	TVector3 operator-() const{
		return TVector3(-v[0], -v[1], -v[2]);
	}

	TVector2<value_type> Projection() const{
		return TVector2<value_type>(v[0] / v[2], v[1] / v[2]);
	}

	static TVector3 X(){
		static const TVector3 vx(value_type(1), value_type(0), value_type(0));
		return vx;
	}
	static TVector3 Y(){
		static const TVector3 vy(value_type(0), value_type(1), value_type(0));
		return vy;
	}
	static TVector3 Z(){
		static const TVector3 vz(value_type(0), value_type(0), value_type(1));
		return vz;
	}
	static TVector3 Zero(){
		static const TVector3 zero(value_type(0), value_type(0), value_type(0));
		return zero;
	}
	static TVector3 Cross(const TVector3 &a, const TVector3 &b){
		return TVector3(
			 a[1] * b[2] - a[2] * b[1],
			 a[2] * b[0] - a[0] * b[2],
			 a[0] * b[1] - a[1] * b[0]
		);
	}
	static value_type Dot(const TVector3 &a, const TVector3 &b){
		return value_type(a[0] * b[0] + a[1] * b[1] + a[2] * b[2]);
	}
	
	static TVector3 Normalize(const TVector3 &u){
		TVector3 v(u); v.Normalize();
		return v;
	}

	static void Orth(const TVector3 &a, TVector3 *b, TVector3 *c = NULL){
		// Given a vector a, generate orthogonal unit vectors b and c such that
		//   normalize(a) x b = c
		TVector3 an(a);
		an.Normalize();
		if(std::abs(an[0]) > std::abs(an[2])){
			value_type invLen = value_type(1) / std::hypot(an[0], an[1]);
			(*b)[0] = -an[1] * invLen;
			(*b)[1] = an[0] * invLen;
			(*b)[2] = 0;
		}else{
			value_type invLen = value_type(1) / std::hypot(an[1], an[2]);
			(*b)[0] = 0;
			(*b)[1] = an[2] * invLen;
			(*b)[2] = -an[1] * invLen;
		}
		if(NULL != c){ *c = TVector3::Cross(an, *b); }
	}
	// Given two NORMALIZED vectors, return their dot product in c and cross product in s
	static void DotCross(const TVector3 &a, const TVector3 &b, value_type *c, value_type *s){
		static const value_type one(1);
		*c = TVector3::Dot(a, b);
		if(*c < value_type(0.707)){ *s = sqrt((one - (*c)) * (one + (*c))); }
		else{ *s = TVector3::Cross(a, b).Norm(); }
	}
};

template <typename T>
inline bool operator==(const TVector3<T> &a, const TVector3<T> &b){
	return a[0] == b[0] && a[1] == b[1] && a[2] == b[2];
}

template <typename S, typename T>
inline TVector3<T> operator*(const S &s, const TVector3<T> &a){
	return TVector3<T>(s * a[0], s * a[1], s * a[2]);
}

template <typename T>
inline std::ostream& operator<<(std::ostream &s, const TVector3<T> &v){
	s << '[' << v[0] << ',' << v[1] << ',' << v[2] << ']';
	return s;
}

template <typename T>
class TVector4{
public:
	typedef T value_type;
private:
	value_type v[4];
public:
	// Default constructor
	TVector4(){
		v[0] = 0; v[1] = 0; v[2] = 0; v[3] = 0;
	}
	TVector4(const value_type &x, const value_type &y, const value_type &z, const value_type &w){
		v[0] = x;
		v[1] = y;
		v[2] = z;
		v[3] = w;
	}
	TVector4(const TVector3<value_type> &v3, const value_type &w){ v[0] = v3[0]; v[1] = v3[1]; v[2] = v3[2]; v[3] = w; }

	value_type x() const{ return v[0]; }
	value_type y() const{ return v[1]; }
	value_type z() const{ return v[2]; }
	value_type w() const{ return v[3]; }
	const value_type &operator[](unsigned i) const{ return v[i]; }
	value_type &operator[](unsigned i){ return v[i]; }

	value_type NormSq() const{ return v[0] * v[0] + v[1] * v[1] + v[2] * v[2] + v[3] * v[3]; }
	value_type Norm1() const{
		return std::abs(v[0]) + std::abs(v[1]) + std::abs(v[2]) + std::abs(v[3]);
	}
	value_type NormInf() const{
		value_type a = std::abs(v[0]);
		value_type b = std::abs(v[1]);
		if(b > a){ a = b; }
		b = std::abs(v[2]);
		if(b > a){ a = b; }
		b = std::abs(v[3]);
		if(b > a){ a = b; }
		return a;
	}
	value_type Norm() const{
		return sqrt(NormSq());
	}
	value_type Normalize(const value_type &newlen = 1){
		const value_type n = Norm();
		value_type in = newlen / n;
		v[0] *= in; v[1] *= in; v[2] *= in; v[3] *= in;
		return n;
	}
	unsigned MinDim() const{
		unsigned i = 0;
		if(v[1] < v[i]){ i = 1; }
		if(v[2] < v[i]){ i = 2; }
		if(v[3] < v[i]){ i = 3; }
		return i;
	}
	unsigned MaxDim() const{
		unsigned i = 0;
		if(v[1] > v[i]){ i = 1; }
		if(v[2] > v[i]){ i = 2; }
		if(v[3] > v[i]){ i = 3; }
		return i;
	}

	TVector4 operator+(const TVector4 &rhs) const{
		return TVector4(v[0] + rhs[0], v[1] + rhs[1], v[2] + rhs[2], v[3] + rhs[3]);
	}
	TVector4 operator-(const TVector4 &rhs) const{
		return TVector4(v[0] - rhs[0], v[1] - rhs[1], v[2] - rhs[2], v[3] - rhs[3]);
	}

	TVector4& operator+=(const TVector4 &rhs){
		v[0] += rhs[0];
		v[1] += rhs[1];
		v[2] += rhs[2];
		v[3] += rhs[3];
		return *this;
	}
	TVector4& operator-=(const TVector4 &rhs){
		v[0] -= rhs[0];
		v[1] -= rhs[1];
		v[2] -= rhs[2];
		v[3] -= rhs[3];
		return *this;
	}
	TVector4& operator*=(const value_type &s){
		v[0] *= s;
		v[1] *= s;
		v[2] *= s;
		v[3] *= s;
		return *this;
	}
	TVector4 operator*(const value_type &s) const{
		return TVector4(v[0] * s, v[1] * s, v[2] * s, v[3] * s);
	}
	TVector4& operator/=(const value_type &s){
		v[0] /= s;
		v[1] /= s;
		v[2] /= s;
		v[3] /= s;
		return *this;
	}
	TVector4 operator/(const value_type &s) const{
		return TVector4(v[0] / s, v[1] / s, v[2] / s, v[3] / s);
	}
	TVector4 operator-() const{
		return TVector4(-v[0], -v[1], -v[2], -v[3]);
	}

	TVector3<value_type> Projection() const{
		return TVector3<value_type>(v[0] / v[3], v[1] / v[3], v[2] / v[3]);
	}
};

template <typename T>
inline bool operator==(const TVector4<T> &a, const TVector4<T> &b){
	return a[0] == b[0] && a[1] == b[1] && a[2] == b[2] && a[3] == b[3];
}

template <typename S, typename T>
inline TVector4<T> operator*(const S &s, const TVector4<T> &a){
	return TVector4<T>(s * a[0], s * a[1], s * a[2], s * a[3]);
}

template <typename T>
inline std::ostream& operator<<(std::ostream &s, const TVector4<T> &v){
	s << '[' << v[0] << ',' << v[1] << ',' << v[2] << ',' << v[3] << ']';
	return s;
}

typedef TVector2<coord_type> Vector2;
typedef TVector3<coord_type> Vector3;
typedef TVector4<coord_type> Vector4;

} // namespace affine
} // namespace geomod

#endif  // GEOMOD_AFFINE_VECTOR_H_
