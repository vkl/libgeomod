#ifndef GEOMOD_AFFINE_RAY_H_
#define GEOMOD_AFFINE_RAY_H_

#include <geomod/types.h>
#include <geomod/affine/vector.h>
#include <geomod/affine/point.h>

namespace geomod{
namespace affine{

// A ray is the combination of a point and a vector.
// A ray simultaneous can define a line segment (the segment from
// p to p+d), or an infinite line (the extension of the ray).

// For 2D rays, we provide a routine to compute ray-ray intersection,
// which also serves as a general routine for segment-segment, or
// ray-line, etc. intersections.

template <typename T>
class TRay2{
public:
	typedef T value_type;
private:
	TPoint2<value_type> p;
	TVector2<value_type> d;
public:
	TRay2(const TPoint2<value_type> &p, const TVector2<value_type> &d) : p(p), d(d) {}

	const TPoint2<value_type>&  origin() const { return p; }
	const TVector2<value_type>& dir()    const { return d; }
	
	TPoint2<value_type> operator()(const value_type &t) const {
		return p + t * d;
	}

	struct Intersection{
		value_type s;          // curve parameter
		value_type t;          // ray parameter
		TPoint2<value_type> p; // intersection point

		bool operator<(const Intersection &rhs) const{ return t < rhs.t; }
	};
	bool Intersect(const TRay2 &r, Intersection &x) const{
		// Returns intersection of two rays.
		// Note, this always returns an intersection if the rays are not parallel.
		// Therefore, the ray parameters may be negative!
		//
		// Solve: p + s*d == r.p + t*r.d
		// This is the solution to a 2x2 linear system:
		//   [ d  r.d ] * [ s; -t ] == [ r.p - p ]
		// or:
		//   [ d  r.d ] * [ -s; t ] == [ p - r.p ]
		// the solution is:
		//   [ -s ] = [ r.d[1]  -r.d[0] ] [ p - r.p ]
		//   [  t ]   [ -d[1]     d[0]  ]
		// or
		//   [  s ] = [ -r.d[1]  r.d[0] ] [ p - r.p ]
		//   [  t ]   [ -d[1]     d[0]  ]
		const value_type denom = TVector2<value_type>::Cross(d, r.d);
		if(0 == denom){ return false; }
		const TVector2<value_type> b(p - r.p);
		x.s = TVector2<value_type>::Cross(r.d, b) / denom;
		x.t = TVector2<value_type>::Cross(d, b) / denom;
		x.p = p + x.s * d;
		return true;
	}

	coord_type Nearest(const TPoint2<value_type> &q) const{
		// compute parameter value of point on ray (possibly negative) nearest to q)
		return TVector2<value_type>::Dot(q - p, d) / d.NormSq();
	}
};

template <typename T>
class TRay3{
public:
	typedef T value_type;
private:
	TPoint3<value_type> p;
	TVector3<value_type> d;
public:
	TRay3(const TPoint3<value_type> &p, const TVector3<value_type> &d) : p(p), d(d) {}

	const TPoint3<value_type>&  origin() const { return p; }
	const TVector3<value_type>& dir()    const { return d; }

	TPoint3<value_type> operator()(const value_type &t) const {
		return p + t * d;
	}

	struct Intersection{
		value_type t;           // ray parameter
		TPoint2<value_type> uv; // surface parameter
		TPoint3<value_type> p;  // intersection point

		bool operator<(const Intersection &rhs) const{ return t < rhs.t; }
	};
};

typedef TRay2<coord_type> Ray2;
typedef TRay3<coord_type> Ray3;

} // namespace affine
} // namespace geomod

#endif  // GEOMOD_AFFINE_RAY_H_
