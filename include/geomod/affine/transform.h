#ifndef GEOMOD_AFFINE_TRANSFORM_H_
#define GEOMOD_AFFINE_TRANSFORM_H_

#include <geomod/types.h>
#include <geomod/affine/vector.h>
#include <geomod/affine/point.h>
#include <geomod/affine/ray.h>
#include <geomod/affine/aabb.h>
#include <geomod/util/specfun.h>
#include <geomod/util/quaternion.h>
#include <cstring> // for memcmp

namespace geomod{
namespace affine{

template <typename T>
class TTransform2 {
	// A 2D transformation can be represented by a rotation and a translation:
	// [ cs -sn x[0] ]
	// [ sn  cs x[1] ]
	T cs, sn, x, y;
public:
	TTransform2(
		const T &cs,
		const T &sn,
		const T &x,
		const T &y
	) : cs(cs), sn(sn), x(x), y(y) {
	}
	TTransform2(): cs(T(1)), sn(T(0)), x(T(0)), y(T(0)) {
	}
	TTransform2(const TVector2<T> &translation):
		cs(T(1)), sn(T(0)),
		x(translation[0]),
		y(translation[1])
	{
	}
	TTransform2(const T &rotation_degrees): x(T(0)), y(T(0)) {
		specfun::cossind(rotation_degrees, &cs, &sn);
	}

	static TTransform2<T> Translate(const TVector2<T> &v) {
		return TTransform2<T>(v);
	}
	static TTransform2<T> Rotate(const T &angle_degrees) {
		return TTransform2<T>(angle_degrees);
	}

	static TTransform2<T> Inverse(const TTransform2<T> &x) {
		return TTransform2<T>(
			x.cs, -x.sn,
			-(x.cs*x.x + x.sn*x.y),
			-(x.cs*x.y - x.sn*x.x)
		);
	}

	bool Equals(const TTransform2<T> &b) const {
		return cs == b.cs && sn == b.sn && x == b.x && y == b.y;
	}

	// Decomose the transform into a translation and rotation
	// (rotation is applied first)
	void Decompose(TVector2<T> &translation, T &rotation_degrees) const {
		rotation_degrees = T(180) * (std::atan2(sn, cs) / M_PI);
		translation[0] = x;
		translation[1] = y;
	}

	// Return the 2x3 matrix corresponding to the transform:
	// [ m[0] m[2] m[4] ]
	// [ m[1] m[3] m[5] ]
	void AsMatrix(T *m) const {
		m[0] = cs;
		m[1] = sn;
		m[2] = -sn;
		m[3] = cs;
		m[4] = x;
		m[5] = y;
	}

	TTransform2<T> operator*(const TTransform2<T> &b) const {
		// [ cs_ -sn_ x[0] ]   [ x.cs_ -x.sn_ x.x[0] ]
		// [ sn_  cs_ x[1] ] * [ x.sn_  x.cs_ x.x[1] ]
		// [  0    0    1  ]   [   0      0     1    ]
		return TTransform2<T>(
			 cs * b.cs - sn * b.sn,
			 sn * b.cs + cs * b.sn,
			 cs * b.x  - sn * b.y + x,
			 sn * b.x  + cs * b.y + y
		);
	}

	// Return the inverse of this transform
	TTransform2<T> operator~() const {
		return TTransform2<T>(
			 cs, -sn,
			 -(cs * x + sn * y),
			 -(cs * y - sn * x)
		);
	}


	TPoint2<T> operator()(const TPoint2<T> &p) const {
		return TPoint2<T>(
			 cs * p[0] - sn * p[1] + x,
			 cs * p[1] + sn * p[0] + y
		);
	}
	TVector2<T> operator()(const TVector2<T> &v) const {
		return TVector2<T>(
			 cs * v[0] - sn * v[1],
			 cs * v[1] + sn * v[0]
		);
	}
	TRay2<T> operator()(const TRay2<T> &r) const {
		return TRay2<T>((*this)(r.origin()), (*this)(r.dir()));
	}
	TAABB2<T> operator()(const TAABB2<T> &b) const {
		TAABB2<T> bound;
		for(unsigned i = 0; i < 4; ++i){
			bound += (*this)(b.Vertex(i));
		}
		return bound;
	}

	// Homogeneous coordinate transforms
	TPoint3<T> operator()(const TPoint3<T> &p) const {
		return TPoint3<T>(
			 cs * p[0] - sn * p[1] + x * p[2],
			 cs * p[1] + sn * p[0] + y * p[2],
			 p[2]
		);
	}
	TVector3<T> operator()(const TVector3<T> &v) const {
		return TVector3<T>(
			 cs * v[0] - sn * v[1] + x * v[2],
			 cs * v[1] + sn * v[0] + y * v[2],
			 v[2]
		);
	}
};

template <typename T>
inline bool operator==(const TTransform2<T> &a, const TTransform2<T> &b) {
	return a.Equals(b);
}

template <typename T>
inline std::ostream& operator<<(std::ostream &s, const TTransform2<T> &x) {
	T m[6];
	x.AsMatrix(m);
	s << '['
		<< '[' << m[0] << ',' << m[2] << ',' << m[4] << "],"
		<< '[' << m[1] << ',' << m[3] << ',' << m[5] << "]]";
	return s;
}

/*
We will represent 3-dimensional rigid transformations using dual quaternions.
A dual quaternion is
	q = qr + qd*e
where e*e = 0. The real and dual parts of q are both quaternions, e.g.:
	qr = w + x*i + y*j + z*k = w + <v>
where
	i*i = j*j = k*k = -1,
	i*j =  k, j*k =  i, k*i =  j
	j*i = -k, k*j = -i, i*k = -j

Rigid transformations are represented by unit dual quaternions satisfying
	q*conj(q) = 1, where conj(q) = conj(qr) + conj(qd)*e
and, e.g.
	conj(qr) = w - x*i - y*j - z*k
Therefore, a unit dual quaternion satisfies
	(qr + qd * e) * (conj(qr) + conj(qd)*e)
	qr*conj(qr) == 1
	qd*conj(qr) + qr*conj(qd) == 0
A pure rotation can be represented by q = qr, where
	qr = cos(theta/2) + sin(theta/2) <v>
where v is the unit vector of the axis of rotation and theta is the angle
of the rotation about the axis [1].
A pure translation can be represented by q = 1 + qd*e, where
	qd = <v/2>
where v is the translation vector [2].

Application of these transformations is performed by:
	p' = q*<p>*Conj(q)
where Conj(q) = conj(qr) - conj(qd) * e. Note this is not the same conjugate
as above: Conj(q) != conj(q).
Note that the scalar and dual components should be zero [3].

[0] Vector representation:
A homogeneous point r+<p> = (x,y,z,r) is represented by r + <p>*e

Quaternion multiplication:
q * <p> = (w + <v>) * (r + <p>)
				= w*r - v.p + w*<p> + r*<v> + vxp
where v.p = dot(v, p) and vxp = cross(v, p).

We must also ensure that the product of two unit dual quaternions remains unit.
q1*q2 = (qr1 + qd1*e)*(qr2 + qd2*e)
			= qr1*qr2 + (qd1*qr2 + qr1*qd2)*e
Thus we require that qr1*qr2 is a unit real quaternion, which is trivially satisfied,
and also that
conj(qr1*qr2)*(qd1*qr2 + qr1*qd2) + conj(qd1*qr2 + qr1*qd2)*(qr1*qr2) = 0
	= conj(qr2)*conj(qr1)*(qd1*qr2 + qr1*qd2) + (conj(qr2)*conj(qd1) + conj(qd2)*conj(qr1))*(qr1*qr2)
	= conj(qr2)*conj(qr1)*qd1*qr2 + conj(qr2)*conj(qr1)*qr1*qd2 + conj(qr2)*conj(qd1)*qr1*qr2 + conj(qd2)*conj(qr1)*qr1*qr2
	= conj(qr2)*(conj(qr1)*qd1 + conj(qd1)*qr1)*qr2 + conj(qr2)*conj(qr1)*qr1*qd2 + conj(qd2)*conj(qr1)*qr1*qr2
Since we started with conj(qr1)*qd1 + conj(qd1)*qr1 = 0, and conj(qr1)*qr1 = 1
	= conj(qr2)*qd2 + conj(qd2)*qr2
Since we also have conj(qr2)*qd2 + conj(qd2)*qr2 = 0, the condition is satisfied.

[1] Proof:
Let q = cos(theta/2) + sin(theta/2)*<u> where u.u = 1. Let r = 1:
p' = q*(r + <p>*e)*conj(q) = (w + <v>)*(r + <p>*e)*(w - <v>)
	 = (w*r - v.p*e + w*<p>*e + r*<v> + vxp*e) * (w - <v>)
	 = w*w*r - w*(v.p)*e + w*(p.v)*e + r*v.v - w*r*<v> + (v.p)*<v>*e + w*w<p>*e + w*r*<v> + w*vxp*e - w*pxv*e - r*vxv - (vxp)xv*e
	 = r*(w*w+v.v) + (v.p)*<v>*e + w*w<p>*e + w*vxp*e - w*pxv*e - r*vxv + vx(vxp)*e
	 = r + (v.p)v*e + w*w*<p>*e + 2*w*vxp*e + vx(vxp)*e
	 = [sin^2(theta/2) outer(u, u) + cos^2(theta/2) I + 2*cos(theta/2)sin(theta/2) cross(u) + sin^2(theta/2) cross(u)^2 (outer(u, u) - I) ] p
where outer(x, y) is the outer product of x and y, and cross(u) is the cross product matrix formed from vector u.
	 = [ (1 - cos(theta)) outer(u,u) + cos(theta) I + sin(theta) cross(u) ] p
which is the standard equation for a transformation matrix acting on p.

[2] Proof:
Let q = 1 + <v/2>*e
p' = (1 + <v/2>*e) * (r + <p>*e) * (1 - <v/2>*e)
	 = (r + <p>*e + r*<v/2>*e) * (1 - <v/2>*e)
	 = r + <p>*e + r*<v/2>*e + r*<v/2>*e
	 = r + <p>*e + r*<v>*e

*/

template <typename T>
class TTransform3 {
	// A 3D transformation can be represented by a dual quaternion:
	// Q = Qr + e Qd
	// Qr = q_[0][0] + i q_[0][1] + j q_[0][2] + k q_[0][3]
	// Qd = q_[1][0] + i q_[1][1] + j q_[1][2] + k q_[1][3]
	// i*i = j*j = k*k = -1
	// i*j =  k, j*k =  i, k*i =  j
	// j*i = -k, k*j = -i, i*k = -j
	// e*e =  0
	util::TQuaternion<T> qr, qd;
public:
	TTransform3():qr(TVector3<T>::Zero(), 1), qd(TVector3<T>::Zero(), 0) {}
	TTransform3(const util::Quaternion &qr, const util::Quaternion &qd):qr(qr), qd(qd){}
	TTransform3(const TVector3<T> &translation):qr(TVector3<T>::Zero(), 1), qd(T(0.5) * translation, 0) {}
	TTransform3(const TVector3<T> &axis, const T &rotation_degrees):qd(Vector3::Zero(), 0) {
		if(0 == rotation_degrees){
			qr = util::TQuaternion<T>(Vector3::Zero(), 1);
		}else{
			T cs, sn;
			specfun::cossind(0.5 * rotation_degrees, &cs, &sn);
			TVector3<T> n(axis); n.Normalize();
			qr = util::TQuaternion<T>(sn*n, cs);
		}
	}
	TTransform3(const util::TQuaternion<T> &q):qr(q), qd(TVector3<T>::Zero(), 0) {}

	static TTransform3<T> Translate(const TVector3<T> &v) {
		return TTransform3<T>(v);
	}
	static TTransform3<T> Rotate(const TVector3<T> &axis, const T &angle_degrees) {
		return TTransform3<T>(axis, angle_degrees);
	}
	static TTransform3<T> Rotate(const TVector3<T> &src, const TVector3<T> &dst) {
		TVector3<T> a(src); a.Normalize();
		TVector3<T> b(dst); b.Normalize();
		TVector3<T> axis(TVector3<T>::Cross(a, b));
		const T dot(Vector3::Dot(a, b));
		const T cross(axis.Norm());
		if(0 == cross){
			if(dot > 0){
				return TTransform3<T>(axis, 0);
			}else{
				TVector3<T>::Orth(src, &axis);
				return TTransform3<T>(axis, 180);
			}
		}else{
			return TTransform3<T>(axis, (std::atan2(cross, dot) / M_PI) * 180);
		}
	}
	static TTransform3<T> Inverse(const TTransform3<T> &x) {
		return TTransform3<T>(x.qr.Conjugate(), x.qd.Conjugate());
	}

	bool Equals(const TTransform3<T> &x) const {
		return qr == x.qr && qd == x.qd;
	}

	// Decomose the transform into a translation and rotation
	// (rotation is applied first)
	void Decompose(
		TVector3<T> &translation,
		TVector3<T> &axis, T &rotation_degrees
	) const {
		const T cs = qr.Scalar(); //q_[0][0];
		const T sn = qr.Vector().Norm();
		rotation_degrees = (std::atan2(sn, cs) / M_PI) * 360;
		axis = qr.Vector();
		// t = 2 Qd Qr*
		//   = Qd Qr* - Qr Qd*
		//   = (wd + <vd>) (wr - <vr>) - (wr + <vr>) (wd - <vd>)
		//   = (wd wr + vd.vr) + (vec) - (wr wd + vr.vd) + (vec)
		// thus proving that this always results in zero scalar component.
		//   = 2 [ (wd + <vd>) (wr - <vr>) ]
		//   = 2 [ wd wr + vd.vr + wr<vd> - wd<vr> + vdxvr ]
		//   = 2 [ wr<vd> - wd<vr> + vdxvr ]
		translation = (T(2) * qd * qr.Conjugate()).Vector();
	}

	// Return the 3x4 matrix corresponding to the transform:
	// [ m[0] m[3] m[6] m[ 9] ]
	// [ m[1] m[4] m[7] m[10] ]
	// [ m[2] m[5] m[8] m[11] ]
	void AsMatrix(T *m) const {
		qr.AsMatrix(m);
		TVector3<T> translation = T(2) * (qd * qr.Conjugate()).Vector();
		m[ 9] = translation[0];
		m[10] = translation[1];
		m[11] = translation[2];
	}
	void AsDualQuaternion(util::TQuaternion<T> &qreal, util::TQuaternion<T> &qdual) const {
		qreal = qr;
		qdual = qd;
	}
	const util::TQuaternion<T>& RotationQuaternion() const { return qr; }

	TTransform3<T> operator*(const TTransform3<T> &x) const {
		// q1*q2 = (qr1 + qd1*e)*(qr2 + qd2*e)
		//       = qr1*qr2 + (qd1*qr2 + qr1*qd2)*e
		//
		// q * <p> = (w + <v>) * (r + <p>)
		//         = w*r - v.p + w*<p> + r*<v> + vxp
		return TTransform3<T>(qr * x.qr, qd * x.qr + qr * x.qd);
	}

	// Return the inverse of this transform
	TTransform3<T> operator~() const {
		return TTransform3<T>(qr.Conjugate(), qd.Conjugate());
	}

	TPoint3<T> operator()(const TPoint3 <T>&p) const {
		// If P is the point inserted into a quaternion, then
		//   P' = q * P * q.Conjugate
		// where P = 1 + <p>*e. Thus,
		//   P' = (qr + qd*e) * (1 + <p>*e) * (conj(qr) - conj(qd)*e)
		//      = (qr + (qd + qr*<p>)*e) * (conj(qr) - conj(qd)*e)
		//      = 1 + [(qd + qr*<p>)*conj(qr) - qr*conj(qd)] * e
		//      = 1 + [qd*conj(qr) + qr*<p>*conj(qr) - qr*conj(qd)] * e
		//      = 1 + [qr*<p>*conj(qr) - 2*qr*conj(qd)] * e
		T xr = qr.v[0];
		T yr = qr.v[1];
		T zr = qr.v[2];
		T wr = qr.w;
		T xd = qd.v[0];
		T yd = qd.v[1];
		T zd = qd.v[2];
		T wd = qd.w;
		T x = p[0];
		T y = p[1];
		T z = p[2];

		T wxd = wr*x + yr*z - y*zr;
		T wyd = wr*y - xr*z + x*zr;
		T wzd = wr*z + xr*y - x*yr;
		T d = x*xr + y*yr + z*zr;
		return TPoint3<T>(
			2*(wr*xd - wd*xr + yr*zd - zr*yd) + yr*wzd - zr*wyd + wr*wxd + xr*d,
			2*(wr*yd - wd*yr + zr*xd - xr*zd) + zr*wxd - xr*wzd + wr*wyd + yr*d,
			2*(wr*zd - wd*zr + xr*yd - yr*xd) + xr*wyd - yr*wxd + wr*wzd + zr*d
		);
	}
	TVector3<T> operator()(const TVector3<T> &v) const {
		// If V is the point inserted into a quaternion, then
		// V' = q * V * q.Conjugate
		// where V = 1 + <v>*e. Thus,
		//   V' = (qr + qd*e) * (<v>*e) * (conj(qr) - conj(qd)*e)
		//      = (qr*<v>*e) * (conj(qr) - conj(qd)*e)
		//      = qr*<v>*conj(qr)*e
		T xr = qr.v[0];
		T yr = qr.v[1];
		T zr = qr.v[2];
		T wr = qr.w;
		T x = v[0];
		T y = v[1];
		T z = v[2];

		T wxd = wr*x + yr*z - y*zr;
		T wyd = wr*y - xr*z + x*zr;
		T wzd = wr*z + xr*y - x*yr;
		T d = x*xr + y*yr + z*zr;
		return TVector3<T>(
			wr*wxd + yr*wzd - zr*wyd + xr*d,
			wr*wyd + zr*wxd - xr*wzd + yr*d,
			wr*wzd + xr*wyd - yr*wxd + zr*d
		);
	}
	TRay3<T> operator()(const TRay3<T> &r) const {
		return TRay3<T>((*this)(r.origin()), (*this)(r.dir()));
	}
	TAABB3<T> operator()(const TAABB3<T> &b) const {
		TAABB3<T> bound;
		for(unsigned i = 0; i < 8; ++i){
			bound += (*this)(b.Vertex(i));
		}
		return bound;
	}

	// Homogeneous coordinate transforms
	TPoint4<T> operator()(const TPoint4<T> &p) const {
		T m[12];
		this->AsMatrix(m);
		return TPoint4<T>(
			m[0] * p[0] + m[3] * p[1] + m[6] * p[2] + m[ 9] * p[3],
			m[1] * p[0] + m[4] * p[1] + m[7] * p[2] + m[10] * p[3],
			m[2] * p[0] + m[5] * p[1] + m[8] * p[2] + m[11] * p[3],
			p[3]
		);
	}
	TVector4<T> operator()(const TVector4<T> &v) const {
		T m[12];
		this->AsMatrix(m);
		return TVector4<T>(
			m[0] * v[0] + m[3] * v[1] + m[6] * v[2] + m[ 9] * v[3],
			m[1] * v[0] + m[4] * v[1] + m[7] * v[2] + m[10] * v[3],
			m[2] * v[0] + m[5] * v[1] + m[8] * v[2] + m[11] * v[3],
			v[3]
		);
	}
};

template <typename T>
inline bool operator==(const TTransform3<T> &a, const TTransform3<T> &b) {
	return a.Equals(b);
}

template <typename T>
inline std::ostream& operator<<(std::ostream &s, const TTransform3<T> &x) {
	T m[12];
	x.AsMatrix(m);
	s << '['
		<< '[' << m[0] << ',' << m[3] << ',' << m[6] << ',' << m[ 9] << "],"
		<< '[' << m[1] << ',' << m[4] << ',' << m[7] << ',' << m[10] << "],"
		<< '[' << m[2] << ',' << m[5] << ',' << m[8] << ',' << m[11] << "]]";
	return s;
}

typedef TTransform2<coord_type> Transform2;
typedef TTransform3<coord_type> Transform3;

} // namespace affine
} // namespace geomod

#endif  // GEOMOD_AFFINE_VECTOR_H_
