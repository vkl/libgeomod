#ifndef GEOMOD_PLANAR_LATTICE_H_
#define GEOMOD_PLANAR_LATTICE_H_

#include <geomod/types.h>
#include <geomod/affine/vector.h>
#include <geomod/affine/point.h>
#include <vector>

namespace geomod{
namespace planar{

class Lattice{
	coord_type L[4];
public:
	typedef affine::Point2  point_type;
	typedef affine::Vector2 vector_type;
	typedef std::pair<index_type, index_type> index_pair;

	Lattice(const coord_type &period);
	Lattice(const vector_type &u, const vector_type &v = vector_type::Zero());

	affine::Vector2 u() const{ return affine::Vector2(L[0], L[1]); }
	affine::Vector2 v() const{ return affine::Vector2(L[2], L[3]); }

	unsigned Dimensionality() const; // 1 or 2

	coord_type Volume() const;

	Lattice Reciprocal() const;

	point_type Reduce(const point_type &p) const; // reduce to WS cell
	
	vector_type operator()(index_type iu, index_type iv = 0) const;

	void SelectShortest(size_t n, std::vector<index_pair> &indices) const;
};

} // namespace planar
} // namespace geomod

#endif // GEOMOD_PLANAR_LATTICE_H_
