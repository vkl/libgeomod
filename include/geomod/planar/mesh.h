#ifndef GEOMOD_PLANAR_MESH_H_
#define GEOMOD_PLANAR_MESH_H_

#include <geomod/types.h>
#include <geomod/affine/point.h>
#include <geomod/affine/aabb.h>
#include <geomod/planar/polygon.h>
#include <vector>

namespace geomod{
namespace planar{

// This class represents a planar mesh in 2D with a single connected component.

class Mesh{
public:
	typedef index_type vertex_index;
protected:
	typedef index_type halfedge_index;
	typedef index_type face_index;
	typedef index_type edge_index;
	struct Halfedge{
		vertex_index   from;
		halfedge_index next;
		halfedge_index flip;
		face_index     face;
		Halfedge(index_type from_, index_type next_, index_type flip_, index_type face_):
			from(from_),
			next(next_),
			flip(flip_),
			face(face_)
		{
		}
	};
public:
	typedef std::vector<affine::Point2> vertex_vector;
protected:
	vertex_vector v;
	std::vector<Halfedge> half;
	std::vector<halfedge_index> edge;
	std::vector<halfedge_index> face;

	// TODO: adjacency info: https://github.com/BrunoLevy/geogram/wiki/Mesh#why-dont-you-use-halfedges-
public:
	Mesh(){}
	virtual ~Mesh(){}

	index_type NumVertices() const;
	index_type NumEdges() const;
	index_type NumFaces() const;

	const affine::Point2& GetVertex(vertex_index vi) const;
	void GetFace(face_index f, vertex_index &v0, vertex_index &v1, vertex_index &v2) const;
	void GetEdge(edge_index e, vertex_index &v0, vertex_index &v1) const;
	vertex_vector GetBoundary() const;

	bool Convex() const;

	affine::AABB2 Bounds() const;

	void InsertVertex(const affine::Point2 &v);
	void DelaunayRefine(index_type max_new_vertices = 0);

	// Computes the Delaunay triangulation of the given set of points.
	static Mesh* Triangulate(const std::vector<affine::Point2> &points);

	static Mesh* Triangulate(const Polygon &poly);

protected:
	face_index Locate(const affine::Point2 &p, face_index hint = 0) const;
};

} // namespace planar
} // namespace geomod

#endif // GEOMOD_PLANAR_MESH_H_
