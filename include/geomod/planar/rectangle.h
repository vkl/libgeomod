#ifndef GEOMOD_PLANAR_RECTANGLE_H_
#define GEOMOD_PLANAR_RECTANGLE_H_

#include <geomod/types.h>
#include <geomod/planar/shape.h>

namespace geomod{
namespace planar{

class Rectangle:
	public Shape,
	public Shape::FourierTransformable
{
	class EdgeParameterization : public onedim::Parameterization{
		affine::Point2 a;
		affine::Vector2 v;
	public:
		EdgeParameterization(const affine::Point2 &a, const affine::Point2 &b): a(a), v(b-a){}
		affine::Point2 operator()(const coord_type &u) const;
		coord_type operator()(const affine::Point2 &p)  const;
		bool RayIntersection(const affine::Ray2 &ray, std::vector<affine::Ray2::Intersection> &x) const;
		void Neighborhood(const coord_type &u, onedim::Parameterization::DifferentialGeometry &dg) const;
		onedim::Interval Bounds() const{ return onedim::Interval(0, 1); }
	};
	coord_type h[2];
	std::vector<EdgeParameterization> param;
public:
	Rectangle(const coord_type &width, const coord_type &height){
		h[0] = 0.5*width;
		h[1] = 0.5*height;
		param.reserve(4);
		param.push_back(EdgeParameterization(affine::Point2(-h[0], -h[0]), affine::Point2( h[0], -h[0])));
		param.push_back(EdgeParameterization(affine::Point2( h[0], -h[0]), affine::Point2( h[0],  h[0])));
		param.push_back(EdgeParameterization(affine::Point2( h[0],  h[0]), affine::Point2(-h[0],  h[0])));
		param.push_back(EdgeParameterization(affine::Point2(-h[0],  h[0]), affine::Point2(-h[0], -h[0])));
	}
	~Rectangle(){}

	coord_type size(index_type iaxis) const{ return 2*h[iaxis]; }
	affine::Point2 vertex(index_type i) const;

	Rectangle* Clone() const{ return new Rectangle(2*h[0], 2*h[1]); }

	bool Contains(const affine::Point2 &p) const;
	
	bool Convex() const{ return true; }

	size_t NumCharts() const{ return 4; }
	const onedim::Parameterization* GetChartParameterization(index_type ichart) const{ return &param[ichart]; }
	onedim::Parameterizable::chart_polyline GetChartPolyline(index_type ichart, const coord_type &tol = 1e-3) const;
	
	affine::Point2 Extremum(const affine::Vector2 &dir) const;
	affine::Point2 Nearest(const affine::Point2 &p) const;
	affine::AABB2 Bounds() const;

	coord_type Perimeter() const;
	coord_type Area() const;
	affine::Point2 Centroid() const;

	void GetSamples(size_t n, Shape::SamplingSpec spec, std::vector<point_type> &points) const;

	const Shape::FourierTransformable* GetFourierTransformable() const{ return this; }
	complex_type FourierTransform(const affine::Vector2 &f) const;
	
	Polygon* AsPolygon(const coord_type &tol = 1e-3) const;
	Rectangle* Offset(const coord_type &dist, const coord_type &tol = 0) const;
};

} // namespace planar
} // namespace geomod

#endif // GEOMOD_PLANAR_RECTANGLE_H_
