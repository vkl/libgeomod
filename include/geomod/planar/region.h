#ifndef GEOMOD_PLANAR_REGION_H_
#define GEOMOD_PLANAR_REGION_H_

#include <vector>
#include <complex>
#include <geomod/affine/transform.h>
#include <geomod/planar/shape.h>

namespace geomod{
namespace planar{

// This represents a transformed shape.

class Region:
	public onedim::Parameterizable,
	public Shape::FourierTransformable
{
	Shape *shape;
	affine::Transform2 transform;
public:
	typedef affine::Point2  point_type;
	typedef affine::Vector2 vector_type;
	typedef affine::AABB2   bound_type;

	// Integrability
	coord_type Perimeter() const;
	coord_type Area() const;
	point_type Centroid() const;

	// Extremizability
	point_type Extremum(const vector_type &dir) const;
	point_type Nearest(const point_type &p) const;
	bound_type Bounds() const;
	
	// Parameterizability
	size_t NumCharts() const;
	const onedim::Parameterization* GetChartParameterization(index_type ichart) const;
	
	void GetSamples(size_t n, Shape::SamplingSpec spec, std::vector<point_type> &points) const;

	Region(Shape *shape, const affine::Transform2 &xform = affine::Transform2()):shape(shape), transform(xform){}
	~Region(){ delete shape; }

	Region* Clone() const;

	bool Contains(const point_type &p) const;
	bool Convex() const;
	
	const Shape::FourierTransformable* GetFourierTransformable() const;
	complex_type FourierTransform(const vector_type &f) const;

	coord_type SignedDistance(const point_type &p, vector_type *grad = NULL) const;
	
	Polygon* AsPolygon(const coord_type &tol = 1e-3) const;
	Region* Offset(const coord_type &dist, const coord_type &tol = 0) const;
	Region* Transformed(const affine::Transform2 &transform) const;
	void Transform(const affine::Transform2 &transform);
};

} // namespace planar
} // namespace geomod

#endif // GEOMOD_PLANAR_REGION_H_
