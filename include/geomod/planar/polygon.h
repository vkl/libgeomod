#ifndef GEOMOD_PLANAR_POLYGON_H_
#define GEOMOD_PLANAR_POLYGON_H_

#include <geomod/types.h>
#include <geomod/affine/ray.h>
#include <geomod/affine/transform.h>
#include <geomod/planar/shape.h>

namespace geomod{
namespace planar{

class Polygon:
	public Shape,
	public Shape::FourierTransformable
{
public:
	typedef affine::Point2 point_type;
	typedef std::vector<point_type> vertex_vector;
	typedef std::vector<index_type> index_vector;
private:
	class EdgeParameterization : public onedim::Parameterization{
		affine::Point2 a;
		affine::Vector2 v;
	public:
		EdgeParameterization(const affine::Point2 &a, const affine::Point2 &b): a(a), v(b-a){}
		affine::Point2 operator()(const coord_type &u) const;
		coord_type operator()(const affine::Point2 &p)  const;
		bool RayIntersection(const affine::Ray2 &ray, std::vector<affine::Ray2::Intersection> &x) const;
		void Neighborhood(const coord_type &u, onedim::Parameterization::DifferentialGeometry &dg) const;
		onedim::Interval Bounds() const{ return onedim::Interval(0, 1); }
	};
	vertex_vector v;
	std::vector<EdgeParameterization> param;
public:
	Polygon(const vertex_vector &vertices): v(vertices) {
		const size_t n = vertices.size();
		param.reserve(n);
		for(size_t i = 0; i < n; ++i){
			param.push_back(EdgeParameterization(vertex(i), vertex(i+1)));
		}
	}
	~Polygon(){}

	const point_type& vertex(index_type i) const;
	point_type& mutable_vertex(index_type i);
	const point_type& operator[](index_type i) const{ return vertex(i); }
	size_t size() const{ return v.size(); }
	const vertex_vector& GetVertices() const{ return v; }
	void Transform(const affine::Transform2 &transform);

	Polygon* Clone() const{ return new Polygon(v); }

	bool Contains(const point_type &p) const;
	
	bool Convex() const;

	size_t NumCharts() const{ return v.size(); }
	const onedim::Parameterization* GetChartParameterization(index_type ichart) const{ return &param[ichart]; }
	onedim::Parameterizable::chart_polyline GetChartPolyline(index_type ichart, const coord_type &tol = 1e-3) const;
	
	affine::Point2 Extremum(const affine::Vector2 &dir) const;
	affine::Point2 Nearest(const affine::Point2 &p) const;
	affine::AABB2 Bounds() const;

	// Returns the signed distance from the point p to the polygon,
	// and optionally, the gradient of the distance.
	// Points outside the polygon have positive distance.
	coord_type SignedDistance(const affine::Point2 &p, affine::Vector2 *grad = NULL) const;

	coord_type Perimeter() const;
	coord_type Area() const;
	affine::Point2 Centroid() const;

	const Shape::FourierTransformable* GetFourierTransformable() const{ return this; }
	complex_type FourierTransform(const affine::Vector2 &f) const;
	
	void GetSamples(size_t n, Shape::SamplingSpec spec, std::vector<point_type> &points) const;

	Polygon* AsPolygon(const coord_type &tol = 1e-3) const{ return this->Clone(); }
	Polygon* Offset(const coord_type &dist, const coord_type &tol = 0) const;

	enum ClipOperation{
		POLYGON_CLIP_INTERSECTION,
		POLYGON_CLIP_UNION,
		POLYGON_CLIP_DIFFERENCE,
		POLYGON_CLIP_XOR
	};
	int Clip(const Polygon &b, ClipOperation op, std::vector<Polygon> &results) const;
	Polygon Clip(const affine::Ray2 &r) const; // intersect this polygon with the halfplane to the left of the ray

	bool Triangulate(index_vector &triangle_indices) const;

	static Polygon* ConvexHull(const vertex_vector &points);
};

} // namespace planar
} // namespace geomod

#endif // GEOMOD_PLANAR_POLYGON_H_
