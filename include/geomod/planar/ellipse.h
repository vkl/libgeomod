#ifndef GEOMOD_PLANAR_ELLIPSE_H_
#define GEOMOD_PLANAR_ELLIPSE_H_

#include <geomod/types.h>
#include <geomod/planar/shape.h>

namespace geomod{
namespace planar{

class Ellipse:
	public Shape,
	public Shape::FourierTransformable
{
	class Parameterization : public onedim::Parameterization{
		const Ellipse *e;
	public:
		Parameterization(const Ellipse *parent);
		affine::Point2 operator()(const coord_type &u) const;
		coord_type operator()(const affine::Point2 &p)  const;
		bool RayIntersection(const affine::Ray2 &ray, std::vector<affine::Ray2::Intersection> &x) const;
		void Neighborhood(const coord_type &u, onedim::Parameterization::DifferentialGeometry &dg) const;
		onedim::Interval Bounds() const{ return onedim::Interval(0, 1); }
	};

	coord_type r[2];
	Parameterization param;
public:
	Ellipse(const coord_type &semi_major_x, const coord_type &semi_major_y):param(this){
		r[0] = semi_major_x;
		r[1] = semi_major_y;
	}
	~Ellipse(){}

	const coord_type& semimajor_axis(index_type iaxis) const{ return r[iaxis]; }

	Ellipse* Clone() const{ return new Ellipse(r[0], r[1]); }

	bool Contains(const affine::Point2 &p) const;
	
	bool Convex() const{ return true; }

	size_t NumCharts() const{ return 1; }
	const onedim::Parameterization* GetChartParameterization(index_type ichart) const{ return &param; }
	onedim::Parameterizable::chart_polyline GetChartPolyline(index_type ichart, const coord_type &tol = 1e-3) const;
	
	affine::Point2 Extremum(const affine::Vector2 &dir) const;
	affine::Point2 Nearest(const affine::Point2 &p) const;
	affine::AABB2 Bounds() const;

	coord_type Perimeter() const;
	coord_type Area() const;
	affine::Point2 Centroid() const;

	void GetSamples(size_t n, Shape::SamplingSpec spec, std::vector<point_type> &points) const;

	const Shape::FourierTransformable* GetFourierTransformable() const{ return this; }
	complex_type FourierTransform(const affine::Vector2 &f) const;
	
	Polygon* AsPolygon(const coord_type &tol = 1e-3) const;
	Shape* Offset(const coord_type &dist, const coord_type &tol = 0) const;
};

} // namespace planar
} // namespace geomod

#endif // GEOMOD_PLANAR_ELLIPSE_H_
