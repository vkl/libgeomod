#ifndef GEOMOD_TYPES_H_
#define GEOMOD_TYPES_H_

#ifndef NOMINMAX
#define NOMINMAX
#endif

#define _USE_MATH_DEFINES
#include <cmath>
#include <complex>
#include <cstdint>
#include <cstddef>
#include <limits>

namespace geomod {

typedef double coord_type;   // spatial coordinates
typedef std::complex<coord_type> complex_type; // complex fields
typedef int32_t index_type;

// Template to determine if a numeric type is complex or not.
template<typename T> struct is_complex_type                  : public std::false_type {};
template<typename T> struct is_complex_type<std::complex<T>> : public std::true_type  {};

// Template to determine the underlying floating point type of std::complex<T>
template <typename T> struct underlying_float_type                  { using type = T; };
template <typename T> struct underlying_float_type<std::complex<T>> { using type = T; };

// Template to upgrade to the wider of A and B, and to complex if either is complex
template <typename A, typename B>
struct upgrade_if_complex {
private:
	using FloatA = typename underlying_float_type<A>::type;
	using FloatB = typename underlying_float_type<B>::type;
	using FloatC = typename std::conditional<(sizeof(FloatA) >= sizeof(FloatB)), FloatA, FloatB>::type;
public:
	using type = typename std::conditional<
		is_complex_type<A>::value || is_complex_type<B>::value,
		std::complex<FloatC>,
		FloatC
	>::type;
};

// Template overloads to cast complex numbers of differing underlying types.
template <typename A, typename B> inline void complex_cast(const              A  &a,              B  &b){ b =                 B(a);  }
template <typename A, typename B> inline void complex_cast(const              A  &a, std::complex<B> &b){ b = std::complex<B>(B(a)); }
template <typename A, typename B> inline void complex_cast(const std::complex<A> &a, std::complex<B> &b){ b = std::complex<B>(B(a.real()), B(a.imag())); }

} // namespace geomod

#endif  // GEOMOD_TYPES_H_
