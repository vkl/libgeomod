#include <geomod/affine/vector.h>
#include <geomod/affine/point.h>
#include <geomod/affine/ray.h>
#include <geomod/affine/transform.h>
#include <geomod/planar/circle.h>
#include <geomod/surface/sag.h>
#include <map>
#include <cstdio>

template <typename K, typename V>
V GetWithDefault(const std::map <K,V> &m, const K &key, const V &defval){
	typename std::map<K,V>::const_iterator it = m.find(key);
	if(m.end() == it){
		return defval;
	}else{
		return it->second;
	}
}

using geomod::coord_type;
using geomod::affine::Point2;
using geomod::affine::Vector2;
using geomod::affine::Point3;
using geomod::affine::Vector3;
using geomod::affine::Ray3;

class SphericalSag : public geomod::surface::Sag::Function{
	coord_type curvature;
	coord_type clear_diam;
	coord_type mech_diam;
public:
	SphericalSag(
		const coord_type &curvature,
		const coord_type &clear_diam,
		const coord_type &mech_diam
	):
		curvature(curvature),
		clear_diam(clear_diam),
		mech_diam(mech_diam)
	{}

	coord_type Evaluate(const Point2 &rxy, Vector2 *gradient = NULL, coord_type *II = NULL) const{
		const coord_type kappa = 0.0;
		const coord_type r = std::hypot(rxy[0], rxy[1]);
		if(0 == r){
			if(NULL != gradient){
				*gradient = Vector2::Zero();
			}
			if(NULL != II){
				II[0] = curvature;
				II[1] = 0;
				II[2] = curvature;
			}
			return 0;
		}
		coord_type reval = r;
		if(r >= clear_diam){
			reval = clear_diam;
		}
		static const coord_type one(1);
		const coord_type cr = curvature*reval;
		const coord_type cr2 = cr*reval;
		const coord_type disc = one - (one + kappa)*cr*cr;
		const coord_type rt = sqrt(disc);
		coord_type z = cr2 / (one + rt);

		if(NULL != gradient){
			if(r < clear_diam){
				*gradient = (cr / rt) * Vector2(rxy[0] / r, rxy[1] / r);
			}else{
				*gradient = Vector2::Zero();
			}
		}
		return z;
	}

	geomod::onedim::Interval Bounds(const geomod::affine::AABB2 &bounds) const{
		coord_type z = Evaluate(Point2(clear_diam, 0), NULL, NULL);
		if(z >= 0){
			return geomod::onedim::Interval(0, z);
		}else{
			return geomod::onedim::Interval(z, 0);
		}
	}

	SphericalSag* Clone() const{
		return new SphericalSag(curvature, clear_diam, mech_diam);
	}

	coord_type BaseCurvature() const{
		return curvature;
	}
};

struct LightRay {
	Ray3 r;
	int field;
	coord_type t;
	double n;
	LightRay(
		const Point3 &p,
		const Vector3 &dir,
		int field = 0,
		const coord_type &t = 0,
		const double &n = 1
	):
		r(p, Vector3::Normalize(dir)), field(field), t(t), n(n)
	{
	}
};

struct LensSurface {
	coord_type curvature;
	coord_type thickness;
	std::string glass;
	coord_type clear_diam;
	coord_type mech_diam;
	LensSurface(
		const coord_type curvature,
		const coord_type thickness,
		const std::string glass,
		const coord_type clear_diam,
		const coord_type mech_diam
	):
		curvature(curvature),
		thickness(thickness),
		glass(glass),
		clear_diam(clear_diam),
		mech_diam(mech_diam)
	{}
};

struct LensSystem {
	std::vector<LensSurface> surfaces;
	std::map<std::string, double> catalog;

	void trace(
		const LightRay &initial_ray,
		std::vector<LightRay> &output_rays
	) const;
	void draw() const;
};

void LensSystem::trace(
	const LightRay &initial_ray,
	std::vector<LightRay> &output_rays
) const{
	const size_t nsurf = surfaces.size();
	LightRay ray(initial_ray);
	//output_rays.push_back(initial_ray);
	coord_type tt = 0; // running total thickness;
	for(size_t isurf = 1; isurf < nsurf; ++isurf){
		//printf("   Surface %d\n", (int)isurf);
		//printf("    ray1 org(%f, %f, %f) dir(%f, %f, %f)\n", ray.r.origin()[0], ray.r.origin()[1], ray.r.origin()[2], ray.r.dir()[0], ray.r.dir()[1], ray.r.dir()[2]);
		const LensSurface &surface = surfaces[isurf];
		SphericalSag func(surface.curvature, surface.clear_diam, surface.mech_diam);
		geomod::planar::Circle reg(0.5*surface.clear_diam);
		geomod::surface::Sag sag(&func, &reg);
		geomod::affine::Ray3::Intersection x;
		if(!sag.RayIntersection(ray.r, x) || !(x.t == x.t)){
			break;
		}
		//printf("> Intersection:\n");
		//printf("    t  = %f\n", x.t);
		//printf("    uv = %f, %f\n", x.uv[0], x.uv[1]);
		//printf("    p  = %f, %f\n", x.p[0], x.p[1]);
		geomod::surface::Parameterization::DifferentialGeometry dg;
		sag.Neighborhood(x.uv, dg);
		const Vector3 normal = Vector3::Normalize(Vector3::Cross(dg.dpdu, dg.dpdv));
		//printf("    dpdu  = %f, %f, %f\n", dg.dpdu[0], dg.dpdu[1], dg.dpdu[2]);
		//printf("    dpdv  = %f, %f, %f\n", dg.dpdv[0], dg.dpdv[1], dg.dpdv[2]);
		//printf("    normal  = %f, %f, %f\n", normal[0], normal[1], normal[2]);
		const double n1 = ray.n;
		const double n2 = GetWithDefault(catalog, surface.glass, 1.0);
		//printf("    n1, n2 = %f, %f\n", n1, n2);
		
		const double nn = n1/n2;
		const double cos1 = Vector3::Dot(normal, ray.r.dir());
		const double sin2 = nn*std::sqrt(1 - cos1*cos1);
		//printf("    cos1 = %f, sin2 = %f\n", cos1, sin2);
		if(sin2 >= 1){ break; }
		const double cos2 = std::sqrt(1 - sin2*sin2);
		//printf("    cos2 = %f\n", cos2);
		const Vector3 dir2 = Vector3::Normalize(nn*ray.r.dir() - (nn*cos1 - cos2)*normal);
		tt += surfaces[isurf-1].thickness;
		ray.r = Ray3(ray.r.origin() + tt*Vector3::Z(), ray.r.dir());
		ray.t = x.t;
		//printf("pushing ray with origin (%f, %f, %f), t = %f\n", ray.r.origin()[0], ray.r.origin()[1], ray.r.origin()[2], ray.t);
		output_rays.push_back(ray);
		ray = LightRay(Point3(x.p[0], x.p[1], x.p[2]-surface.thickness), dir2, ray.field);
		ray.n = n2;
		//printf("    ray2 org(%f, %f, %f) dir(%f, %f, %f)\n", ray.r.origin()[0], ray.r.origin()[1], ray.r.origin()[2], ray.r.dir()[0], ray.r.dir()[1], ray.r.dir()[2]);
	}
}

void LensSystem::draw() const{
	printf("%%!PS-Adobe-3.0 EPSF-3.0\n");
	printf("%%%%Creator: raytracer\n");
	printf("%%%%BoundingBox: 0 0 %f %f\n", 792., 612.);
	printf("%%%%LanguageLevel: 2\n");
	printf("%%%%Pages: 1\n");
	printf("%%%%DocumentData: Clean7Bit\n");
	printf("1 %f dup dup scale div setlinewidth\n", 0.75 * 72.0/25.4); // 72 ppi
	printf("10 140 translate\n");
	printf("gsave\n");
	coord_type last_x = 0;
	bool last_glass = false;
	for(std::vector<LensSurface>::const_iterator s = surfaces.begin(); surfaces.end() != s; ++s){
		const coord_type c = s->curvature;
		const coord_type a = 0.5*s->clear_diam;
		const coord_type m = 0.5*s->mech_diam;
		if(0 == c){
			printf("0 %f dup neg 0 exch moveto lineto stroke\n", a);
			last_x = -s->thickness;
		}else{
			const coord_type r = 1/c;
			const coord_type x = r*(1 - std::sqrt(1-(a/r)*(a/r)));
			const coord_type angle = std::abs(std::asin(a/r)) / M_PI * 180;
			if(c > 0){
				printf("%f dup 0 exch %f dup neg 180 add exch 180 add arc stroke\n", r, angle);
			}else{
				printf("%f dup 0 exch neg %f dup neg exch arc stroke\n", r, angle);
			}
			if(a < m){
				printf("%f %f moveto %f %f lineto stroke\n", x, a, x, m);
				printf("%f %f moveto %f %f lineto stroke\n", x, -a, x, -m);
			}
			if(last_glass){
				printf("%f %f moveto %f %f lineto stroke\n", last_x, m, x, m);
				printf("%f %f moveto %f %f lineto stroke\n", last_x, -m, x, -m);
			}
			last_x = x - s->thickness;
		}
		printf("%f 0 translate\n", s->thickness);
		last_glass = !s->glass.empty();
	}
	printf("grestore\n");
}

void draw_rays(const std::vector<LightRay> &rays){
	const unsigned char colortable[][3] = {
		{ 0x17, 0xbe, 0xcf },
		{ 0xff, 0x7f, 0x0e },
		{ 0x2c, 0xa0, 0x2c },
		{ 0xd6, 0x27, 0x28 },
		{ 0x94, 0x67, 0xbd },
		{ 0x8c, 0x56, 0x4b },
		{ 0xe3, 0x77, 0xc2 },
		{ 0x7f, 0x7f, 0x7f },
		{ 0xbc, 0xbd, 0x22 },
		{ 0x1f, 0x77, 0xb4 }
	};
	for(std::vector<LightRay>::const_iterator ray = rays.begin(); rays.end() != ray; ++ray){
		int field = ray->field;
		int c = field % 10;
		printf("%f %f %f setrgbcolor\n",
			colortable[c][0] / 255.,
			colortable[c][1] / 255.,
			colortable[c][2] / 255.
		);
		printf("%f %f moveto %f %f lineto stroke %% t=%f\n",
			ray->r.origin()[2],
			ray->r.origin()[0],
			ray->r.origin()[2] + ray->t * ray->r.dir()[2],
			ray->r.origin()[0] + ray->t * ray->r.dir()[0],
			ray->t
		);
	}
	printf("showpage\n");
}

int main(int argc, char *argv[]){
	LensSystem sys;
	//                        curvature    thickness glass       clear_diam mech_diam
	sys.surfaces.emplace_back(1.0/ 150.0  , 50.0   , ""         , 289.7364, 289.7364);
	sys.surfaces.emplace_back(1.0/ 143.47 ,  7     , "E-BK7"    , 212.453 , 212.453 );
	sys.surfaces.emplace_back(1.0/  52.5  , 28     , ""         , 104.2364, 212.453 );
	sys.surfaces.emplace_back(1.0/  76.4  ,  3.8   , "E-BK7"    , 102     , 102     );
	sys.surfaces.emplace_back(1.0/  31.521, 21.8   , ""         ,  61     , 102     );
	sys.surfaces.emplace_back(1.0/ 150    ,  3     , "E-BK7"    ,  56     ,  56     );
	sys.surfaces.emplace_back(1.0/  17.1  , 16.5   , ""         , 32.24031,  56     );
	sys.surfaces.emplace_back(1.0/ -60    ,  7     , "E-SK16"   , 29      ,  29     );
	sys.surfaces.emplace_back(1.0/  22.625,  0.6   , ""         , 25.07798,  29     );
	sys.surfaces.emplace_back(1.0/  23.9  , 12.6   , "E-SF10"   , 29      ,  29     );
	sys.surfaces.emplace_back(1.0/  78.988, 22.7   , ""         , 23      ,  29     );
	sys.surfaces.emplace_back(0           ,  1.8   , "E-K3"     , 16.6    ,  16.6   );
	sys.surfaces.emplace_back(0           ,  5.1   , ""         , 16.6    ,  16.6   );
	sys.surfaces.emplace_back(1.0/ 278.333,  3     , "E-SF2"    , 16.6    ,  16.6   );
	sys.surfaces.emplace_back(1.0/-185.42 ,  0.1   , ""         , 16.6    ,  16.6   );
	sys.surfaces.emplace_back(1.0/  52.03 ,  7     , "534554"   , 16.6    ,  16.6   );
	sys.surfaces.emplace_back(1.0/ -28.5  ,  2     , "E-LASF016", 16.6    ,  16.6   );
	sys.surfaces.emplace_back(1.0/ -77    , 13     , ""         , 16.6    ,  16.6   );
	sys.surfaces.emplace_back(1.0/  45    ,  8     , "E-K3"     , 19      ,  19     );
	sys.surfaces.emplace_back(1.0/ -14.4  ,  0.6   , "796408"   , 19      ,  19     );
	sys.surfaces.emplace_back(1.0/ -34.5  ,  0.1   , ""         , 19      ,  19     );
	sys.surfaces.emplace_back(1.0/-110    ,  1     , "E-SF1"    , 19      ,  19     );
	sys.surfaces.emplace_back(1.0/  35    ,  3.5   , "E-BK7"    , 19      ,  19     );
	sys.surfaces.emplace_back(1.0/ -25.763, 37.6296, ""         , 19      ,  19     );
	sys.surfaces.emplace_back(0           ,  0     , ""         , 23.38712, 23.38712);

	sys.catalog["E-BK7"    ] = 1.51679988;
	sys.catalog["E-SK16"   ] = 1.62041101;
	sys.catalog["E-SF10"   ] = 1.72825003;
	sys.catalog["E-K3"     ] = 1.51822900;
	sys.catalog["E-SF2"    ] = 1.64768902;
	sys.catalog["534554"   ] = 1.53400191;
	sys.catalog["E-LASF016"] = 1.77249902;
	sys.catalog["796408"   ] = 1.79600382;
	sys.catalog["E-SF1"    ] = 1.71736202;

	
	//LightRay initial_ray(Point3(1, 0, -50), Vector3(0, 0, 1));
	std::vector<LightRay> output_rays;
	sys.trace(LightRay(Point3(1, 0, -50), Vector3(0, 0, 1), 0), output_rays);
	sys.trace(LightRay(Point3(-1, 0, -50), Vector3(0, 0, 1), 0), output_rays);
	sys.trace(LightRay(Point3(-19.5, 0, -50), Vector3(0.2, 0, 1), 1), output_rays);
	sys.trace(LightRay(Point3(-20.5, 0, -50), Vector3(0.2, 0, 1), 1), output_rays);
	sys.trace(LightRay(Point3(-40.5, 0, -50), Vector3(0.4, 0, 1), 2), output_rays);
	sys.trace(LightRay(Point3(-41.5, 0, -50), Vector3(0.4, 0, 1), 2), output_rays);
	sys.trace(LightRay(Point3(-77.5, 0, -50), Vector3(0.8, 0, 1), 3), output_rays);
	sys.trace(LightRay(Point3(-78.5, 0, -50), Vector3(0.8, 0, 1), 3), output_rays);
	sys.trace(LightRay(Point3(-125, 0, -20), Vector3(0.8, 0, 0.4), 4), output_rays);
	sys.trace(LightRay(Point3(-124, 0, -20), Vector3(0.8, 0, 0.4), 4), output_rays);
	sys.trace(LightRay(Point3(-124, 0, 37.5), Vector3(0.8, 0, -0.1), 5), output_rays);
	sys.trace(LightRay(Point3(-124, 0, 38), Vector3(0.8, 0, -0.1), 5), output_rays);
	sys.draw();
	draw_rays(output_rays);

	return 0;
}
