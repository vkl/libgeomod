#include <geomod/surface/plane.h>
#include <geomod/planar/region.h>
#include <geomod/planar/polygon.h>

using namespace geomod;
using namespace geomod::affine;

bool surface::Plane::RayIntersection(const affine::Ray3 &r, affine::Ray3::Intersection &x) const{
	// Solve: n.(r.p + t*r.d - p) == 0
	//        t = n.(p-r.p) / n.(r.d) 
	//const coord_type denom = affine::Vector3::Dot(r.dir(), affine::Vector3::Z());
	const coord_type denom = r.dir()[2];
	if(0 == denom){ return false; }
	//x.t = affine::Vector3::Dot(affine::Vector3::Z(), p - r.origin()) / denom;
	x.t = -r.origin()[2] / denom;
	x.p = r.origin() + x.t * r.dir();
	x.uv = affine::Point2(x.p[0], x.p[1]);
	return x.t > 0;
}

surface::Mesh* surface::Plane::AsMesh(const planar::Region *reg, const coord_type &tol) const{
	planar::Polygon *boundary = reg->AsPolygon();
	planar::Polygon::index_vector triangle_indices;
	boundary->Triangulate(triangle_indices);
	surface::Mesh::vertex_vector vertices;
	vertices.reserve(boundary->size());
	for(size_t i = 0; i < boundary->size(); ++i){
		vertices.push_back(surface::Mesh::vertex_type(
			(*boundary)[i][0],
			(*boundary)[i][1],
			0
		));
	}
	surface::Mesh *mesh = new surface::Mesh(vertices, triangle_indices);
	delete boundary;
	return mesh;
}

Transform3 surface::Plane::ToTransform(const Point3 &p, const Vector3 &n){
	return Transform3::Translate(p - Point3::Origin()) * Transform3::Rotate(Vector3::Z(), n);
}

bool surface::Plane::PlaneIntersection(const Transform3 &t, Ray2 &x) const{
	// Step 1: find a point on the intersection.
	//   We will use the two plane normals as a basis for this operation.
	//   Let z = Point3::Z(), p = t(Point3::Origin()), n = t(z).
	//   We then seek p0 = p + alpha*n + beta*z, such that
	//     z.p0 = 0 and n.(alpha*n + beta*z) = 0.
	//   These reduce to
	//     [  1  n.z ] [ alpha ] = [  0  ]
	//     [ n.z  1  ] [ beta  ] = [-z.p ]
	const Vector3 n(t(Vector3::Z()));
	const coord_type det = 1 - n[2]*n[2];
	if(0 == det){ return false; }
	const Point3 p(t(Point3::Origin()));
	const coord_type beta = -p[2]/det;
	const coord_type alpha = -n[2]*beta;
	const Point3 p0(p + alpha*n + beta*Vector3::Z());
	// At this point, we should have p0[2] == 0.

	// Step 2: find ray direction
	//   At this point, we simply need to solve
	//     n.d = 0
	//   for the ray direction d.
	x = Ray2(Point2(p0[0], p0[1]), Vector2(-n[1], n[0]));
	return true;
}
