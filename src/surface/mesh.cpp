#include <geomod/surface/mesh.h>
#include <cstdio>

using namespace geomod;
using namespace geomod::affine;

const surface::Mesh::vertex_type& surface::Mesh::GetVertex(index_type vi) const{
	return vert[vi];
}
void surface::Mesh::GetFace(index_type f, index_type &v0, index_type &v1, index_type &v2) const{
	v0 = tri[3*f+0];
	v1 = tri[3*f+1];
	v2 = tri[3*f+2];
}
	
affine::AABB3 surface::Mesh::Bounds() const{
	affine::AABB3 bounds;
	for(size_t i = 0; i < vert.size(); ++i){
		bounds += vert[i];
	}
	return bounds;
}

static int SaveOFF(const surface::Mesh *mesh, const char *filename){
	FILE *fp = fopen(filename, "wt");
	if(NULL == fp){ return -2; }

	fprintf(fp, "OFF\n%d %d 0\n", (int)mesh->NumVertices(), (int)mesh->NumFaces());
	for(size_t i = 0; i < mesh->NumVertices(); ++i){
		const Point3 &v = mesh->GetVertex(i);
		fprintf(fp, "%g %g %g\n", v[0], v[1], v[2]);
	}
	for(size_t i = 0; i < mesh->NumFaces(); ++i){
		index_type v0, v1, v2;
		mesh->GetFace(i, v0, v1, v2);
		fprintf(fp, "3 %d %d %d\n", (int)v0, (int)v1, (int)v2);
	}

	fclose(fp);
	return 0;
}
int surface::Mesh::Serialize(surface::Mesh::Format format, const char *filename) const{
	switch(format){
	case MESH_FORMAT_OFF: return SaveOFF(this, filename);
	default:              return -1;
	}
}
