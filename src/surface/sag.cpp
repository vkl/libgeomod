#include <geomod/surface/sag.h>
#include <geomod/planar/region.h>
#include <geomod/planar/polygon.h>
#include <geomod/planar/mesh.h>
#include <geomod/util/polynomial.h>

using namespace geomod;
using namespace geomod::affine;

surface::Sag::Sag(const Function *func, const planar::Region *region):
	func(func->Clone()),
	reg(region->Clone())
{
}
surface::Sag::~Sag(){
	delete reg;
	delete func;
}

bool surface::Sag::RayIntersection(const affine::Ray3 &r, affine::Ray3::Intersection &x) const{
	// Let rxy = oxy + t dxy
	// We want oz + t dz - z(r) == 0
	// The derivative is:
	//    dz - dxy.grad_r z(r)
	// The Newton step is therefore
	//   t -= [oz + t dz - z(r)] / [dz - dxy.grad_r z(r)]

	// Form initial guess
	coord_type t = 0;
	affine::Point2 xy;
	coord_type base_curvature = 0;
	if(0 != (base_curvature = func->BaseCurvature())){
		// If a nonzero base curvature was specified, form the initial guess
		// using a spherical approximation.
		coord_type radius = coord_type(1) / base_curvature;
		const Vector3 p(r.origin() - Point3::Origin() - radius * Vector3::Z());
		coord_type rt[2];
		unsigned nroots = polynomial::quadratic_equation(
			r.dir().NormSq(),
			Vector3::Dot(r.dir(), p),
			p.NormSq() - radius*radius,
			rt
		);
		if(0 == nroots){ // Fall back to planar approximation
			t = -r.origin()[2]/r.dir()[2];
		}else if(1 == nroots){
			t = rt[0];
		}else if(nroots > 1 && rt[0] > rt[1]){
			t = rt[1];
		}
	}else{
		// Default planar approximation
		t = -r.origin()[2]/r.dir()[2];
	}
	xy[0] = r.origin()[0] + t * r.dir()[0];
	xy[1] = r.origin()[1] + t * r.dir()[1];

	// If initial guess is not in the specified region then quit
	if(NULL != reg && !reg->Contains(xy)){
		return false;
	}

	// Newton-Raphson iteration
	coord_type dt;
	unsigned niter = 0;
	affine::Vector2 g;
	coord_type z, rr;
	static const coord_type tol = std::sqrt(std::numeric_limits<coord_type>::epsilon());
	static const unsigned maxiter = 64;

	//printf("surface::Sag::RayIntersection ray origin = (%f, %f, %f)\n", r.origin()[0], r.origin()[1], r.origin()[2]);
	//printf("surface::Sag::RayIntersection ray dir = (%f, %f, %f)\n", r.dir()[0], r.dir()[1], r.dir()[2]);
	//printf("surface::Sag::RayIntersection Initial t = %f\n", t);

	do{
		// Loop precondition:
		//   assert(NULL == reg || reg->Contains(xy));
		z = func->Evaluate(xy, &g);
		// It is assumed that z is not NaN; we are allowed to evaluate
		// anywhere within the specified region.

		//printf("surface::Sag::RayIntersection iter = %u\n", niter);
		//printf("surface::Sag::RayIntersection xy = %f, %f; z = %f; g = %f, %f\n", xy[0], xy[1], z, g[0], g[1]);
		
		// Compute Newton correction
		const coord_type val = r.origin()[2] + t * r.dir()[2] - z;
		const coord_type deriv = r.dir()[2] - r.dir()[0]*g[0] - r.dir()[1]*g[1];
		if(0 == deriv){ break; }
		dt = val / deriv;

		// Check if we will step outside the region
		coord_type t_new = t - dt;
		xy[0] = r.origin()[0] + t_new * r.dir()[0];
		xy[1] = r.origin()[1] + t_new * r.dir()[1];
		if(NULL == reg || reg->Contains(xy)){
			t = t_new;
		}else{
			// Exponential backoff until we remain within region.
			do{
				dt *= 0.5;
				t_new = t - dt;
				xy[0] = r.origin()[0] + t_new * r.dir()[0];
				xy[1] = r.origin()[1] + t_new * r.dir()[1];
			}while(niter++ < maxiter && !reg->Contains(xy));
			t = t_new;
		}
	}while(std::abs(dt) > tol && niter++ < maxiter);

	if(niter >= maxiter){ return false; }
	// Check if valid intersection
	if(t < 0){ return false; }
	if(NULL != reg && !reg->Contains(xy)){ return false; }

	x.t = t;
	x.p = r.origin() + x.t * r.dir();
	x.uv = xy;
	return true;
}

surface::Mesh* surface::Sag::AsMesh(const planar::Region *reg, const coord_type &tol) const{
	static const size_t nsamples = 100;
	planar::Polygon *boundary = reg->AsPolygon();

	// Generate 2D mesh
	std::vector<affine::Point2> vertices2;
	boundary->GetSamples(nsamples, planar::Shape::SamplingSpec::SAMPLED_APPROX, vertices2);
	for(size_t i = 0; i < boundary->size(); ++i){
		vertices2.push_back((*boundary)[i]);
	}
	planar::Mesh *mesh2 = planar::Mesh::Triangulate(vertices2);

	// Convert to 3D mesh
	surface::Mesh::vertex_vector vertices;
	vertices.reserve(vertices2.size());
	for(size_t i = 0; i < vertices2.size(); ++i){
		vertices.push_back(surface::Mesh::vertex_type(
			vertices2[i][0],
			vertices2[i][1],
			func->Evaluate(vertices2[i])
		));
	}
	const index_type nf = mesh2->NumFaces();
	surface::Mesh::index_vector triangle_indices;
	triangle_indices.reserve(3*nf);
	for(index_type i = 0; i < nf; ++i){
		index_type vi[3];
		mesh2->GetFace(i, vi[0], vi[1], vi[2]);
		for(unsigned j = 0; j < 3; ++j){
			triangle_indices.push_back(vi[j]);
		}
	}
	surface::Mesh *mesh = new surface::Mesh(vertices, triangle_indices);
	delete mesh2;
	delete boundary;
	return mesh;
}

coord_type surface::Sag::Orient(const affine::Point3 &q) const{
	return q[2] - func->Evaluate(affine::Point2(q[0], q[1]));
}

affine::Point3 surface::Sag::operator()(const affine::Point2 &uv) const{
	return affine::Point3(uv[0], uv[1], func->Evaluate(uv));
}
affine::Point2 surface::Sag::operator()(const affine::Point3 &r) const{
	return affine::Point2(r[0], r[1]);
}
bool surface::Sag::RayIntersection(const affine::Ray3 &r, std::vector<affine::Ray3::Intersection> &x) const{
	// TODO: use first intersection logic for now
	affine::Ray3::Intersection x1;
	bool hit = RayIntersection(r, x1);
	if(hit){
		x.push_back(x1);
	}
	return hit;
}
void surface::Sag::Neighborhood(const affine::Point2 &uv, DifferentialGeometry &dg) const{
	affine::Vector2 grad;
	coord_type II[3];
	const coord_type z = func->Evaluate(uv, &grad, II);
	dg.dpdu = affine::Vector3(1, 0, grad[0]);
	dg.dpdv = affine::Vector3(0, 1, grad[1]);
	dg.II[0] = II[0];
	dg.II[1] = II[1];
	dg.II[2] = II[2];
	dg.orientation = 1;
}

surface::Sag* surface::Sag::Clone() const{
	return new surface::Sag(func, reg);
}
