#include <geomod/surface/spherical.h>
#include <geomod/planar/circle.h>

using namespace geomod;
using namespace geomod::affine;

void geomod::surface::Spherical::GetSamples(size_t n, std::vector<affine::Point3> &samples) const{
	samples.reserve(n);
	if(0 < maxangle && maxangle <= 90){
		planar::Circle c(std::sin(maxangle / 180 * M_PI));
		std::vector<Point2> p;
		c.GetSamples(n, planar::Shape::SAMPLED_QUALITY, p);
		for(size_t i = 0; i < p.size(); ++i){
			samples.push_back(Point3(p[i][0], p[i][1], std::sqrt(1 - p[i][0]*p[i][0] - p[i][1]*p[i][1])));
		}
	}else{
		coord_type phi_scale;
		if(maxangle <= 0 || maxangle >= 180){
			phi_scale = 1;
		}else{
			phi_scale = maxangle / 180;
		}
		static const coord_type gr = (1 + std::sqrt(coord_type(5))) / 2;
		static const coord_type epsilon = 0.36;
		// Sample using an offset fibonacci spiral
		for(size_t i = 0; i < n; ++i){
			const coord_type theta = 2*M_PI*i / gr;
			const coord_type phi = phi_scale * std::acos(1 - 2*(i+epsilon)/(n-1+2*epsilon));
			samples.push_back(Point3(
				std::cos(theta) * std::sin(phi),
				std::sin(theta) * std::sin(phi),
				std::cos(phi)
			));
		}
	}
}
