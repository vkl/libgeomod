#include <geomod/util/arcball.h>
#include <geomod/affine/point.h>

using namespace geomod;
using namespace geomod::affine;
using namespace geomod::util;

Vector3 Arcball::MapPoint(const Point2 &p){
	Vector3 r(2*p[0]-1, 2*p[1]-1, 0);

	coord_type lr(r.NormSq());
	if(lr < 1){
		r[2] = sqrt(coord_type(1) - lr);
	}else{
		r.Normalize();
	}
	return r;
}

Arcball::Arcball(){
	current.cam_target = Point3::Origin();
	current.cam_offset = -coord_type(32)*Vector3::Z();
	current.q = Quaternion(Vector3::Zero(), coord_type(1));
	predrag = current;

	cursor = Point2::Origin();
	cursor_predrag = cursor;

	cam_target_prev   = current.cam_target;
	cam_target_target = current.cam_target;
	anim_num_timesteps = 20;
	anim_timesteps_remaining = 0;
	
	rotating = false;
	panning = false;
}
Arcball::~Arcball(){
}

void Arcball::GetView(View &view) const{
	view = current;
}
void Arcball::LoadView(const View &view){
	current = view;
	predrag = view;
}
void Arcball::UpdateCursor(const Point2 &p){
	cursor = p;
	if(rotating){
		const Vector3 u(MapPoint(cursor_predrag));
		const Vector3 v(MapPoint(cursor));
		Quaternion qnew(Vector3::Cross(u, v), Vector3::Dot(u, v));
		qnew.Normalize();
		current.q = qnew*predrag.q;
		current.q.Normalize();
	}else if(panning){
		const Vector2 pan(cursor - cursor_predrag);
		Vector3 offset(current.cam_offset.Norm() * pan, 0);
		current.cam_target = predrag.cam_target + Transform3(current.q.Conjugate())(offset);
	}else{
		cursor_predrag = p;
	}
}

bool Arcball::Rotating() const{ return rotating; }
void Arcball::RotationBegin(){ rotating = true; }
void Arcball::RotationEnd  (){ rotating = false; predrag.q = current.q; }

bool Arcball::Panning() const{ return panning; }
void Arcball::PanBegin(){ panning = true; predrag.cam_target = current.cam_target; }
void Arcball::PanEnd(){ panning = false;}

void Arcball::Zoom(const coord_type &offset){
	current.cam_offset *= pow(1.1, -offset);
}

affine::Transform3 Arcball::ViewTransform() const{
	Transform3 rot(current.q);
	return Transform3::Translate(current.cam_offset) *
	rot *
	Transform3::Translate(current.cam_target - Point3::Origin());

}
affine::Transform3 Arcball::ViewRotation() const{
	return Transform3(current.q);
}
coord_type Arcball::CameraDistance() const{
	return current.cam_offset.Norm();
}

void Arcball::AnimateToTarget(const affine::Point3 &target, int nsteps){
	const Point3 ntarg(-target[0], -target[1], -target[2]);
	if(0 == anim_timesteps_remaining){ // Currently not moving
		if(!(ntarg == current.cam_target)){
			cam_target_target = ntarg;
			anim_timesteps_remaining = anim_num_timesteps;
			cam_target_prev = current.cam_target;
		}
	}else{
		if(ntarg == cam_target_target){
			// ignore this request
		}else{
			cam_target_target = ntarg;
			anim_timesteps_remaining = anim_num_timesteps;
			cam_target_prev = current.cam_target;
		}
	}
	anim_num_timesteps = nsteps;
}

// sigmoidal curve in [0, 1]
template <typename T>
static T tween(const T &x0){
	static const T initial_slope = 0.1;
	static const T a = 2*(1-initial_slope);
	T x = x0;
	if(x0 > 0.5){ x = 1-x; }
	T y = a*(x+initial_slope)*x;
	if(x0 > 0.5){ y = 1-y; }
	return y;
}
void Arcball::IncrementAnimationTimestep(){
	if(anim_timesteps_remaining <= 0){
		return;
	}
	const coord_type t = (coord_type)anim_timesteps_remaining / (coord_type)anim_num_timesteps;
	current.cam_target = Point3::Interpolate(cam_target_target, cam_target_prev, tween(t));
	anim_timesteps_remaining--;
}
