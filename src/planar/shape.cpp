#include <geomod/planar/shape.h>
#include <geomod/planar/region.h>

using namespace geomod;
using namespace geomod::affine;
using namespace geomod::planar;

coord_type Shape::SignedDistance(const Point2 &p, Vector2 *grad) const{
	const Point2 q = Nearest(p);
	Vector2 v = p-q; v.Normalize();
	const bool neg = Contains(p);
	if(NULL != grad){
		*grad = (neg ? -1 : 1) * v;
	}
	return (neg ? -1 : 1) * v.Norm();
}

Region* Shape::Transformed(const Transform2 &transform) const{
	return new Region(Clone(), transform);
}
