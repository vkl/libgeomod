#include <iostream>
#include <geomod/planar/region.h>
#include <geomod/onedim/parameterization.h>
#include <geomod/planar/polygon.h>

using namespace geomod;
using namespace geomod::affine;
using namespace geomod::planar;

coord_type Region::Perimeter() const{
	return shape->Perimeter();
}
coord_type Region::Area() const{
	return shape->Area();
}
Point2 Region::Centroid() const{
	return transform(shape->Centroid());
}

Point2 Region::Extremum(const vector_type &dir) const{
	return transform(shape->Extremum(Transform2::Inverse(transform)(dir)));
}
Point2 Region::Nearest(const point_type &p) const{
	return transform(shape->Nearest(Transform2::Inverse(transform)(p)));
}
AABB2 Region::Bounds() const{
	bound_type bnd;
	bnd += transform(shape->Extremum(Transform2::Inverse(transform)( Vector2::X())));
	bnd += transform(shape->Extremum(Transform2::Inverse(transform)(-Vector2::X())));
	bnd += transform(shape->Extremum(Transform2::Inverse(transform)( Vector2::Y())));
	bnd += transform(shape->Extremum(Transform2::Inverse(transform)(-Vector2::Y())));
	return bnd;
}

size_t Region::NumCharts() const{
	return shape->NumCharts();
}
const onedim::Parameterization* Region::GetChartParameterization(index_type ichart) const{
	// TODO: fix this
	return shape->GetChartParameterization(ichart);
}

void Region::GetSamples(size_t n, Shape::SamplingSpec spec, std::vector<Point2> &points) const{
	size_t np = points.size();
	shape->GetSamples(n, spec, points);
	for(size_t i = np; i < points.size(); ++i){
		points[i] = transform(points[i]);
	}
}

Region* Region::Clone() const{
	return new Region(shape->Clone(), transform);
}

bool Region::Contains(const point_type &p) const{
	return shape->Contains(Transform2::Inverse(transform)(p));
}
bool Region::Convex() const{ return shape->Convex(); }

const Shape::FourierTransformable* Region::GetFourierTransformable() const{
	if(NULL != shape->GetFourierTransformable()){
		return this;
	}else{
		return NULL;
	}
}
complex_type Region::FourierTransform(const vector_type &f) const{
	const Transform2 inv(Transform2::Inverse(transform));
	const Shape::FourierTransformable* ft = shape->GetFourierTransformable();
	complex_type z = ft->FourierTransform(inv(f));
	coord_type cs, sn;
	specfun::cossin(2*M_PI*Vector2::Dot(transform(inv(Point2::Origin())-Point2::Origin()), f), &cs, &sn);
	z *= complex_type(cs, sn);
	return z;
}

coord_type Region::SignedDistance(const Point2 &p, Vector2 *grad) const{
	const coord_type d = shape->SignedDistance(Transform2::Inverse(transform)(p), grad);
	if(NULL != grad){ *grad = transform(*grad); }
	return d;
}

Polygon* Region::AsPolygon(const coord_type &tol) const{
	Polygon *poly = shape->AsPolygon(tol);
	for(size_t i = 0; i < poly->size(); ++i){
		poly->mutable_vertex(i) = transform(poly->vertex(i));
	}
	return poly;
}
Region* Region::Offset(const coord_type &dist, const coord_type &tol) const{
	return new Region(shape->Offset(dist, tol), transform);
}

Region* Region::Transformed(const Transform2 &xform) const{
	return new Region(shape->Clone(), xform * transform);
}

void Region::Transform(const Transform2 &xform){
	transform = xform * transform;
}
