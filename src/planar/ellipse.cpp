#include <geomod/planar/ellipse.h>
#include <geomod/planar/circle.h>
#include <geomod/onedim/parameterization.h>
#include <geomod/util/specfun.h>
#include <geomod/util/polynomial.h>
#include <geomod/util/rootfinder.h>
#include <geomod/planar/polygon.h>

using namespace geomod;
using namespace geomod::affine;
using namespace geomod::planar;


Ellipse::Parameterization::Parameterization(const Ellipse *parent):e(parent){}

Point2 Ellipse::Parameterization::operator()(const coord_type &u) const{
	const coord_type &rx = e->semimajor_axis(0);
	const coord_type &ry = e->semimajor_axis(1);
	coord_type cs, sn;
	specfun::cossin(2*M_PI*u, &cs, &sn);
	return Point2(rx*cs, ry*sn);
}

coord_type Ellipse::Parameterization::operator()(const Point2 &p)  const{
	const coord_type &rx = e->semimajor_axis(0);
	const coord_type &ry = e->semimajor_axis(1);
	coord_type u = atan2(p[1]/ry, p[0]/rx) / (2*M_PI);
	if(u < 0){ u += 1; }
	return u;
}

bool Ellipse::Parameterization::RayIntersection(const Ray2 &ray, std::vector<Ray2::Intersection> &x) const{
	const coord_type &rx = e->semimajor_axis(0);
	const coord_type &ry = e->semimajor_axis(1);
	Point2 o2(ray.origin());
	Vector2 d2(ray.dir());
	coord_type tscal;
	const onedim::Parameterization* param;
	if(rx < ry){
		const coord_type rr = rx/ry;
		Circle cr(rx);
		param = cr.GetChartParameterization(0);
		param->RayIntersection(Ray2(Point2(o2[0], rr*o2[1]), Vector2(d2[0], rr*d2[1])), x);
		tscal = d2.Norm() / hypot(d2[0], rr*d2[1]);
	}else{
		const coord_type rr = ry/rx;
		Circle cr(ry);
		param = cr.GetChartParameterization(0);
		param->RayIntersection(Ray2(Point2(rr*o2[0], o2[1]), Vector2(rr*d2[0], d2[1])), x);
		tscal = d2.Norm() / hypot(rr*d2[0], d2[1]);
	}
	delete param;
	for(size_t i = 0; i < x.size(); ++i){
		x[i].t *= tscal;
		x[i].p = ray(x[i].t);
		x[i].s = (*this)(x[i].p);
	}
	return x.size() > 0;
}

void Ellipse::Parameterization::Neighborhood(const coord_type &u, DifferentialGeometry &dg) const{
	const coord_type &rx = e->semimajor_axis(0);
	const coord_type &ry = e->semimajor_axis(1);
	static const coord_type tau(2*M_PI);
	coord_type cs, sn;
	specfun::cossin(tau*u, &cs, &sn);
	dg.dp = Vector2(-tau*rx*sn, tau*ry*cs);
	dg.ddp = Vector2(-tau*tau*rx*cs, -tau*tau*ry*sn);
}

bool Ellipse::Contains(const Point2 &p) const{
	Vector2 v(p - Point2::Origin());
	v[0] /= r[0];
	v[1] /= r[1];
	return v.Norm() < 1;
}

Point2 Ellipse::Extremum(const Vector2 &dir) const{
	// We must find the point at which the outward normal vector is in the given direction.
	// The quadratic form of the ellipse is:
	//   [ x y ] [ r[0]^-2      0    ] [ x ] = 1
	//           [    0      r[1]^-2 ] [ y ]
	// which we can write as r'*inv(A^2)*r = 1
	// Taking the gradient of the LHS and equating with the direction gives
	//   inv(A^2)*r = alpha*dir
	//   r = A^2 * alpha*dir
	// for some constant alpha.
	// Substituting this into the original quadratic form, we can solve for alpha:
	//   alpha^2 dir' A^2 dir = 1
	//   alpha = 1 / norm(A*dir)
	// Therefore,
	//   r = A^2 * dir / norm(A*dir)
	const Vector2 denom(r[0]*dir[0], r[1]*dir[1]);
	const coord_type dn = denom.Norm();
	return Point2(r[0]*r[0]*dir[0]/dn, r[1]*r[1]*dir[1]/dn);
}
Point2 Ellipse::Nearest(const Point2 &p_) const{
	// We want to solve:
	//    min |r-p|^2
	//   s.t. r' * inv(A)^2 * r = 1
	// Note that the gradient of the implicit form must be parallel to (p-r). That is,
	//   inv(A)^2 * r = lambda * (p-r)
	// for some scalar lambda. Solving for r,
	//   r = A^2 inv(A^2 + lambda I) p
	// Substituting into the ellipse equation,
	//   p' * inv(A^2 + lambda I) * A^2 * inv(A^2 + lambda I) * p = 1

	// We will follow David Eberly's solution method.
	bool negx = false, negy = false;
	Point2 p(p_);
	if(p_[0] < 0){
		negx = true;
		p[0] = -p[0];
	}
	if(p_[1] < 1){
		negy = true;
		p[1] = -p[1];
	}
	if(0 == p[0]){
		if(0 == p[1]){
			if(r[0] > r[1]){
				return Point2(0, r[1]);
			}else{
				return Point2(r[0], 0);
			}
		}else{ // On vertical axis
			if(r[0] > r[1]){ // wide ellipse
				return Point2(0, r[1]);
			}else{ // tall ellipse
				const coord_type rt = 1 - (r[0]/r[1])*(r[0]/r[1]);
				if(std::abs(p[1]) > r[1]*rt){
					return Point2(0, r[1]);
				}else{
					const coord_type y = p[1] / rt;
					return Point2(r[0]*std::sqrt(1 - (y/r[1])*(y/r[1])), y);
				}
			}
		}
	}else if(0 == p[1]){
		if(r[1] > r[0]){ // tall ellipse
			return Point2(r[0], 0);
		}else{ // wide ellipse
			const coord_type rt = 1 - (r[1]/r[0])*(r[1]/r[0]);
			if(std::abs(p[0]) > r[0]*rt){
				return Point2(r[0], 0);
			}else{
				const coord_type x = p[0] / rt;
				return Point2(x, r[1]*std::sqrt(1 - (x/r[0])*(x/r[0])));
			}
		}
	}else{
		// Let tbar be solution of F(t) = 0 on (-min(r[0], r[1])^2, infty)
		// where F(t) = [r[0]*p[0]/(t + r[0]*r[0])]^2 + [r[1]*p[1]/(t + r[1]*r[1])]^2 - 1
		//
		// Let imin = 0 if r[0] < r[1] else 1, rmin = r[imin]
		// Let s = t/rmin^2
		coord_type z[2] = { p[0]/r[0], p[1]/r[1] };
		const unsigned imin = (r[0] < r[1] ? 0 : 1);
		const coord_type rr = r[imin ^ 1]/r[imin];
		// Now let us define a scaled function:
		//   G(s) = [r[0]*r[0]*z[0]/(s*rmin*rmin + r[0]*r[0])]^2 + [r[1]*r[1]*z[1]/(s*rmin*rmin + r[1]*r[1])]^2 - 1
		//        = [z[imin]/(s + 1)]^2 + [z[1-imin]/(s + rr*rr)]^2 - 1
		// The root is bounded in [-1+z[imin], -1+sqrt(rr^4 z[1-imin]^2 + z[imin]^2)]
		const size_t maxiter = std::numeric_limits<coord_type>::digits - std::numeric_limits<coord_type>::min_exponent;
		
		struct scaledG{
			coord_type z0, z1, rr2;
			scaledG(const coord_type &z0, const coord_type &z1, const coord_type &rr):z0(z0), z1(z1), rr2(rr*rr){
			}
			coord_type operator()(const coord_type &s) const{
				coord_type t1 = z0/(s+1);
				coord_type t2 = z1/(s+rr2);
				return t1*t1 + t2*t2 - 1;
			}
		};
		coord_type s = rootfinder::Bisection(
			scaledG(z[imin], z[imin^1], rr),
			-1 + z[imin], -1 + hypot(rr*rr * z[imin^1], z[imin]),
			std::numeric_limits<coord_type>::epsilon(),
			maxiter
		);
		coord_type tbar = s * r[imin]*r[imin];
		coord_type x = r[0]*r[0]*p[0] / (tbar + r[0]*r[0]);
		coord_type y = r[1]*r[1]*p[1] / (tbar + r[1]*r[1]);
		if(negx){ x = -x; }
		if(negy){ y = -y; }
		return Point2(x, y);
	}
}
AABB2 Ellipse::Bounds() const{
	return AABB2(Point2(-r[0], -r[1]), Point2(r[0], r[1]));
}


coord_type Ellipse::Perimeter() const{ return 0; } // TODO
coord_type Ellipse::Area() const{ return M_PI*r[0]*r[1]; }
Point2 Ellipse::Centroid() const{ return Point2::Origin(); }

complex_type Ellipse::FourierTransform(const Vector2 &f) const{
	if(r[0] >= r[1]){
		const Vector2 fs(f[0], r[1]/r[0] * f[1]);
		return Area() * specfun::FourierJinc(r[0] * fs.Norm());
	}else{
		const Vector2 fs(r[0]/r[1] * f[0], f[1]);
		return Area() * specfun::FourierJinc(r[1] * fs.Norm());
	}
}
	
Polygon* Ellipse::AsPolygon(const coord_type &tol) const{
	// Compute angle theta such that tol/r = 1 - cos(theta/2)
	// Then nsegments = ceil(2pi/angle)
	// We use the 4th order Taylor series,
	//   tol/r =~ theta^2/8 - theta^4/96
	// where the RHS is an underestimate.
	const coord_type rmax = (r[0] >= r[1] ? r[0] : r[1]);
	const coord_type rtol8 = 8*tol/rmax;
	coord_type theta2 = rtol8; // 2nd order Taylor approximation for theta^2
	// Perform 1 step of Newton correction
	coord_type qq = theta2*theta2/coord_type(6);
	theta2 -= 0.5*qq / (qq - 1); // One Newton correction
	const coord_type theta = std::sqrt(theta2);
	const size_t n = (int)ceil(2*M_PI / theta);

	coord_type cs1, sn1;
	specfun::cossin(2*M_PI / n, &cs1, &sn1);

	Polygon::vertex_vector v;
	v.reserve(n);
	coord_type cs(1), sn(0);
	for(size_t i = 0; i < n; ++i){
		v.push_back(Point2(r[0]*cs, r[1]*sn));
		coord_type temp = cs;
		cs = cs1*cs - sn1*sn;
		sn = cs1*sn + sn1*temp;
	}
	return new Polygon(v);
}

onedim::Parameterizable::chart_polyline Ellipse::GetChartPolyline(index_type ichart, const coord_type &tol) const{
	Polygon *p = AsPolygon(tol);
	onedim::Parameterizable::chart_polyline poly(p->GetVertices());
	delete p;
	poly.push_back(poly.front());
	return poly;
}

Shape* Ellipse::Offset(const coord_type &dist, const coord_type &tol) const{
	if(r[0] == r[1]){
		return new Circle(r[0] + dist);
	}
	return NULL;
}

void Ellipse::GetSamples(size_t n, Shape::SamplingSpec spec, std::vector<point_type> &points) const{
	// Generate a hexapolar grid.
	points.push_back(Point2::Origin());
	if(n < 7){ return; }
	// First "ring" contains 1 point, next contains 6, 12, 18, etc.

	// Determine number of rings; find smallest m such that:
	//   1 + 6*Sum[i, {i, 1, m}] <= n
	// I'm sure we can determine this with an equation, but this is fine for now.
	int m = 0;
	{
		int nn = 1;
		while(nn <= n){
			m++;
			nn += 6*m;
		}
		nn -= 6*m;
		m--; // we overshot by one, so go back.
		points.reserve(nn);
	}

	const coord_type fr = coord_type(1) / (m+0.5);
	for(int ir = 1; ir <= m; ++ir){ // which ring
		const int nq = 6*ir;
		const coord_type dq = 2*M_PI / nq;
		const coord_type cs1 = cos(dq);
		const coord_type sn1 = sin(dq);
		coord_type cs = 1, sn = 0;
		for(int iq = 0; iq < nq; ++iq){
			points.push_back(point_type(r[0]*ir*fr * cs, r[1]*ir*fr * sn));
			// Advance angle
			const coord_type cc = cs;
			cs = cs1*cc - sn1*sn;
			sn = cs1*sn + sn1*cc;
		}
	}
}

