#include <geomod/planar/mesh.h>

using namespace geomod;
using namespace geomod::affine;
using namespace geomod::planar;

index_type Mesh::NumVertices() const{
	return v.size();
}
index_type Mesh::NumEdges() const{
	return edge.size();
}
index_type Mesh::NumFaces() const{
	return face.size();
}

const affine::Point2&  Mesh::GetVertex(vertex_index vi) const{
	return v[vi];
}
void Mesh::GetFace(face_index f, vertex_index &v0, vertex_index &v1, vertex_index &v2) const{
	index_type h = face[f];
	v0 = half[h].from;
	h = half[h].next;
	v1 = half[h].from;
	h = half[h].next;
	v2 = half[h].from;
}
void Mesh::GetEdge(edge_index e, vertex_index &v0, vertex_index &v1) const{
	index_type h = edge[e];
	v0 = half[h].from;
	h = half[h].next;
	v1 = half[h].from;
}
Mesh::vertex_vector Mesh::GetBoundary() const{
	return vertex_vector();
}

bool Mesh::Convex() const{
	return false; // TODO
}

affine::AABB2 Mesh::Bounds() const{
	affine::AABB2 b;
	for(index_type i = 0; i < NumVertices(); ++i){
		b += v[i];
	}
	return b;
}

Mesh* Mesh::Triangulate(const Polygon &poly){
	Mesh *mesh = new Mesh();

	mesh->v = poly.GetVertices();
	const index_type n = poly.size();
	mesh->face.resize(n-2);
	mesh->half.reserve(3*(n-2));
	
	// Generate main polygon loop
	index_type loop_size = n;
	index_type loop_handle = 0; // index of a halfedge on main loop
	for(index_type i = 0; i < n; ++i){
		const index_type ip1 = i+1 < n ? i+1 : 0;
		mesh->half.push_back(Halfedge(i, i+1, -1, 0));
	}
	// Start looking for ears
	while(loop_size > 3){
		index_type h0 = loop_handle;
		index_type h1 = mesh->half[h0].next;
		index_type h2 = mesh->half[h1].next;
		for(index_type i = 0; i < loop_size; ++i){
			const vertex_index vi0 = mesh->half[h0].from;
			const vertex_index vi1 = mesh->half[h1].from;
			const vertex_index vi2 = mesh->half[h2].from;

			// Check if we can clip this ear
			bool can_clip = true;
			if(Point2::Orient(mesh->v[vi0], mesh->v[vi1], mesh->v[vi2]) < 0){
				can_clip = false;
			}else{
				// if this ear's triangle contains any other vertex, can't clip.
				for(index_type p = 0; p < n; ++p){
					if((p == vi0) || (p == vi1) || (p == vi2)){ continue; }
					if(
						(Point2::Orient(mesh->v[vi0], mesh->v[vi1], mesh->v[p]) > 0) &&
						(Point2::Orient(mesh->v[vi1], mesh->v[vi2], mesh->v[p]) > 0) &&
						(Point2::Orient(mesh->v[vi2], mesh->v[vi0], mesh->v[p]) > 0)
					){
						can_clip = false;
						break;
					}
				}
			}

			if(can_clip){ break; }

			h0 = h1;
			h1 = h2;
			h2 = mesh->half[h2].next;
		}

		// Clip off ear h0, h1, h2
		index_type nh = mesh->half.size();
		index_type nf = mesh->face.size();
		mesh->half.push_back(Halfedge(mesh->half[h2].from, h0, nh+1, nf));
		mesh->half.push_back(Halfedge(mesh->half[h0].from, h2, nh  , 0));
		mesh->face.push_back(nh);
		mesh->half[h1].next = nh;
		mesh->half[h0].next = nh+1;
		loop_handle = nh+1;
	}
	// Make final face
	mesh->face.push_back(loop_handle);

	// Make edges
	for(index_type h = 0; h < mesh->half.size(); ++h){
		if(mesh->half[h].flip < 0){
			mesh->edge.push_back(h);
		}else if(h < mesh->half[h].flip){
			mesh->edge.push_back(h);
		}
	}

	return mesh;
}

Mesh::face_index Mesh::Locate(const affine::Point2 &p, face_index hint) const{
	if(hint < 0){ hint = 0; }
	// Implement celestial walk
	return 0;
}

void Mesh::InsertVertex(const affine::Point2 &v){

}

void Mesh::DelaunayRefine(index_type max_new_vertices){

}

#include <CDT.h>

Mesh* Mesh::Triangulate(const std::vector<Point2> &points){
	CDT::Triangulation<coord_type> cdt(CDT::VertexInsertionOrder::AsProvided);
	cdt.insertVertices(
		points.begin(),
		points.end(),
		[](const Point2& p){ return p[0]; },
		[](const Point2& p){ return p[1]; }
	);
	cdt.eraseSuperTriangle();

	Mesh* mesh = new Mesh();

	const size_t nv = cdt.vertices.size();
	mesh->v.reserve(nv);
	for(size_t i = 0; i < nv; ++i){
		mesh->v.push_back(Point2(cdt.vertices[i].x, cdt.vertices[i].y));
	}

	const int nt = cdt.triangles.size();
	mesh->half.reserve(3*nt);
	mesh->face.reserve(nt);
	for(int i = 0; i < nt; ++i){
		for(int j = 0; j < 3; ++j){
			int flip = -1;
			if(CDT::noNeighbor != cdt.triangles[i].neighbors[j]){
				flip = cdt.triangles[i].neighbors[j];
			}
			mesh->half.push_back(Halfedge(
				cdt.triangles[i].vertices[j], // from
				3*i + (j+1)%3, // next
				flip, // flip; we temporarily store the neighboring face index
				i // face
			));
		}
		mesh->face.push_back(3*i);
	}
	// Go back and fix up the flip fields
	std::vector<halfedge_index> flip(3*nt, -1);
	for(int i = 0; i < nt; ++i){
		for(int j = 0; j < 3; ++j){
			int h = 3*i+j;
			if(mesh->half[h].flip < 0){
				mesh->edge.push_back(h);
			}else{
				for(int k = 0; k < 3; ++k){
					int h2 = 3*mesh->half[h].flip + k;
					if(mesh->half[h2].flip == i){
						flip[h] = h2;
					}
				}
			}
		}
	}
	// Make interior edges
	for(int i = 0; i < nt; ++i){
		for(int j = 0; j < 3; ++j){
			int h = 3*i+j;
			if(flip[h] >= 0 && h < flip[h]){
				mesh->edge.push_back(h);
			}
			mesh->half[h].flip = flip[h];
		}
	}
	return mesh;
}
