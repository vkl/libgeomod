#include <geomod/onedim/parameterization.h>
#include <geomod/planar/polygon.h>
#include <geomod/planar/rectangle.h>
#include <geomod/util/specfun.h>

using namespace geomod;
using namespace geomod::affine;
using namespace geomod::planar;

Point2 Polygon::EdgeParameterization::operator()(const coord_type &u) const{
	return a+u*v;
}
coord_type Polygon::EdgeParameterization::operator()(const affine::Point2 &p) const{
	coord_type t = Vector2::Dot(p-a, v) / v.NormSq();
	if(t < 0){ t = 0; }
	if(t > 1){ t = 1; }
	return t;
}
bool Polygon::EdgeParameterization::RayIntersection(const affine::Ray2 &ray, std::vector<affine::Ray2::Intersection> &x) const{
	Ray2::Intersection xsect;
	if(Ray2(a, v).Intersect(ray, xsect) && xsect.t > 0 && 0 <= xsect.s && xsect.s < 1){
		x.push_back(xsect);
		return true;
	}
	return false;
}
void Polygon::EdgeParameterization::Neighborhood(const coord_type &u, onedim::Parameterization::DifferentialGeometry &dg) const{
	dg.dp = v;
	dg.ddp = Vector2::Zero();
}


const Point2& Polygon::vertex(index_type i) const{
	index_type n = v.size();
	if(i < 0){ i += n; }
	else if(i >= n){ i -= n; }
	return v[i];
}

Point2& Polygon::mutable_vertex(index_type i){
	index_type n = v.size();
	if(i < 0){ i += n; }
	else if(i >= n){ i -= n; }
	return v[i];
}

void Polygon::Transform(const Transform2 &transform){
	for(size_t i = 0; i < v.size(); ++i){
		v[i] = transform(v[i]);
	}
}

bool Polygon::Contains(const Point2 &p) const{
	// This is the PNPOLY algorithm of W. Randolph Franklin:
	// https://wrfranklin.org/Research/Short_Notes/pnpoly.html
	bool c = false;
	for(size_t i = v.size()-1, j = 0; j < v.size(); i = j++){
		if(
			((v[j][1] > p[1]) != (v[i][1] > p[1])) &&
			(p[0] < (v[i][0]-v[j][0]) * (p[1]-v[j][1]) / (v[i][1]-v[j][1]) + v[j][0])
		){
			c = !c;
		}
	}
	return c;
}

bool Polygon::Convex() const{
	for(size_t i = v.size()-2, j = v.size()-1, k = 0; k < v.size(); i = j, j = k++){
		if(Point2::Orient(v[i], v[j], v[k]) < 0){
			return false;
		}
	}
	return true;	
}

Point2 Polygon::Extremum(const Vector2 &dir) const{
	size_t j = 0;
	for(size_t i = 1; i < v.size(); ++i){
		if(Vector2::Dot(v[i] - v[j], dir) > 0){
			j = i;
		}
	}
	return v[j];
}
Point2 Polygon::Nearest(const Point2 &p) const{
	Point2 pbest;
	coord_type dbest = std::numeric_limits<coord_type>::max(); 
	const size_t n = v.size();
	for(size_t i = n-1, j = 0; j < n; i=j++){
		const Vector2 ab(v[j] - v[i]);
		// Check vertex
		const Vector2 ap(p - v[i]);
		const coord_type di = ap.NormSq();
		if(di < dbest){
			pbest = v[i];
		}
		// Check edge
		//   min_t |v[i] + (v[j]-v[i])*t - p|^2
		//         |-ap + ab*t|^2
		//         di - 2*ap.ab t + ab.ab t^2
		//   t = ap.ab / ab.ab
		const coord_type t = Vector2::Dot(ap, ab) / ab.NormSq();
		if(coord_type(0) < t && t < coord_type(1)){
			const Point2 q(v[i] + t*ab);
			if((p-q).NormSq() < dbest){
				pbest = q;
			}
		}
	}
	return pbest;
}
AABB2 Polygon::Bounds() const{
	AABB2 b;
	for(size_t i = 0; i < v.size(); ++i){
		b += v[i];
	}
	return b;
}

coord_type Polygon::SignedDistance(const Point2 &p, Vector2 *grad) const{
	coord_type mindist = std::numeric_limits<coord_type>::max();
	Vector2 mingrad;

	const int psign = Contains(p) ? -1 : 1;

	for(size_t i = v.size()-1, j = 0; j < v.size(); i = j++){
		// Check vertex
		const Vector2 vdiff(p - v[i]);
		const coord_type vdist = vdiff.Norm();
		if(vdist < mindist){
			mindist = vdist;
			mingrad = coord_type(psign) * vdiff;
		}

		// Check edge
		const Vector2 vedge(v[j] - v[i]);
		// Let the nearest point be q(t) = v[i] + t*vedge for t in (0, 1)
		// The solution is:
		//   argmin_t |p-q(t)|^2 = |p - v[i] - t*vedge|^2
		//                       = |vdiff - t*vedge|^2
		//   t = vdiff.vedge / vedge.vedge
		const coord_type tedge = Vector2::Dot(vdiff, vedge) / vedge.NormSq();
		if(0 < tedge && tedge < 1){
			const coord_type edist = (vdiff - tedge*vedge).Norm();
			if(edist < mindist){
				mindist = edist;
				mingrad = -Vector2::Rot(vedge);
			}
		}
	}
	if(NULL != grad){
		mingrad.Normalize();
		*grad = mingrad;
	}
	return coord_type(psign) * mindist;
}

coord_type Polygon::Perimeter() const{
	coord_type p = 0;
	for(size_t i = v.size()-1, j = 0; j < v.size(); i = j++){
		p += (v[j] - v[i]).Norm();
	}
	return p;
}
coord_type Polygon::Area() const{
	coord_type a = 0;
	for(size_t i = v.size()-1, j = 0; j < v.size(); i = j++){
		a += Vector2::Cross(v[i] - Point2::Origin(), v[j] - Point2::Origin());
	}
	return 0.5*a;
}
Point2 Polygon::Centroid() const{
	Vector2 c;
	for(size_t i = v.size()-1, j = 0; j < v.size(); i = j++){
		Vector2 m(Point2::Interpolate(v[i], v[j], 0.5) - Point2::Origin());
		coord_type x = Vector2::Cross(v[i] - Point2::Origin(), v[j] - Point2::Origin());
		c += x*m;
	}
	return Point2::Origin() + c/(3*Area());
}

complex_type Polygon::FourierTransform(const Vector2 &f) const{
	if(Vector2::Zero() == f){
		return Area();
	}
	complex_type z = 0;
	// For k != 0,
	//   S(k) = i/|k|^2 * \sum_{i=0}^{n-1} z.((v_{i+1}-v_{i}) x k) sinc(k.(v_{i+1}-v_{i})/2) e^{ik.(v_{i+1}+v_{i})/2}
	// where sinc(x) = sin(x)/x, and k = 2*pi*f.
	for(size_t p = v.size()-1, q = 0; q < v.size(); p = q++){
		const Vector2 u(v[q] - v[p]);
		const Point2 c(Point2::Interpolate(v[p], v[q], 0.5));

		const coord_type num = Vector2::Cross(u, f) * specfun::FourierSinc(Vector2::Dot(u, f));
		coord_type cs, sn;
		specfun::cossin(2*M_PI*Vector2::Dot(Point2::Origin() - c, f), &cs, &sn);

		z += num * complex_type(sn, -cs);
	}
	z /= 2*M_PI*f.NormSq();
	return z;
}

Polygon* Polygon::Offset(const coord_type &dist, const coord_type &tol) const{
	const size_t n = v.size();

	vertex_vector vnew(v);
	for(size_t i = n-2, j = n-1, k = 0; k < n; i=j,j=k++){
		Vector2 x(Vector2::Rot(v[j]-v[i])); x.Normalize();
		Vector2 y(Vector2::Rot(v[k]-v[j])); y.Normalize();
		// Let the offset vector of the new vertex from p[j] be u.
		// We require:
		//   u.x == d1
		//   u.y == d2
		// In other words,
		//   [ x' ] u = [ d1 ]
		//   [ y' ]     [ d2 ]
		// So then u = [ -rot(y) rot(x) ] [ d1; d2 ]/cross(x,y)
		coord_type det = Vector2::Cross(x, y);
		const Vector2 un(
			y[1]*dist - x[1]*dist,
			x[0]*dist - y[0]*dist
		);
		if(0 != det){
			det = coord_type(1)/det;
			vnew[j] -= det*un;
		}
	}
	return new Polygon(vnew);
}


onedim::Parameterizable::chart_polyline Polygon::GetChartPolyline(index_type ichart, const coord_type &tol) const{
	onedim::Parameterizable::chart_polyline poly;
	poly.reserve(2);
	poly.push_back(vertex(ichart));
	poly.push_back(vertex(ichart+1));
	return poly;
}

Polygon Polygon::Clip(const Ray2 &r) const{
	const size_t n = v.size();
	size_t n_outside = n;
	std::vector<bool> inside(v.size(), false);
	for(size_t i = 0; i < v.size(); ++i){
		if(Vector2::Cross(r.dir(), v[i] - r.origin()) > 0){
			inside[i] = true;
			n_outside--;
		}
	}
	if(n == n_outside){
		return Polygon(Polygon::vertex_vector());
	}else if(0 == n_outside){
		return *this;
	}

	Polygon::vertex_vector w;

	// Find an outside vertex preceded by an inside vertex
	size_t ia = 0, ib = 0, ic = 0, id = 0;
	for(size_t p = v.size()-1, q = 0; q < n; p = q++){
		if(inside[p] && !inside[q]){
			ib = q;
			break;
		}
	}
	//std::cout << "n_outside = " << n_outside << ", initial ib = " << ib << std::endl;
	
	do{
		size_t nrun = 1;
		ic = ib;
		id = ib+1; if(id >= n){ id = 0; }
		while(!inside[id]){
			ic = id++;
			if(id >= n){ id = 0; }
			nrun++;
		}
		ia = (0 == ib ? n-1 : ib-1);
		//std::cout << "ia = " << ia << ", ib = " << ib << ", ic = " << ic << ", id = " << id << ", nrun = " << nrun << std::endl;
		// We now have the interval [i0, i1) (cyclically) containing
		// a run of vertices that are outside the halfplane.
		// ia, ib straddle the first new vertex, and ic, id straddle the second
		// Note: it is possible to have ib == ic, or ia == id

		// Compute new vertices
		Ray2::Intersection x;
		r.Intersect(Ray2(v[ia], v[ib] - v[ia]), x);
		const Point2 pab = x.p;
		r.Intersect(Ray2(v[ic], v[id] - v[ic]), x);
		const Point2 pcd = x.p;
		//std::cout << "pab = " << pab << std::endl;
		//std::cout << "pcd = " << pcd << std::endl;

		w.push_back(pab);
		w.push_back(pcd);

		// Advance ib to the last outside vertex or until we loop back
		size_t ib0 = ib;
		ib = id;
		do{
			w.push_back(v[ib]);
			ib++;
			if(ib >= n){ ib = 0; }
			if(!inside[ib]){ break; }
		}while(ib != ib0);

		n_outside -= nrun;
		if(0 == n_outside){ break; }
	}while(1);

	return Polygon(w);
}

bool Polygon::Triangulate(index_vector &triangle_indices) const{
	const size_t n = v.size();
	if(n < 3){ return false; }
	
	// Triangulation (without vertex insertion) will have n-2 triangles.
	triangle_indices.resize(3*(n-2));
	// We will store a working copy of the currently clipped polygon;
	// if it has m vertices currently, then we need to store 3 <= m <= n.
	// We also need to store the resulting triangles, requiring 3*(n-m) indices.
	// Together, the total size is 3*n - 2*m, which must be <= 3*(n-2). This is true for m >= 3.
	// Thus, we can keep the working copy of the polygon at the very end,
	// and add triangles to the beginning.

	// Let V be the working copy.
	index_type *V; // pointer to start of working copy
	size_t nv; // number of vertices in V
	size_t nt = 0; // number of triangles currently in triangulation
	
	// Make a copy of all the vertices
	V = &triangle_indices[2*n-6];
	nv = n;
	for(size_t i = 0; i < n; ++i){ V[i] = i; }
	
	index_type count = 2*nv;
	for(size_t i = nv-1; nv > 2; ){
		size_t u, w;
		if(0 >= (count--)){ // We have looped too much; this is a bad polygon
			return false;
		}
		// get 3 consecutive vertices
		u = i; //if(nv <= u){ u = 0; } // prev
		i = u+1; if(nv <= i){ i = 0; } // mid
		w = i+1; if(nv <= w){ w = 0; } // next

		// Determine if we can clip the ear
		bool can_clip = true;
		if(Point2::Orient(v[V[u]], v[V[i]], v[V[w]]) < 0){
			can_clip = false;
		}else{
			// if the u-i-w triangle contains any other vertex, can't clip.
			for(size_t p = 0; p < nv; ++p){
				if((p == u) || (p == i) || (p == w)){ continue; }
				if(
					(Point2::Orient(v[V[u]],v[V[i]],v[V[p]]) > 0) &&
					(Point2::Orient(v[V[i]],v[V[w]],v[V[p]]) > 0) &&
					(Point2::Orient(v[V[w]],v[V[u]],v[V[p]]) > 0)
				){
					can_clip = false;
					break;
				}
			}
		}

		// Perform the clip
		if(can_clip){
			index_type ii[3] = {V[u], V[i], V[w]};
			// erase vertex i
			while(i > 0){
				V[i] = V[i-1];
				--i;
			}
			++V; --nv; count = 2*nv;
			
			triangle_indices[3*nt+0] = ii[0];
			triangle_indices[3*nt+1] = ii[1];
			triangle_indices[3*nt+2] = ii[2];
			++nt;
		}
	}
	return true;
}

void Polygon::GetSamples(size_t n, Shape::SamplingSpec spec, std::vector<point_type> &points) const{
	AABB2 bounds(Bounds());
	const Vector2 offset(bounds.Center() - Point2::Origin());
	Rectangle r(bounds.Size(0), bounds.Size(1));
	std::vector<point_type> superset;
	r.GetSamples(n, spec, superset);
	points.reserve(superset.size());
	for(size_t i = 0; i < superset.size(); ++i){
		const Point2 p(superset[i] + offset);
		if(Contains(p)){
			points.push_back(p);
		}
	}
}
