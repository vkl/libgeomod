#include <geomod/volume/extrusion.h>
#include <geomod/util/specfun.h>
#include <geomod/planar/polygon.h>
#include <geomod/surface/mesh.h>
#include <geomod/planar/mesh.h>

using namespace geomod;
using namespace geomod::affine;
using namespace geomod::volume;

Point3 Extrusion::BaseParameterization::operator()(const Point2 &uv) const{
	return Point3(
		uv[0],
		uv[1],
		NULL != e->surface[isurf] ? e->surface[isurf]->Evaluate(uv) : 0
	) + (0 == isurf ? Vector3::Zero() : e->offset());
}
Point2 Extrusion::BaseParameterization::operator()(const Point3 &r) const{
	if(0 == isurf){
		return Point2(r[0], r[1]);
	}else{
		const Vector3 &h = e->offset();
		return Point2(r[0] - h[0], r[1] - h[1]);
	}
}
bool Extrusion::BaseParameterization::RayIntersection(const Ray3 &r, std::vector<Ray3::Intersection> &x) const{
	if(NULL == e->surface[isurf]){
		// Ray-plane intersection:
		//   p[2] + t*d[2] == h[2]
		const Vector3 h(0 != isurf ? e->offset() : Vector3::Zero());
		const Point3  &rp = r.origin();
		const Vector3 &rd = r.dir();
		const coord_type t = (h[2]-rp[2]) / rd[2];
		if(t <= 0){ return false; }
		Point2 p(rp[0] + t*rd[0] - h[0], rp[1] + t*rd[1] - h[1]);
		if(e->base()->Contains(p)){
			Ray3::Intersection xi;
			xi.t = t;
			xi.uv = p;
			xi.p = r(t);
			x.push_back(xi);
			return true;
		}
		return false;
	}else{
		// We want to solve
		//    r := oxy + t dxy
		//    oz + t dz - z(r-offset.xy) - offset.z == 0
		// The derivative is
		//    dz - dxy.grad_r z(r-offset.xy)

		const Vector3 offset = (0 != isurf ? e->offset() : Vector3::Zero());
		const Vector2 offset2(offset[0], offset[1]);

		// We start by bounding the problem in t
		//   r.o[2] + t * r.d[2] == z
		//   t = (z - r.o[2]) / r.d[2]
		onedim::Interval t_interval(0, 0);
		{
			const onedim::Interval surface_bounds = e->surface[isurf]->Bounds(e->base()->Bounds());
			const onedim::Interval z_interval(
				surface_bounds.beginning() + offset[2],
				surface_bounds.end()       + offset[2]
			);
			coord_type t0 = (z_interval.beginning() - r.origin()[2]) / r.dir()[2];
			coord_type t1 = (z_interval.end()       - r.origin()[2]) / r.dir()[2];
			if(t0 > t1){ std::swap(t0, t1); }
			t_interval = onedim::Interval(t0, t1);
		}

		// Early exit if the surface is bounded away from ray
		if(t_interval.end() <= 0){ return false; }

		// Define the 2D ray for convenience
		const Ray2 r2(
			Point2(r.origin()[0], r.origin()[1]),
			Vector2(r.dir()[0], r.dir()[1])
		);

		// We will apply Newton iterations to solve for t.
		coord_type t = t_interval.Center();
		coord_type dt;
		unsigned niter = 0;
		Point2 xy;
		Vector2 g;
		coord_type z, rr;
		static const coord_type tol = 1e-10;
		static const unsigned maxiter = 64;
		do{
			xy = r2(t) - offset2;
			z = e->surface[isurf]->Evaluate(xy, &g);
			const coord_type val = r.origin()[2] + t * r.dir()[2] - z - offset[2];
			const coord_type deriv = r.dir()[2] - Vector2::Dot(r2.dir(), g);
			if(0 == deriv){ break; }
			dt = val / deriv;
			t -= dt;
		}while(std::abs(dt) > tol && niter++ < maxiter);
		if(niter >= maxiter){ return false; }

		if(e->base()->Contains(xy)){
			Ray3::Intersection xi;
			xi.t = t;
			xi.uv = xy;
			xi.p = r(t);
			x.push_back(xi);
			return true;
		}
		return false;
	}
}
void Extrusion::BaseParameterization::Neighborhood(const Point2 &uv, DifferentialGeometry &dg) const{
	const coord_type hz(e->offset()[2]);
	if(NULL == e->surface[isurf]){
		dg.dpdu = Vector3::X();
		dg.dpdv = Vector3::Y();
		dg.II[0] = 0;
		dg.II[1] = 0;
		dg.II[2] = 0;
	}else{
		Vector2 g;
		e->surface[isurf]->Evaluate(uv, &g, dg.II);
		dg.dpdu = Vector3(1, 0, g[0]);
		dg.dpdv = Vector3(0, 1, g[1]);
	}
	if((hz > 0 && 0 == isurf) || (hz < 0 && 0 != isurf)){
		dg.orientation = -1;
	}else{
		dg.orientation = 1;
	}
}
AABB2 Extrusion::BaseParameterization::Bounds() const{
	return e->base()->Bounds();
}
surface::Parameterization::uv_boundary Extrusion::BaseParameterization::GetBoundary(const coord_type &tol) const{
	planar::Polygon *poly = e->base()->AsPolygon(tol);
	surface::Parameterization::uv_boundary b(poly->GetVertices());
	delete poly;
	return b;
}

Point3 Extrusion::EdgeParameterization::operator()(const Point2 &uv) const{
	const Vector3 &h = e->offset();
	const onedim::Parameterization *param = e->base()->GetChartParameterization(ichart);
	const Point2 p((*param)(uv[0]));
	return Point3(p[0] + uv[1]*h[0], p[1] + uv[1]*h[1], uv[1]*h[2]);
}
Point2 Extrusion::EdgeParameterization::operator()(const Point3 &r) const{
	const Vector3 &h = e->offset();
	const coord_type v(specfun::clamp(r[2] / h[2]));
	const Point2 p(r[0] - v*h[0], r[1] - v*h[1]);
	const onedim::Parameterization *param = e->base()->GetChartParameterization(ichart);
	return Point2((*param)(p), v);
}
bool Extrusion::EdgeParameterization::RayIntersection(const Ray3 &r, std::vector<Ray3::Intersection> &x) const{
	const Vector3 &h = e->offset();
	const Point3  &p3 = r.origin();
	const Vector3 &d3 = r.dir();
	// Perform 2D intersection on de-skewed ray.
	// Shearing transform:
	//   [ 1   a ] [ x ]   [ x' ]
	//   [   1 b ] [ y ] = [ y' ]
	//   [     1 ] [ z ]   [ z  ]
	// We want to shear the h vector to be vertical, so a = -h[0]/h[2], b = -h[1]/h[2]
	const onedim::Parameterization *param = e->base()->GetChartParameterization(ichart);
	Point2 p2(
		p3[0] - h[0]/h[2]*p3[2],
		p3[1] - h[1]/h[2]*p3[2]
	);
	Vector2 d2(
		d3[0] - h[0]/h[2]*d3[2],
		d3[1] - h[1]/h[2]*d3[2]
	);
	Ray2 r2(p2, d2);
	std::vector<Ray2::Intersection> x2;
	if(!param->RayIntersection(r2, x2)){ return false; }
	for(std::vector<Ray2::Intersection>::const_iterator xi = x2.begin(); x2.end() != xi; ++xi){
		const coord_type v = (p3[2] + xi->t * d3[2]) / h[2];
		if(0 < v && v < 1){
			Ray3::Intersection x3;
			x3.t = xi->t;
			x3.uv = Point2(xi->s, v);
			x3.p = r(x3.t);
			x.push_back(x3);
		}
	}
	return x.size() > 0;
}
void Extrusion::EdgeParameterization::Neighborhood(const Point2 &uv, DifferentialGeometry &dg) const{
	const onedim::Parameterization *param = e->base()->GetChartParameterization(ichart);
	onedim::Parameterization::DifferentialGeometry dg1;
	param->Neighborhood(uv[0], dg1);
	dg.dpdu = Vector3(dg1.dp[0], dg1.dp[1], 0);
	dg.dpdv = e->offset();

	Vector3 n(Vector3::Cross(dg.dpdu, dg.dpdv));
	n.Normalize();
	dg.II[0] = dg1.ddp[0]*n[0] + dg1.ddp[1]*n[1];
	dg.II[1] = 0;
	dg.II[2] = 0;
	dg.orientation = 1;
}
AABB2 Extrusion::EdgeParameterization::Bounds() const{
	return AABB2(Point2(0, 0), Point2(1, 1));
}
surface::Parameterization::uv_boundary Extrusion::EdgeParameterization::GetBoundary(const coord_type &tol) const{
	surface::Parameterization::uv_boundary b;
	b.reserve(4);
	b.push_back(Point2(0, 0));
	b.push_back(Point2(1, 0));
	b.push_back(Point2(1, 1));
	b.push_back(Point2(0, 1));
	return b;
}


Extrusion::Extrusion(
	const Extrusion::shape_type *shape, const Extrusion::vector_type &v,
	const SurfaceProfile *surface0,
	const SurfaceProfile *surface1
):
	shape(shape->Clone()),
	h(v),
	surface{
		NULL != surface0 ? surface0->Clone() : NULL,
		NULL != surface1 ? surface1->Clone() : NULL
	},
	paramb{
		BaseParameterization(this, 0),
		BaseParameterization(this, 1)
	}
{
	// Generate parameterizations
	parame.reserve(shape->NumCharts());
	for(size_t ichart = 0; ichart < shape->NumCharts(); ++ichart){
		parame.push_back(EdgeParameterization(this, ichart));
	}
}
Extrusion::~Extrusion(){
	if(NULL != surface[1]){ delete surface[1]; }
	if(NULL != surface[0]){ delete surface[0]; }
	delete shape;
}

coord_type Extrusion::SurfaceArea() const{
	// This is wrong if surface[] is not NULL
	return 2*shape->Area() + h.Norm()*shape->Perimeter();
}
coord_type Extrusion::Volume() const{
	// This is wrong if surface[] is not NULL
	return shape->Area() * h[2];
}
Point3 Extrusion::Centroid() const{
	// This is wrong if surface[] is not NULL
	Point2 c(shape->Centroid());
	return Point3(c[0] + 0.5*h[0], c[1] + 0.5*h[1], 0.5*h[2]);
}

Point3 Extrusion::Extremum(const Vector3 &dir) const{
	// This is wrong if surface[] is not NULL
	Vector2 v(dir[0], dir[1]);
	Point2 p(shape->Extremum(v));
	Vector3 q(p[0], p[1], 0);
	if(Vector3::Dot(dir, q) > Vector3::Dot(dir, q+h)){
		return Point3::Origin() + q;
	}else{
		return Point3::Origin() + q + h;
	}
}
Point3 Extrusion::Nearest(const Point3 &p) const{
	// This is wrong if surface[] is not NULL
	Point2 q(shape->Nearest(Point2(p[0], p[1])));
	Point3 r(q[0], q[1], 0);
	if((r-p).NormSq() < (r+h-p).NormSq()){
		return r;
	}else{
		return r + h;
	}
}
AABB3 Extrusion::Bounds() const{
	const AABB2 basebounds(shape->Bounds());
	AABB3 c;
	for(unsigned isurf = 0; isurf < 2; ++isurf){
		const Vector3 hi(0 == isurf ? Vector3::Zero() : h);
		onedim::Interval surfbounds(0, 0);
		if(NULL != surface[isurf]){
			surfbounds = surface[isurf]->Bounds(basebounds);
			for(unsigned ivert = 0; ivert < 4; ++ivert){
				for(unsigned iside = 0; iside < 2; ++iside){
					c += Point3(basebounds.Vertex(ivert)[0], basebounds.Vertex(ivert)[1], surfbounds[iside]) + hi;
				}
			}
		}else{
			for(unsigned ivert = 0; ivert < 4; ++ivert){
				c += Point3(basebounds.Vertex(ivert)[0], basebounds.Vertex(ivert)[1], 0) + hi;
			}
		}
	}
	return c;
}

size_t Extrusion::NumCharts() const{
	return 2 + shape->NumCharts();
}
const surface::Parameterization* Extrusion::GetChartParameterization(index_type ichart) const{
	if(0 == ichart){ return &paramb[0]; }
	if(1 == ichart){ return &paramb[1]; }
	return &parame[ichart-2];
}
surface::Parameterizable::chart_outline Extrusion::GetChartOutline(index_type ichart, const coord_type &tol) const{
	surface::Parameterizable::chart_outline outline;
	const planar::Polygon *poly = shape->AsPolygon(tol);
	size_t n = poly->size();
	if(0 == ichart || 1 == ichart){
		outline.reserve(n);
		if(0 == ichart){
			while(n --> 0){
				const Point2 &v = poly->vertex(n);
				const coord_type z = (NULL == surface[ichart] ? 0 : surface[ichart]->Evaluate(v));
				outline.push_back(Point3(v[0], v[1], z));
			}
		}else{
			for(size_t i = 0; i < n; ++i){
				const Point2 &v = poly->vertex(i);
				const coord_type z = (NULL == surface[ichart] ? 0 : surface[ichart]->Evaluate(v));
				outline.push_back(Point3(v[0]+h[0], v[1]+h[1], z+h[2]));
			}
		}
	}else{
		ichart -= 2;
	}
	delete poly;
	return outline;
}

Extrusion* Extrusion::Clone() const{ return new Extrusion(shape, h, surface[0], surface[1]); }

bool Extrusion::Contains(const Point3 &p) const{
	if(NULL == surface[0] && NULL == surface[1]){
		if(p[2] < 0 || p[2] > h[2]){ return false; }
		coord_type t = p[2] / h[2];
		Point2 q(p[0] - t*h[0], p[1] - t*h[1]);
		return shape->Contains(q);
	}else{
		coord_type t = p[2] / h[2];
		Point2 q(p[0] - t*h[0], p[1] - t*h[1]);
		if(!shape->Contains(q)){ return false; }
		const coord_type z0 = (NULL != surface[0] ? surface[0]->Evaluate(q) : 0);
		const coord_type z1 = (NULL != surface[1] ? surface[1]->Evaluate(q) : 0);
		return z0 < p[2] && p[2] < z1+h[2];
	}
}

surface::Mesh* Extrusion::AsMesh(const coord_type &tol) const{
	static const size_t num_sample_points = 100; // this really should be adaptive based on tol, but that is hard
	const planar::Polygon* poly = shape->AsPolygon(tol);
	planar::Polygon::vertex_vector vertices = poly->GetVertices();
	const size_t region_vertex_count = vertices.size();

	// We must sample the interior of the region if the surfaces are not flat
	size_t ns = 0;
	if(NULL != surface[0] || NULL != surface[1]){
		std::vector<planar::Shape::point_type> samples;
		shape->GetSamples(num_sample_points, planar::Shape::SAMPLED_APPROX, samples);
		ns = samples.size();
		vertices.insert(vertices.end(), samples.begin(), samples.end());
	}
	const size_t total_vertex_count = vertices.size();

	surface::Mesh::vertex_vector v;
	// Add all vertices
	v.reserve(2*total_vertex_count);
	for(size_t i = 0; i < total_vertex_count; ++i){
		const coord_type z = (NULL != surface[1] ? surface[1]->Evaluate(vertices[i]) : 0);
		v.push_back(Point3(
			vertices[i][0] + h[0],
			vertices[i][1] + h[1],
			z + h[2]
		));
	}
	for(size_t i = 0; i < total_vertex_count; ++i){
		const coord_type z = (NULL != surface[0] ? surface[0]->Evaluate(vertices[i]) : 0);
		v.push_back(Point3(
			vertices[i][0],
			vertices[i][1],
			z
		));
	}

	surface::Mesh::index_vector ind;
	if(0 == ns){
		poly->Triangulate(ind); // We may triangulate only the region polygon in the flat case.
		const size_t nt = ind.size() / 3;

		// Add top and bottom triangle indices
		// Top vertices came first, so we just need to append bottoms and flip
		ind.reserve(6*nt + 6*region_vertex_count);
		for(size_t i = 0; i < nt; ++i){
			ind.push_back(ind[3*i+0]+region_vertex_count);
			ind.push_back(ind[3*i+2]+region_vertex_count);
			ind.push_back(ind[3*i+1]+region_vertex_count);
		}
		for(size_t i = region_vertex_count-1, j = 0; j < region_vertex_count; i=j++){
			ind.push_back(j);
			ind.push_back(i);
			ind.push_back(i+region_vertex_count);

			ind.push_back(i+region_vertex_count);
			ind.push_back(j+region_vertex_count);
			ind.push_back(j);
		}
	}else{
		planar::Mesh *meshb = planar::Mesh::Triangulate(vertices);
		const size_t nt = meshb->NumFaces();
		ind.reserve(3*(2*nt + 2*region_vertex_count));
		for(size_t i = 0; i < nt; ++i){
			planar::Mesh::vertex_index vi0, vi1, vi2;
			meshb->GetFace(i, vi0, vi1, vi2);
			// Top triangle
			ind.push_back(vi0);
			ind.push_back(vi1);
			ind.push_back(vi2);
			// Bottom triangle
			ind.push_back(vi0+total_vertex_count);
			ind.push_back(vi2+total_vertex_count);
			ind.push_back(vi1+total_vertex_count);
		}
		for(size_t i = region_vertex_count-1, j = 0; j < region_vertex_count; i=j++){
			ind.push_back(j);
			ind.push_back(i);
			ind.push_back(i+total_vertex_count);

			ind.push_back(i+total_vertex_count);
			ind.push_back(j+total_vertex_count);
			ind.push_back(j);
		}
		delete meshb;
	}
	delete poly;
	return new surface::Mesh(v, ind);
}

Solid* Extrusion::Offset(const coord_type &dist, const coord_type &tol) const{
	return NULL; // TODO
}

bool Extrusion::Convex() const{
	if(NULL == surface[0] && NULL == surface[1]){
		return shape->Convex();
	}else{
		return false;
	}
}